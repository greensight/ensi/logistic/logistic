<?php

namespace App\Providers;

use App\Domain\CargoOrders\Models\Cargo;
use App\Domain\CargoOrders\Observers\CargoObserver;
use App\Domain\DeliveryOrders\Models\DeliveryOrder;
use App\Domain\DeliveryOrders\Observers\DeliveryOrderAfterCommitObserver;
use App\Domain\DeliveryOrders\Observers\DeliveryOrderObserver;
use App\Domain\DeliveryServices\Models\DeliveryServiceDocument;
use App\Domain\DeliveryServices\Observers\DeliveryServiceDocumentObserver;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array<class-string, array<int, class-string>>
     */
    protected $listen = [];

    public function boot(): void
    {
        DeliveryOrder::observe(DeliveryOrderObserver::class);
        DeliveryOrder::observe(DeliveryOrderAfterCommitObserver::class);
        DeliveryServiceDocument::observe(DeliveryServiceDocumentObserver::class);
        Cargo::observe(CargoObserver::class);
    }

    public function shouldDiscoverEvents(): bool
    {
        return false;
    }
}
