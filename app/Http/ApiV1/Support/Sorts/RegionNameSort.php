<?php

namespace App\Http\ApiV1\Support\Sorts;

use App\Domain\Geos\Models\Region;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Spatie\QueryBuilder\Sorts\Sort;

class RegionNameSort implements Sort
{
    public function __invoke(Builder $query, bool $descending, string $property)
    {
        $direction = $descending ? 'DESC' : 'ASC';

        /** @var Builder<Model> $column */
        $column = Region::select('name')
            ->whereColumn('regions.id', 'region_id');

        $query->orderBy(
            $column,
            $direction
        );
    }
}
