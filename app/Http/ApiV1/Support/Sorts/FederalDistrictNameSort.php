<?php

namespace App\Http\ApiV1\Support\Sorts;

use App\Domain\Geos\Models\FederalDistrict;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Spatie\QueryBuilder\Sorts\Sort;

class FederalDistrictNameSort implements Sort
{
    public function __invoke(Builder $query, bool $descending, string $property): void
    {
        $direction = $descending ? 'DESC' : 'ASC';

        /** @var Builder<Model> $column */
        $column = FederalDistrict::select('name')
            ->whereColumn('federal_districts.id', 'federal_district_id');

        $query->orderBy(
            $column,
            $direction
        );
    }
}
