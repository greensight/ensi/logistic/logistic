<?php

namespace App\Http\ApiV1\Modules\Geos\Resources;

use App\Domain\DeliveryServices\Models\City;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/** @mixin City */
class CitiesResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'region_id' => $this->region_id,
            'name' => $this->name,
            'guid' => $this->guid,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'region' => RegionsResource::make($this->whenLoaded('region')),
        ];
    }
}
