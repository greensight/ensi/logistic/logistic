<?php

namespace App\Http\ApiV1\Modules\Geos\Resources;

use App\Domain\DeliveryServices\Models\CityDeliveryServiceLink;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/** @mixin CityDeliveryServiceLink */
class DeliveryCitiesResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'delivery_service' => $this->delivery_service,
            'city_id' => $this->city_id,
            'city_guid' => $this->city_guid,
            'payload' => $this->payload,
            'city' => $this->city,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
