<?php

namespace App\Http\ApiV1\Modules\Geos\Resources;

use App\Domain\Geos\Models\FederalDistrict;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/** @mixin FederalDistrict */
class FederalDistrictsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'regions' => RegionsResource::collection($this->whenLoaded('regions')),
        ];
    }
}
