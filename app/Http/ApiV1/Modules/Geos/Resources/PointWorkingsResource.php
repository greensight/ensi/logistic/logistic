<?php

namespace App\Http\ApiV1\Modules\Geos\Resources;

use App\Domain\DeliveryServices\Models\PointWorking;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Illuminate\Http\Request;

/**
 * @mixin PointWorking
 */
class PointWorkingsResource extends BaseJsonResource
{
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'point_id' => $this->point_id,
            'is_active' => $this->is_active,
            'day' => $this->day,
            'working_start_time' => $this->working_start_time,
            'working_end_time' => $this->working_end_time,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
