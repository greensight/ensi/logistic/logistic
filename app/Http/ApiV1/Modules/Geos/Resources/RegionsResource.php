<?php

namespace App\Http\ApiV1\Modules\Geos\Resources;

use App\Domain\Geos\Models\Region;
use App\Http\ApiV1\Modules\DeliveryPrices\Resources\DeliveryPricesResource;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/** @mixin Region */
class RegionsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'federal_district_id' => $this->federal_district_id,
            'name' => $this->name,
            'guid' => $this->guid,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'federal_district' => FederalDistrictsResource::make($this->whenLoaded('federalDistrict')),
            'delivery_prices' => DeliveryPricesResource::collection($this->whenLoaded('deliveryPrices')),
            'cities' => CitiesResource::collection($this->whenLoaded('cities')),
        ];
    }
}
