<?php

namespace App\Http\ApiV1\Modules\Geos\Resources;

use App\Domain\DeliveryServices\Models\Point;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * @mixin Point
 */
class PointsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'delivery_service_id' => $this->delivery_service_id,
            'external_id' => $this->external_id,
            'is_active' => $this->is_active,
            'name' => $this->name,
            'description' => $this->description,
            'phone' => $this->phone,
            'timezone' => $this->timezone,
            'address' => $this->address,
            'address_reduce' => $this->address_reduce,
            'metro_station' => $this->metro_station,
            'city_guid' => $this->city_guid,
            'geo_lat' => $this->geo_lat,
            'geo_lon' => $this->geo_lon,
            'only_online_payment' => $this->only_online_payment,
            'has_payment_card' => $this->has_payment_card,
            'has_courier' => $this->has_courier,
            'is_postamat' => $this->is_postamat,
            'max_value' => $this->max_value,
            'max_weight' => $this->max_weight,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,

            'point_workings' => PointWorkingsResource::collection($this->whenLoaded('pointWorkings')),
        ];
    }
}
