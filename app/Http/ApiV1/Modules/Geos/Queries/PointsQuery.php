<?php

namespace App\Http\ApiV1\Modules\Geos\Queries;

use App\Domain\DeliveryServices\Models\Point;
use Ensi\QueryBuilderHelpers\Filters\DateFilter;
use Ensi\QueryBuilderHelpers\Filters\NumericFilter;
use Ensi\QueryBuilderHelpers\Filters\StringFilter;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\AllowedInclude;
use Spatie\QueryBuilder\QueryBuilder;

class PointsQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(Point::query());

        $this->allowedSorts([
            'id', 'delivery_service_id', 'external_id', 'is_active', 'name',
            'phone', 'timezone', 'metro_station', 'city_guid', 'geo_lat', 'geo_lon',
            'only_online_payment', 'has_payment_card', 'has_courier', 'is_postamat',
            'max_value', 'max_weight', 'created_at', 'updated_at',
        ]);

        $this->allowedIncludes([
            AllowedInclude::relationship('point_workings', 'pointWorkings'),
        ]);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('delivery_service_id'),
            ...StringFilter::make('external_id')->exact()->contain(),
            AllowedFilter::exact('is_active'),
            ...StringFilter::make('name')->contain(),
            ...StringFilter::make('phone')->contain(),
            ...StringFilter::make('timezone')->contain(),

            ...StringFilter::make('metro_station')->contain(),
            ...StringFilter::make('city_guid')->contain(),
            ...StringFilter::make('geo_lat')->contain()->exact(),
            ...StringFilter::make('geo_lon')->contain()->exact(),

            AllowedFilter::exact('only_online_payment'),
            AllowedFilter::exact('has_payment_card'),
            AllowedFilter::exact('has_courier'),
            AllowedFilter::exact('is_postamat'),

            ...StringFilter::make('max_value')->contain()->exact(),
            ...NumericFilter::make('max_weight')->exact()->gte()->lte(),

            ...DateFilter::make('created_at')->gte()->lte(),
            ...DateFilter::make('updated_at')->gte()->lte(),
        ]);

        $this->defaultSort('-id');
    }
}
