<?php

namespace App\Http\ApiV1\Modules\Geos\Queries;

use App\Domain\Geos\Models\Region;
use Ensi\QueryBuilderHelpers\Filters\StringFilter;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\AllowedInclude;
use Spatie\QueryBuilder\QueryBuilder;

class RegionsQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(Region::query());

        $this->allowedSorts(['id', 'name']);
        $this->allowedIncludes([
            AllowedInclude::relationship('federal_district', 'federalDistrict'),
            AllowedInclude::relationship('delivery_prices', 'deliveryPrices'),
            'cities',
        ]);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('federal_district_id'),
            AllowedFilter::exact('guid'),

            ...StringFilter::make('name')->contain(),
        ]);

        $this->defaultSort('-id');
    }
}
