<?php

namespace App\Http\ApiV1\Modules\Geos\Queries;

use App\Domain\Geos\Models\FederalDistrict;
use Ensi\QueryBuilderHelpers\Filters\StringFilter;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class FederalDistrictsQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(FederalDistrict::query());

        $this->allowedSorts(['id', 'name']);
        $this->allowedIncludes(['regions']);

        $this->allowedFilters([
            AllowedFilter::exact('id'),

            ...StringFilter::make('name')->contain(),
        ]);

        $this->defaultSort('-id');
    }
}
