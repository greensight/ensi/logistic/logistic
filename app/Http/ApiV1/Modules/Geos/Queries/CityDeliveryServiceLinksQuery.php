<?php

namespace App\Http\ApiV1\Modules\Geos\Queries;

use App\Domain\DeliveryServices\Models\CityDeliveryServiceLink;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class CityDeliveryServiceLinksQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(CityDeliveryServiceLink::query());

        $this->allowedSorts(['id', 'name']);
        $this->allowedIncludes(['city']);

        $this->allowedFilters([
            AllowedFilter::exact('delivery_service'),
            AllowedFilter::exact('city_id'),
            AllowedFilter::exact('city_guid'),
        ]);

        $this->defaultSort('-id');
    }
}
