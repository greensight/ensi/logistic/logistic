<?php

namespace App\Http\ApiV1\Modules\Geos\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class PatchPointRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'delivery_service_id' => ['nullable', 'integer'],
            'is_active' => ['boolean'],
            ...self::addressField('address'),
            'city_guid' => ['nullable', 'string'],
            'timezone' => ['nullable', 'string'],
            'geo_lat' => ['nullable', 'string'],
            'geo_lon' => ['nullable', 'string'],
        ];
    }
}
