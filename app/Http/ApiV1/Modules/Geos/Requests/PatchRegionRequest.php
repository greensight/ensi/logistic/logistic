<?php

namespace App\Http\ApiV1\Modules\Geos\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class PatchRegionRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'federal_district_id' => ['nullable', 'integer'],
            'name' => ['nullable', 'string'],
            'guid' => ['nullable', 'string'],
      ];
    }
}
