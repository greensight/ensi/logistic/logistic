<?php

use App\Domain\DeliveryServices\Models\Point;
use App\Domain\DeliveryServices\Models\PointWorking;
use App\Http\ApiV1\Modules\Geos\Tests\Factories\PointRequestFactory;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaginationTypeEnum;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\LaravelTestFactories\FakerProvider;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('GET /api/v1/geos/points/{id} 200', function () {
    /** @var Point $point */
    $point = Point::factory()->create();

    getJson("/api/v1/geos/points/{$point->id}")
        ->assertStatus(200)
        ->assertJsonPath('data.id', $point->id)
        ->assertJsonStructure(['data' => ['id', 'delivery_service_id', 'external_id', 'name']]);
});

test('PATCH /api/v1/geos/points/{id} 200', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;
    /** @var Point $point */
    $point = Point::factory()->create();
    $request = PointRequestFactory::new()->make();
    patchJson("/api/v1/geos/points/{$point->id}", $request)

        ->assertStatus(200)
        ->assertJsonPath('data.id', $point->id)
        ->assertJsonPath('data.address.address_string', data_get($request, 'address.address_string'))
        ->assertJsonStructure(['data' => ['id', 'delivery_service_id', 'external_id', 'name']]);

    if (isset($request['address'])) {
        unset($request['address']);
    }

    assertDatabaseHas(Point::class, array_merge(['id' => $point->id], $request));
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/geos/points:search 200', function () {
    $points = Point::factory()
        ->count(10)
        ->sequence(
            ['is_active' => false],
            ['is_active' => true],
        )
        ->create();

    postJson('/api/v1/geos/points:search', [
        "filter" => ["is_active" => true],
        "sort" => ["-id"],
    ])
        ->assertStatus(200)
        ->assertJsonCount(5, 'data')
        ->assertJsonPath('data.0.id', $points->last()->id)
        ->assertJsonPath('data.0.is_active', true);
});

test("POST /api/v1/geos/points:search filter success", function (
    string $fieldKey,
    $value,
    ?string $filterKey,
    $filterValue,
) {
    /** @var Point $point */
    $point = Point::factory()->create($value ? [$fieldKey => $value] : []);

    postJson("/api/v1/geos/points:search", ["filter" => [
        ($filterKey ?: $fieldKey) => ($filterValue ?: $point->{$fieldKey}),
    ], 'sort' => ['id'], 'pagination' => ['type' => PaginationTypeEnum::OFFSET, 'limit' => 1]])
        ->assertStatus(200)
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $point->id);
})->with([
    ['id', null, null, null],
    ['delivery_service_id', 1, null, null],
    ['external_id', 'key', null, null],
    ['external_id', 'key', 'external_id_like', 'e'],
    ['is_active', true, null, null],
    ['name', 'point_my', 'name_like', '_my'],
    ['phone', '+79779997766', 'phone_like', '+7977'],
    ['timezone', 'Moscow', 'timezone_like', 'Mos'],
    ['metro_station', 'Ховрино', 'metro_station_like', 'Хов'],
    ['city_guid', 'JFEE-FKSS-MEME-FDFD', 'city_guid_like', 'FKSS'],
    ['geo_lat', '50.4323', null, null],
    ['geo_lon', '50.4323', null, null],
    ['only_online_payment', true, null, null],
    ['has_payment_card', true, null, null],
    ['has_courier', true, null, null],
    ['is_postamat', true, null, null],
    ['max_value', '10', null, null],
    ['max_value', '100 m3', 'max_value_like', '100'],
    ['max_weight', 10, null, null],
    ['max_weight', 10, 'max_weight_gte', 9],
    ['max_weight', 10, 'max_weight_lte', 11],
    ['created_at', '2022-04-20', 'created_at_gte', '2022-04-19'],
    ['created_at', '2022-04-20', 'created_at_lte', '2022-04-21'],
    ['updated_at', '2022-04-20', 'updated_at_gte', '2022-04-19'],
    ['updated_at', '2022-04-20', 'updated_at_lte', '2022-04-21'],
]);

test("POST /api/v1/geos/points:search sort success", function (string $sort) {
    Point::factory()->create();
    postJson("/api/v1/geos/points:search", ["sort" => [$sort]])->assertStatus(200);
})->with([
    'id', 'delivery_service_id', 'external_id', 'is_active', 'name',
    'phone', 'timezone', 'metro_station', 'city_guid', 'geo_lat', 'geo_lon',
    'only_online_payment', 'has_payment_card', 'has_courier', 'is_postamat',
    'max_value', 'max_weight', 'created_at', 'updated_at',
]);

test("POST /api/v1/geos/points:search include success", function () {
    /** @var Point $point */
    $point = Point::factory()->create();
    /** @var PointWorking $pointWorking */
    $pointWorking = PointWorking::factory()->for($point)->create();

    postJson("/api/v1/geos/points:search", ["include" => [
        'point_workings',
    ]])
        ->assertStatus(200)
        ->assertJsonPath('data.0.id', $point->id)
        ->assertJsonCount(1, 'data.0.point_workings')
        ->assertJsonPath('data.0.point_workings.0.id', $pointWorking->id);
});
