<?php

namespace App\Http\ApiV1\Modules\Geos\Controllers;

use App\Domain\Geos\Actions\Region\CreateRegionAction;
use App\Domain\Geos\Actions\Region\DeleteRegionAction;
use App\Domain\Geos\Actions\Region\PatchRegionAction;
use App\Http\ApiV1\Modules\Geos\Queries\RegionsQuery;
use App\Http\ApiV1\Modules\Geos\Requests\CreateRegionRequest;
use App\Http\ApiV1\Modules\Geos\Requests\PatchRegionRequest;
use App\Http\ApiV1\Modules\Geos\Resources\RegionsResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Contracts\Support\Responsable;

class RegionsController
{
    public function create(CreateRegionRequest $request, CreateRegionAction $action): Responsable
    {
        return new RegionsResource($action->execute($request->validated()));
    }

    public function patch(int $id, PatchRegionRequest $request, PatchRegionAction $action): Responsable
    {
        return new RegionsResource($action->execute($id, $request->validated()));
    }

    public function delete(int $id, DeleteRegionAction $action): Responsable
    {
        $action->execute($id);

        return new EmptyResource();
    }

    public function get(int $id, RegionsQuery $query): Responsable
    {
        return new RegionsResource($query->findOrFail($id));
    }

    public function search(PageBuilderFactory $pageBuilderFactory, RegionsQuery $query): Responsable
    {
        return RegionsResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    public function searchOne(RegionsQuery $query): Responsable
    {
        return new RegionsResource($query->firstOrFail());
    }
}
