<?php

namespace App\Http\ApiV1\Modules\Geos\Controllers;

use App\Http\ApiV1\Modules\Geos\Queries\CityDeliveryServiceLinksQuery;
use App\Http\ApiV1\Modules\Geos\Resources\DeliveryCitiesResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use Illuminate\Contracts\Support\Responsable;

class DeliveryCitiesController
{
    public function search(PageBuilderFactory $pageBuilderFactory, CityDeliveryServiceLinksQuery $query): Responsable
    {
        return DeliveryCitiesResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }
}
