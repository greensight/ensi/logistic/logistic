<?php

namespace App\Http\ApiV1\Modules\Geos\Controllers;

use App\Domain\Geos\Actions\FederalDistrict\CreateFederalDistrictAction;
use App\Domain\Geos\Actions\FederalDistrict\DeleteFederalDistrictAction;
use App\Domain\Geos\Actions\FederalDistrict\PatchFederalDistrictAction;
use App\Http\ApiV1\Modules\Geos\Queries\FederalDistrictsQuery;
use App\Http\ApiV1\Modules\Geos\Requests\CreateFederalDistrictRequest;
use App\Http\ApiV1\Modules\Geos\Requests\PatchFederalDistrictRequest;
use App\Http\ApiV1\Modules\Geos\Resources\FederalDistrictsResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Contracts\Support\Responsable;

class FederalDistrictsController
{
    public function create(CreateFederalDistrictRequest $request, CreateFederalDistrictAction $action): Responsable
    {
        return new FederalDistrictsResource($action->execute($request->validated()));
    }

    public function patch(int $id, PatchFederalDistrictRequest $request, PatchFederalDistrictAction $action): Responsable
    {
        return new FederalDistrictsResource($action->execute($id, $request->validated()));
    }

    public function delete(int $id, DeleteFederalDistrictAction $action): Responsable
    {
        $action->execute($id);

        return new EmptyResource();
    }

    public function get(int $id, FederalDistrictsQuery $query): Responsable
    {
        return new FederalDistrictsResource($query->findOrFail($id));
    }

    public function search(PageBuilderFactory $pageBuilderFactory, FederalDistrictsQuery $query): Responsable
    {
        return FederalDistrictsResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    public function searchOne(FederalDistrictsQuery $query): Responsable
    {
        return new FederalDistrictsResource($query->firstOrFail());
    }
}
