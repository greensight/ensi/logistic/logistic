<?php

namespace App\Http\ApiV1\Modules\Geos\Controllers;

use App\Domain\DeliveryServices\Actions\Point\PatchPointAction;
use App\Http\ApiV1\Modules\Geos\Queries\PointsQuery;
use App\Http\ApiV1\Modules\Geos\Requests\PatchPointRequest;
use App\Http\ApiV1\Modules\Geos\Resources\PointsResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use Illuminate\Contracts\Support\Responsable;

class PointsController
{
    public function get(int $id, PointsQuery $query): Responsable
    {
        return PointsResource::make($query->findOrFail($id));
    }

    public function patch(int $id, PatchPointRequest $request, PatchPointAction $action): Responsable
    {
        return PointsResource::make($action->execute($id, $request->validated()));
    }

    public function search(PageBuilderFactory $pageBuilderFactory, PointsQuery $query): Responsable
    {
        return PointsResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }
}
