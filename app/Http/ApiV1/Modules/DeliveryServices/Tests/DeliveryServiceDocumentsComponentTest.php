<?php

use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;

use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('POST /api/v1/delivery-services/delivery-service-documents:search 200', function () {
    postJson('/api/v1/delivery-services/delivery-service-documents:search')
        ->assertStatus(200);
});
