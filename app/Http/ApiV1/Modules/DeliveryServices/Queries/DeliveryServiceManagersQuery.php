<?php

namespace App\Http\ApiV1\Modules\DeliveryServices\Queries;

use App\Domain\DeliveryServices\Models\DeliveryServiceManager;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\AllowedInclude;
use Spatie\QueryBuilder\QueryBuilder;

class DeliveryServiceManagersQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(DeliveryServiceManager::query());

        $this->allowedSorts(['id', 'name', 'phone', 'email']);
        $this->allowedIncludes([
            AllowedInclude::relationship('delivery_service', 'deliveryService'),
        ]);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('delivery_service_id'),
            AllowedFilter::exact('name'),
            AllowedFilter::exact('phone'),
            AllowedFilter::exact('email'),
        ]);

        $this->defaultSort('-id');
    }
}
