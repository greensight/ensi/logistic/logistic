<?php

namespace App\Http\ApiV1\Modules\DeliveryServices\Queries;

use App\Domain\DeliveryServices\Models\DeliveryService;
use Ensi\QueryBuilderHelpers\Filters\StringFilter;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\AllowedInclude;
use Spatie\QueryBuilder\QueryBuilder;

class DeliveryServicesQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(DeliveryService::query());

        $this->allowedSorts([
            'id',
            'name',
            'legal_info_company_name',
        ]);
        $this->allowedIncludes([
            AllowedInclude::relationship('delivery_service_documents', 'deliveryServiceDocuments'),
            AllowedInclude::relationship('delivery_service_managers', 'deliveryServiceManagers'),
            AllowedInclude::relationship('payment_methods', 'paymentMethods'),
        ]);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            ...StringFilter::make('name')->exact()->contain(),
            AllowedFilter::partial('legal_info_company_name'),
            AllowedFilter::exact('legal_info_inn'),
            AllowedFilter::exact('legal_info_payment_account'),
            AllowedFilter::exact('legal_info_bik'),
            AllowedFilter::partial('legal_info_bank'),
            AllowedFilter::exact('legal_info_bank_correspondent_account'),
            AllowedFilter::partial('general_manager_name'),
            AllowedFilter::exact('contract_number'),
            AllowedFilter::exact('status'),
            AllowedFilter::partial('comment'),
            AllowedFilter::exact('do_consolidation'),
            AllowedFilter::exact('do_deconsolidation'),
            AllowedFilter::exact('do_zero_mile'),
            AllowedFilter::exact('do_express_delivery'),
            AllowedFilter::exact('do_return'),
            AllowedFilter::exact('do_dangerous_products_delivery'),
            AllowedFilter::exact('do_transportation_oversized_cargo'),
            AllowedFilter::exact('add_partial_reject_service'),
            AllowedFilter::exact('add_insurance_service'),
            AllowedFilter::exact('add_fitting_service'),
            AllowedFilter::exact('add_return_service'),
            AllowedFilter::exact('add_open_service'),
        ]);

        $this->defaultSort('name');
    }
}
