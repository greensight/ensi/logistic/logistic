<?php

namespace App\Http\ApiV1\Modules\DeliveryServices\Controllers;

use App\Domain\DeliveryServices\Actions\DeliveryServiceManager\CreateDeliveryServiceManagerAction;
use App\Domain\DeliveryServices\Actions\DeliveryServiceManager\DeleteDeliveryServiceManagerAction;
use App\Domain\DeliveryServices\Actions\DeliveryServiceManager\PatchDeliveryServiceManagerAction;
use App\Http\ApiV1\Modules\DeliveryServices\Queries\DeliveryServiceManagersQuery;
use App\Http\ApiV1\Modules\DeliveryServices\Requests\CreateDeliveryServiceManagerRequest;
use App\Http\ApiV1\Modules\DeliveryServices\Requests\PatchDeliveryServiceManagerRequest;
use App\Http\ApiV1\Modules\DeliveryServices\Resources\DeliveryServiceManagersResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Contracts\Support\Responsable;

class DeliveryServiceManagersController
{
    public function create(CreateDeliveryServiceManagerRequest $request, CreateDeliveryServiceManagerAction $action): Responsable
    {
        return new DeliveryServiceManagersResource($action->execute($request->validated()));
    }

    public function patch(int $id, PatchDeliveryServiceManagerRequest $request, PatchDeliveryServiceManagerAction $action): Responsable
    {
        return new DeliveryServiceManagersResource($action->execute($id, $request->validated()));
    }

    public function delete(int $id, DeleteDeliveryServiceManagerAction $action): Responsable
    {
        $action->execute($id);

        return new EmptyResource();
    }

    public function get(int $id, DeliveryServiceManagersQuery $query): Responsable
    {
        return new DeliveryServiceManagersResource($query->findOrFail($id));
    }

    public function search(PageBuilderFactory $pageBuilderFactory, DeliveryServiceManagersQuery $query): Responsable
    {
        return DeliveryServiceManagersResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    public function searchOne(DeliveryServiceManagersQuery $query): Responsable
    {
        return new DeliveryServiceManagersResource($query->firstOrFail());
    }
}
