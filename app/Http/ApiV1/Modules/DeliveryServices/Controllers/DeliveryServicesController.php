<?php

namespace App\Http\ApiV1\Modules\DeliveryServices\Controllers;

use App\Domain\DeliveryServices\Actions\DeliveryService\PatchDeliveryServiceAction;
use App\Http\ApiV1\Modules\DeliveryServices\Queries\DeliveryServicesQuery;
use App\Http\ApiV1\Modules\DeliveryServices\Requests\PatchDeliveryServiceRequest;
use App\Http\ApiV1\Modules\DeliveryServices\Resources\DeliveryServicesResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use Illuminate\Contracts\Support\Responsable;

class DeliveryServicesController
{
    public function patch(int $id, PatchDeliveryServiceRequest $request, PatchDeliveryServiceAction $action): Responsable
    {
        return new DeliveryServicesResource($action->execute($id, $request->validated()));
    }

    public function get(int $id, DeliveryServicesQuery $query): Responsable
    {
        return new DeliveryServicesResource($query->findOrFail($id));
    }

    public function search(PageBuilderFactory $pageBuilderFactory, DeliveryServicesQuery $query): Responsable
    {
        return DeliveryServicesResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    public function searchOne(DeliveryServicesQuery $query): Responsable
    {
        return new DeliveryServicesResource($query->firstOrFail());
    }
}
