<?php

namespace App\Http\ApiV1\Modules\DeliveryServices\Controllers;

use App\Domain\DeliveryServices\Actions\DeliveryServiceDocument\CreateDeliveryServiceDocumentAction;
use App\Domain\DeliveryServices\Actions\DeliveryServiceDocument\DeleteDeliveryServiceDocumentAction;
use App\Domain\DeliveryServices\Actions\DeliveryServiceDocument\PatchDeliveryServiceDocumentAction;
use App\Http\ApiV1\Modules\DeliveryServices\Queries\DeliveryServiceDocumentsQuery;
use App\Http\ApiV1\Modules\DeliveryServices\Requests\CreateDeliveryServiceDocumentRequest;
use App\Http\ApiV1\Modules\DeliveryServices\Requests\PatchDeliveryServiceDocumentRequest;
use App\Http\ApiV1\Modules\DeliveryServices\Resources\DeliveryServiceDocumentsResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Contracts\Support\Responsable;

class DeliveryServiceDocumentsController
{
    public function create(CreateDeliveryServiceDocumentRequest $request, CreateDeliveryServiceDocumentAction $action): Responsable
    {
        return new DeliveryServiceDocumentsResource($action->execute($request->validated()));
    }

    public function patch(int $id, PatchDeliveryServiceDocumentRequest $request, PatchDeliveryServiceDocumentAction $action): Responsable
    {
        return new DeliveryServiceDocumentsResource($action->execute($id, $request->validated()));
    }

    public function delete(int $id, DeleteDeliveryServiceDocumentAction $action): Responsable
    {
        $action->execute($id);

        return new EmptyResource();
    }

    public function get(int $id, DeliveryServiceDocumentsQuery $query): Responsable
    {
        return new DeliveryServiceDocumentsResource($query->findOrFail($id));
    }

    public function search(PageBuilderFactory $pageBuilderFactory, DeliveryServiceDocumentsQuery $query): Responsable
    {
        return DeliveryServiceDocumentsResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    public function searchOne(DeliveryServiceDocumentsQuery $query): Responsable
    {
        return new DeliveryServiceDocumentsResource($query->firstOrFail());
    }
}
