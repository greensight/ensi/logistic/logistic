<?php

namespace App\Http\ApiV1\Modules\DeliveryServices\Controllers;

use App\Domain\DeliveryServices\Actions\DeliveryService\AddPaymentMethodsToDeliveryServiceAction;
use App\Domain\DeliveryServices\Actions\DeliveryService\DeletePaymentMethodFromDeliveryServiceAction;
use App\Http\ApiV1\Modules\DeliveryServices\Requests\AddPaymentMethodsToDeliveryServiceRequest;
use App\Http\ApiV1\Modules\DeliveryServices\Requests\DeletePaymentMethodFromDeliveryServiceRequest;
use App\Http\ApiV1\Support\Resources\EmptyResource;

class DeliveryServicePaymentMethodLinksController
{
    public function add(
        int $deliveryServiceId,
        AddPaymentMethodsToDeliveryServiceRequest $request,
        AddPaymentMethodsToDeliveryServiceAction $action
    ): EmptyResource {
        $action->execute($deliveryServiceId, $request->getPaymentMethods());

        return new EmptyResource();
    }

    public function delete(
        int $deliveryServiceId,
        DeletePaymentMethodFromDeliveryServiceRequest $request,
        DeletePaymentMethodFromDeliveryServiceAction $action
    ): EmptyResource {
        $action->execute($deliveryServiceId, $request->getPaymentMethod());

        return new EmptyResource();
    }
}
