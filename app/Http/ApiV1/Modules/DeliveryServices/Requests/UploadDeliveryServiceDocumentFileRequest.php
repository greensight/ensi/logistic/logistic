<?php

namespace App\Http\ApiV1\Modules\DeliveryServices\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Http\UploadedFile;

class UploadDeliveryServiceDocumentFileRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'file' => ['required', 'file', 'max:102400'],
            'name' => ['nullable', 'string'],
        ];
    }

    public function getName(): ?string
    {
        return $this->input('name');
    }

    public function getFile(): UploadedFile
    {
        return $this->file('file');
    }
}
