<?php

namespace App\Http\ApiV1\Modules\DeliveryServices\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class DeletePaymentMethodFromDeliveryServiceRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'payment_method' => ['required', 'integer'],
        ];
    }

    public function getPaymentMethod(): int
    {
        return $this->input('payment_method');
    }
}
