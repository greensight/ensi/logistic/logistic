<?php

namespace App\Http\ApiV1\Modules\DeliveryServices\Requests;

use App\Http\ApiV1\OpenApiGenerated\Enums\DeliveryServiceEnum;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rules\Enum;

class CreateDeliveryServiceManagerRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'delivery_service_id' => ['required', new Enum(DeliveryServiceEnum::class)],
            'name' => ['required', 'string'],
            'phone' => ['required', 'regex:/^\+7\d{10}$/'],
            'email' => ['required', 'email'],
      ];
    }
}
