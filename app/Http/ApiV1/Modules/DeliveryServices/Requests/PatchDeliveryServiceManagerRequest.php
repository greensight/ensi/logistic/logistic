<?php

namespace App\Http\ApiV1\Modules\DeliveryServices\Requests;

use App\Http\ApiV1\OpenApiGenerated\Enums\DeliveryServiceEnum;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rules\Enum;

class PatchDeliveryServiceManagerRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'delivery_service' => ['nullable', new Enum(DeliveryServiceEnum::class)],
            'name' => ['nullable', 'string'],
            'phone' => ['nullable', 'regex:/^\+7\d{10}$/'],
            'email' => ['nullable', 'string'],
      ];
    }
}
