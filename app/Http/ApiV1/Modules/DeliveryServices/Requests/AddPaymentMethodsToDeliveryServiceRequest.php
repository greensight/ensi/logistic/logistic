<?php

namespace App\Http\ApiV1\Modules\DeliveryServices\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class AddPaymentMethodsToDeliveryServiceRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'payment_methods' => ['required', 'array', 'min:1'],
            'payment_methods.*' => ['integer', 'required'],
        ];
    }

    public function getPaymentMethods(): array
    {
        return $this->input('payment_methods');
    }
}
