<?php

namespace App\Http\ApiV1\Modules\CargoOrders\Controllers;

use App\Domain\CargoOrders\Actions\CancelCargoOrderAction;
use App\Http\ApiV1\Modules\CargoOrders\Queries\CargoOrdersQuery;
use App\Http\ApiV1\Modules\CargoOrders\Resources\CargoOrdersResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use Illuminate\Contracts\Support\Responsable;

/**
 * Контроллер для работы с заказами на забор груза (от продавца до РЦ)
 */
class CargoOrdersController
{
    public function cancel(int $id, CancelCargoOrderAction $action): Responsable
    {
        return CargoOrdersResource::make($action->execute($id));
    }

    public function search(PageBuilderFactory $pageBuilderFactory, CargoOrdersQuery $query): Responsable
    {
        return CargoOrdersResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }
}
