<?php

namespace App\Http\ApiV1\Modules\CargoOrders\Controllers;

use App\Domain\CargoOrders\Actions\CancelCargoAction;
use App\Domain\CargoOrders\Actions\PatchCargoAction;
use App\Http\ApiV1\Modules\CargoOrders\Queries\CargoQuery;
use App\Http\ApiV1\Modules\CargoOrders\Requests\PatchCargoRequest;
use App\Http\ApiV1\Modules\CargoOrders\Resources\CargoResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use Illuminate\Contracts\Support\Responsable;

class CargoController
{
    public function patch(int $id, PatchCargoRequest $request, PatchCargoAction $action): Responsable
    {
        return CargoResource::make($action->execute($id, $request->validated()));
    }

    public function cancel(int $id, CancelCargoAction $action): Responsable
    {
        return CargoResource::make($action->execute($id));
    }

    public function search(PageBuilderFactory $pageBuilderFactory, CargoQuery $query): Responsable
    {
        return CargoResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }
}
