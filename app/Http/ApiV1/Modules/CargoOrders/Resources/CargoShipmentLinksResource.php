<?php

namespace App\Http\ApiV1\Modules\CargoOrders\Resources;

use App\Domain\CargoOrders\Models\CargoShipmentLink;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/** @mixin CargoShipmentLink */
class CargoShipmentLinksResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'cargo_id' => $this->cargo_id,
            'shipment_id' => $this->shipment_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
