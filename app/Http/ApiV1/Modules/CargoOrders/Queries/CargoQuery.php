<?php

namespace App\Http\ApiV1\Modules\CargoOrders\Queries;

use App\Domain\CargoOrders\Models\Cargo;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\AllowedInclude;
use Spatie\QueryBuilder\QueryBuilder;

class CargoQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(Cargo::query());

        $this->allowedSorts([
            'id',
            'status',
            'is_problem',
            'is_problem_at',
            'is_canceled',
            'is_canceled_at',
            'created_at',
            'updated_at',
        ]);

        $this->allowedIncludes([
            AllowedInclude::relationship('shipment_links', 'shipmentLinks'),
            AllowedInclude::relationship('delivery_service', 'deliveryService'),
        ]);

        $this->allowedFilters([
            AllowedFilter::exact('seller_id'),
            AllowedFilter::exact('delivery_service_id'),
            AllowedFilter::exact('store_id'),
            AllowedFilter::exact('status'),
        ]);

        $this->defaultSort('-id');
    }
}
