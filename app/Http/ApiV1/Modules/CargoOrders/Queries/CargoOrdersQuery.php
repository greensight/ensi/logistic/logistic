<?php

namespace App\Http\ApiV1\Modules\CargoOrders\Queries;

use App\Domain\CargoOrders\Models\CargoOrder;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class CargoOrdersQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(CargoOrder::query());

        $this->allowedSorts(['id', 'cargo_id', 'date', 'status']);

        $this->allowedIncludes(['cargo']);

        $this->allowedFilters([
            AllowedFilter::exact('date'),
            AllowedFilter::exact('status'),
            AllowedFilter::exact('timeslot_id'),
        ]);

        $this->defaultSort('-id');
    }
}
