<?php

use App\Domain\DeliveryOrders\Models\DeliveryOrder;
use App\Domain\Kafka\Actions\Send\SendDeliveryOrderEventAction;
use App\Http\ApiV1\Modules\Common\Tests\Factories\CustomerPersonalDataRequestFactory;
use App\Http\ApiV1\Modules\Common\Tests\Factories\DeleteCustomerPersonalDataRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\LaravelTestFactories\FakerProvider;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('POST /api/v1/common/customer-personal-data:delete 200', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $this->mock(SendDeliveryOrderEventAction::class)->allows('execute');

    $customerId = 1;
    DeliveryOrder::factory()
        ->withPersonalData()
        ->done()
        ->count(10)
        ->create(['customer_id' => $customerId]);

    $request = DeleteCustomerPersonalDataRequestFactory::new()->make(['customer_id' => $customerId]);

    postJson('/api/v1/common/customer-personal-data:delete', $request)
        ->assertStatus(200);

    assertDatabaseHas((new DeliveryOrder())->getTable(), [
        'customer_id' => $customerId,
        'recipient_address' => null,
        'recipient_company_name' => null,
        'recipient_contact_name' => null,
        'recipient_email' => null,
        'recipient_phone' => null,
        'recipient_comment' => null,
    ]);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/common/customer-personal-data:delete 200 not delete personal data at another customer', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $this->mock(SendDeliveryOrderEventAction::class)->allows('execute');

    $firstCustomerId = 1;
    DeliveryOrder::factory()
        ->withPersonalData()
        ->done()
        ->create(['customer_id' => $firstCustomerId]);

    $secondCustomerId = 2;
    $secondCustomerDeliveryOrder = DeliveryOrder::factory()
        ->withPersonalData()
        ->done()
        ->create(['customer_id' => $secondCustomerId]);

    $request = DeleteCustomerPersonalDataRequestFactory::new()->make(['customer_id' => $firstCustomerId]);

    postJson('/api/v1/common/customer-personal-data:delete', $request)
        ->assertStatus(200);

    assertDatabaseHas((new DeliveryOrder())->getTable(), [
        'customer_id' => $firstCustomerId,
        'recipient_address' => null,
        'recipient_company_name' => null,
        'recipient_contact_name' => null,
        'recipient_email' => null,
        'recipient_phone' => null,
        'recipient_comment' => null,
    ]);

    assertDatabaseHas((new DeliveryOrder())->getTable(), [
        'customer_id' => $secondCustomerId,
        'recipient_address->address_string' => $secondCustomerDeliveryOrder->recipient_address->address_string,
        'recipient_company_name' => $secondCustomerDeliveryOrder->recipient_company_name,
        'recipient_contact_name' => $secondCustomerDeliveryOrder->recipient_contact_name,
        'recipient_email' => $secondCustomerDeliveryOrder->recipient_email,
        'recipient_phone' => $secondCustomerDeliveryOrder->recipient_phone,
        'recipient_comment' => $secondCustomerDeliveryOrder->recipient_comment,
    ]);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/common/customer-personal-data:delete 400', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $this->mock(SendDeliveryOrderEventAction::class)->allows('execute');

    $customerId = 1;
    DeliveryOrder::factory()
        ->withPersonalData()
        ->notDone()
        ->createOne(['customer_id' => $customerId]);

    $request = DeleteCustomerPersonalDataRequestFactory::new()->make(['customer_id' => $customerId]);

    postJson('/api/v1/common/customer-personal-data:delete', $request)
        ->assertStatus(400);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/common/customer-personal-data:check-conditions-to-delete 200 can_delete is equal true', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $this->mock(SendDeliveryOrderEventAction::class)->allows('execute');

    $customerId = 1;
    DeliveryOrder::factory()
        ->withPersonalData()
        ->done()
        ->createOne(['customer_id' => $customerId]);

    $request = CustomerPersonalDataRequestFactory::new()->make(['customer_id' => $customerId]);

    postJson('/api/v1/common/customer-personal-data:check-conditions-to-delete', $request)
        ->assertStatus(200)
        ->assertJsonPath('data.can_delete', true)
        ->assertJsonPath('data.message', '');
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/common/customer-personal-data:check-conditions-to-delete 400 can_delete is equal false', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $this->mock(SendDeliveryOrderEventAction::class)->allows('execute');

    $customerId = 1;
    DeliveryOrder::factory()
        ->withPersonalData()
        ->notDone()
        ->createOne(['customer_id' => $customerId]);

    $request = CustomerPersonalDataRequestFactory::new()->make(['customer_id' => $customerId]);

    postJson('/api/v1/common/customer-personal-data:check-conditions-to-delete', $request)
        ->assertStatus(200)
        ->assertJsonPath('data.can_delete', false)
        ->assertJsonPath('data.message', trans('customer_delete_messages.incomplete_deliveries'));
})->with(FakerProvider::$optionalDataset);
