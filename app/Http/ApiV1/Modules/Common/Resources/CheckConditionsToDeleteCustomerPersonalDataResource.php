<?php

namespace App\Http\ApiV1\Modules\Common\Resources;

use App\Domain\Common\Actions\Data\CustomerPersonalData;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * @mixin CustomerPersonalData
 */
class CheckConditionsToDeleteCustomerPersonalDataResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'can_delete' => $this->canDelete,
            'message' => $this->message,
        ];
    }
}
