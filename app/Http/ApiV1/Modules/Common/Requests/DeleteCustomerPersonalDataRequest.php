<?php

namespace App\Http\ApiV1\Modules\Common\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class DeleteCustomerPersonalDataRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'customer_id' => ['required', 'integer'],
        ];
    }

    public function getCustomerId(): int
    {
        return $this->input('customer_id');
    }
}
