<?php

namespace App\Http\ApiV1\Modules\Common\Controllers;

use App\Domain\Common\Actions\CheckConditionsToDeleteCustomerPersonalDataAction;
use App\Domain\Common\Actions\DeleteCustomerPersonalDataAction;
use App\Http\ApiV1\Modules\Common\Requests\CheckConditionsToDeleteCustomerPersonalDataRequest;
use App\Http\ApiV1\Modules\Common\Requests\DeleteCustomerPersonalDataRequest;
use App\Http\ApiV1\Modules\Common\Resources\CheckConditionsToDeleteCustomerPersonalDataResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Contracts\Support\Responsable;

class CustomerPersonalDataController
{
    public function delete(DeleteCustomerPersonalDataRequest $request, DeleteCustomerPersonalDataAction $action): Responsable
    {
        $action->execute($request->getCustomerId());

        return new EmptyResource();
    }

    public function checkConditionsToDelete(
        CheckConditionsToDeleteCustomerPersonalDataRequest $request,
        CheckConditionsToDeleteCustomerPersonalDataAction $action,
    ): CheckConditionsToDeleteCustomerPersonalDataResource {
        return new CheckConditionsToDeleteCustomerPersonalDataResource($action->execute($request->getCustomerId()));
    }
}
