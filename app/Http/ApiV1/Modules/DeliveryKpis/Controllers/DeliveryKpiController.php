<?php

namespace App\Http\ApiV1\Modules\DeliveryKpis\Controllers;

use App\Domain\DeliveryKpis\Actions\DeliveryKpi\PatchDeliveryKpiAction;
use App\Http\ApiV1\Modules\DeliveryKpis\Queries\DeliveryKpiQuery;
use App\Http\ApiV1\Modules\DeliveryKpis\Requests\PatchDeliveryKpiRequest;
use App\Http\ApiV1\Modules\DeliveryKpis\Resources\DeliveryKpiResource;
use Exception;

class DeliveryKpiController
{
    public function get(DeliveryKpiQuery $query): DeliveryKpiResource
    {
        return new DeliveryKpiResource($query->firstOrFail());
    }

    /**
     * @throws Exception
     */
    public function patch(PatchDeliveryKpiRequest $request, PatchDeliveryKpiAction $action): DeliveryKpiResource
    {
        return new DeliveryKpiResource($action->execute($request->validated()));
    }
}
