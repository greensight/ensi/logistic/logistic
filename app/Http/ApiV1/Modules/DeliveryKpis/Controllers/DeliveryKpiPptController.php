<?php

namespace App\Http\ApiV1\Modules\DeliveryKpis\Controllers;

use App\Domain\DeliveryKpis\Actions\DeliveryKpiPpt\CreateDeliveryKpiPptAction;
use App\Domain\DeliveryKpis\Actions\DeliveryKpiPpt\DeleteDeliveryKpiPptAction;
use App\Domain\DeliveryKpis\Actions\DeliveryKpiPpt\PatchDeliveryKpiPptAction;
use App\Http\ApiV1\Modules\DeliveryKpis\Queries\DeliveryKpiPptQuery;
use App\Http\ApiV1\Modules\DeliveryKpis\Requests\CreateDeliveryKpiPptRequest;
use App\Http\ApiV1\Modules\DeliveryKpis\Requests\PatchDeliveryKpiPptRequest;
use App\Http\ApiV1\Modules\DeliveryKpis\Resources\DeliveryKpiPptResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Contracts\Support\Responsable;

class DeliveryKpiPptController
{
    public function search(PageBuilderFactory $pageBuilderFactory, DeliveryKpiPptQuery $query): Responsable
    {
        return DeliveryKpiPptResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    public function searchOne(DeliveryKpiPptQuery $query): Responsable
    {
        return new DeliveryKpiPptResource($query->firstOrFail());
    }

    public function get(int $sellerId, DeliveryKpiPptQuery $query): Responsable
    {
        return new DeliveryKpiPptResource($query->whereSellerId($sellerId)->firstOrFail());
    }

    public function create(int $sellerId, CreateDeliveryKpiPptRequest $request, CreateDeliveryKpiPptAction $action): Responsable
    {
        return new DeliveryKpiPptResource($action->execute($sellerId, $request->validated()));
    }

    public function patch(int $sellerId, PatchDeliveryKpiPptRequest $request, PatchDeliveryKpiPptAction $action): Responsable
    {
        return new DeliveryKpiPptResource($action->execute($sellerId, $request->validated()));
    }

    public function delete(int $sellerId, DeleteDeliveryKpiPptAction $action): Responsable
    {
        $action->execute($sellerId);

        return new EmptyResource();
    }
}
