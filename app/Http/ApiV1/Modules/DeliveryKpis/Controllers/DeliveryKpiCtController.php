<?php

namespace App\Http\ApiV1\Modules\DeliveryKpis\Controllers;

use App\Domain\DeliveryKpis\Actions\DeliveryKpiCt\CreateDeliveryKpiCtAction;
use App\Domain\DeliveryKpis\Actions\DeliveryKpiCt\DeleteDeliveryKpiCtAction;
use App\Domain\DeliveryKpis\Actions\DeliveryKpiCt\PatchDeliveryKpiCtAction;
use App\Http\ApiV1\Modules\DeliveryKpis\Queries\DeliveryKpiCtQuery;
use App\Http\ApiV1\Modules\DeliveryKpis\Requests\CreateDeliveryKpiCtRequest;
use App\Http\ApiV1\Modules\DeliveryKpis\Requests\PatchDeliveryKpiCtRequest;
use App\Http\ApiV1\Modules\DeliveryKpis\Resources\DeliveryKpiCtResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Contracts\Support\Responsable;

class DeliveryKpiCtController
{
    public function search(PageBuilderFactory $pageBuilderFactory, DeliveryKpiCtQuery $query): Responsable
    {
        return DeliveryKpiCtResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    public function searchOne(DeliveryKpiCtQuery $query): Responsable
    {
        return new DeliveryKpiCtResource($query->firstOrFail());
    }

    public function get(int $sellerId, DeliveryKpiCtQuery $query): Responsable
    {
        return new DeliveryKpiCtResource($query->whereSellerId($sellerId)->firstOrFail());
    }

    public function create(int $sellerId, CreateDeliveryKpiCtRequest $request, CreateDeliveryKpiCtAction $action): Responsable
    {
        return new DeliveryKpiCtResource($action->execute($sellerId, $request->validated()));
    }

    public function patch(int $sellerId, PatchDeliveryKpiCtRequest $request, PatchDeliveryKpiCtAction $action): Responsable
    {
        return new DeliveryKpiCtResource($action->execute($sellerId, $request->validated()));
    }

    public function delete(int $sellerId, DeleteDeliveryKpiCtAction $action): Responsable
    {
        $action->execute($sellerId);

        return new EmptyResource();
    }
}
