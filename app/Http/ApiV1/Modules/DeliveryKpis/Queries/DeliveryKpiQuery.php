<?php

namespace App\Http\ApiV1\Modules\DeliveryKpis\Queries;

use App\Domain\DeliveryKpis\Models\DeliveryKpi;
use Spatie\QueryBuilder\QueryBuilder;

class DeliveryKpiQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(DeliveryKpi::query());
    }
}
