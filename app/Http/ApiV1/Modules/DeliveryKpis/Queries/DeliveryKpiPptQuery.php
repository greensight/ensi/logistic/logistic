<?php

namespace App\Http\ApiV1\Modules\DeliveryKpis\Queries;

use App\Domain\DeliveryKpis\Models\DeliveryKpiPpt;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class DeliveryKpiPptQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(DeliveryKpiPpt::query());

        $this->allowedSorts(['ppt']);

        $this->allowedFilters([
            AllowedFilter::exact('seller_id'),
        ]);

        $this->defaultSort('-id');
    }

    public function whereSellerId(int $sellerId): self
    {
        $this->where('seller_id', '=', $sellerId);

        return $this;
    }
}
