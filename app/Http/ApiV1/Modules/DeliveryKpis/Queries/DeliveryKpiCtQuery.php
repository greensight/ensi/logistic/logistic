<?php

namespace App\Http\ApiV1\Modules\DeliveryKpis\Queries;

use App\Domain\DeliveryKpis\Models\DeliveryKpiCt;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class DeliveryKpiCtQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(DeliveryKpiCt::query());

        $this->allowedSorts(['ct']);

        $this->allowedFilters([
            AllowedFilter::exact('seller_id'),
        ]);

        $this->defaultSort('-id');
    }

    public function whereSellerId(int $sellerId): self
    {
        $this->where('seller_id', '=', $sellerId);

        return $this;
    }
}
