<?php

namespace App\Http\ApiV1\Modules\DeliveryKpis\Resources;

use App\Domain\DeliveryKpis\Models\DeliveryKpiCt;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/** @mixin DeliveryKpiCt */
class DeliveryKpiCtResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'seller_id' => $this->seller_id,
            'ct' => $this->ct,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
