<?php

namespace App\Http\ApiV1\Modules\DeliveryKpis\Resources;

use App\Domain\DeliveryKpis\Models\DeliveryKpiPpt;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/** @mixin DeliveryKpiPpt */
class DeliveryKpiPptResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'seller_id' => $this->seller_id,
            'ppt' => $this->ppt,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
