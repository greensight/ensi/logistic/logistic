<?php

namespace App\Http\ApiV1\Modules\DeliveryKpis\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class PatchDeliveryKpiCtRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'ct' => ['sometimes', 'integer', 'min:0'],
        ];
    }
}
