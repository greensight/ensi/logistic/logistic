<?php

namespace App\Http\ApiV1\Modules\DeliveryKpis\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class PatchDeliveryKpiPptRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'ppt' => ['sometimes', 'integer', 'min:0'],
        ];
    }
}
