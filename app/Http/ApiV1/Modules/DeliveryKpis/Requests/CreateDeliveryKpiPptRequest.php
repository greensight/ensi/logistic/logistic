<?php

namespace App\Http\ApiV1\Modules\DeliveryKpis\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class CreateDeliveryKpiPptRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'ppt' => ['required', 'integer', 'min:0'],
        ];
    }
}
