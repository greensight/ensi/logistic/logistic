<?php

namespace App\Http\ApiV1\Modules\DeliveryKpis\Tests\Factories;

use Ensi\LaravelTestFactories\BaseApiFactory;

class DeliveryKpiRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'rtg' => $this->faker->numberBetween(1, 100),
            'ct' => $this->faker->numberBetween(1, 100),
            'ppt' => $this->faker->numberBetween(1, 100),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
