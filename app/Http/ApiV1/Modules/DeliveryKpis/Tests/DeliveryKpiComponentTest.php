<?php

use App\Domain\DeliveryKpis\Models\DeliveryKpi;
use App\Http\ApiV1\Modules\DeliveryKpis\Tests\Factories\DeliveryKpiRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('GET /api/v1/delivery-kpis/delivery-kpi 200', function () {
    getJson('/api/v1/delivery-kpis/delivery-kpi')
        ->assertStatus(200);
});

test('PATCH /api/v1/delivery-kpis/delivery-kpi 200', function () {
    $newData = DeliveryKpiRequestFactory::new()->only(['rtg'])->make();

    patchJson('/api/v1/delivery-kpis/delivery-kpi', $newData)
        ->assertStatus(200)
        ->assertJsonPath('data.rtg', $newData['rtg']);

    assertDatabaseHas((new DeliveryKpi())->getTable(), $newData);
});
