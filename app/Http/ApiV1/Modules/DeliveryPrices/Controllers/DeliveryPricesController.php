<?php

namespace App\Http\ApiV1\Modules\DeliveryPrices\Controllers;

use App\Domain\DeliveryPrices\Actions\DeliveryPrice\CheckoutDeliveryPricesAction;
use App\Domain\DeliveryPrices\Actions\DeliveryPrice\CreateDeliveryPriceAction;
use App\Domain\DeliveryPrices\Actions\DeliveryPrice\DeleteDeliveryPriceAction;
use App\Domain\DeliveryPrices\Actions\DeliveryPrice\PatchDeliveryPriceAction;
use App\Http\ApiV1\Modules\DeliveryPrices\Queries\DeliveryPricesQuery;
use App\Http\ApiV1\Modules\DeliveryPrices\Requests\CheckoutDeliveryPricesRequest;
use App\Http\ApiV1\Modules\DeliveryPrices\Requests\CreateDeliveryPriceRequest;
use App\Http\ApiV1\Modules\DeliveryPrices\Requests\PatchDeliveryPriceRequest;
use App\Http\ApiV1\Modules\DeliveryPrices\Resources\DeliveryPricesResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Contracts\Support\Responsable;

class DeliveryPricesController
{
    public function create(CreateDeliveryPriceRequest $request, CreateDeliveryPriceAction $action): DeliveryPricesResource
    {
        return new DeliveryPricesResource($action->execute($request->validated()));
    }

    public function patch(int $id, PatchDeliveryPriceRequest $request, PatchDeliveryPriceAction $action): Responsable
    {
        return new DeliveryPricesResource($action->execute($id, $request->validated()));
    }

    public function delete(int $id, DeleteDeliveryPriceAction $action): Responsable
    {
        $action->execute($id);

        return new EmptyResource();
    }

    public function get(int $id, DeliveryPricesQuery $query): Responsable
    {
        return new DeliveryPricesResource($query->findOrFail($id));
    }

    public function search(PageBuilderFactory $pageBuilderFactory, DeliveryPricesQuery $query): Responsable
    {
        return DeliveryPricesResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    public function searchOne(DeliveryPricesQuery $query): Responsable
    {
        return new DeliveryPricesResource($query->firstOrFail());
    }

    public function checkout(CheckoutDeliveryPricesRequest $request, CheckoutDeliveryPricesAction $action): Responsable
    {
        return DeliveryPricesResource::collection(
            $action->execute($request->convertToObject())
        );
    }
}
