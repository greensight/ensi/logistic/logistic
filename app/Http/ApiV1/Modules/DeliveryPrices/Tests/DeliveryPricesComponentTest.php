<?php

use App\Domain\DeliveryPrices\Models\DeliveryPrice;
use App\Domain\Geos\Models\FederalDistrict;
use App\Domain\Geos\Models\Region;
use App\Http\ApiV1\Modules\DeliveryPrices\Tests\Factories\CheckoutDeliveryPriceRequestFactory;
use App\Http\ApiV1\Modules\DeliveryPrices\Tests\Factories\CreateDeliveryPriceRequestFactory;
use App\Http\ApiV1\Modules\DeliveryPrices\Tests\Factories\PatchDeliveryPriceRequestFactory;
use App\Http\ApiV1\OpenApiGenerated\Enums\DeliveryMethodEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\DeliveryServiceEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaginationTypeEnum;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertModelMissing;
use function Pest\Laravel\deleteJson;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component', 'delivery', 'delivery.prices');

test('POST /api/v1/delivery-prices/delivery-prices 201', function () {
    /** @var FederalDistrict $federalDistrict */
    $federalDistrict = FederalDistrict::factory()->create();
    /** @var Region $region */
    $region = Region::factory()->create();

    $request = CreateDeliveryPriceRequestFactory::new()->make([
        'federal_district_id' => $federalDistrict->id,
        'region_id' => $region->id,
        'region_guid' => $region->guid,
    ]);

    postJson('/api/v1/delivery-prices/delivery-prices', $request)
        ->assertCreated();

    assertDatabaseHas(DeliveryPrice::class, [
        'price' => $request['price'],
    ]);
});

test('POST /api/v1/delivery-prices/delivery-prices 400', function (bool $withRegion) {
    /** @var FederalDistrict $federalDistrict */
    $federalDistrict = FederalDistrict::factory()->create();
    if ($withRegion) {
        /** @var Region $region */
        $region = Region::factory()->create();
    }

    /** @var DeliveryPrice $existModel */
    $existModel = DeliveryPrice::factory()->create([
        'federal_district_id' => $federalDistrict->id,
        'region_id' => $withRegion ? $region->id : null,
        'region_guid' => $withRegion ? $region->guid : null,
    ]);

    $request = CreateDeliveryPriceRequestFactory::new()->make([
        'federal_district_id' => $existModel->federal_district_id,
        'region_id' => $existModel->region_id,
        'region_guid' => $existModel->region_guid,
        'delivery_service' => $existModel->delivery_service,
        'delivery_method' => $existModel->delivery_method,
    ]);

    postJson('/api/v1/delivery-prices/delivery-prices', $request)
        ->assertBadRequest();
})->with([true, false]);

test('GET /api/v1/delivery-prices/delivery-prices/{id} 200', function () {
    /** @var DeliveryPrice $deliveryPrice */
    $deliveryPrice = DeliveryPrice::factory()->create();

    getJson("/api/v1/delivery-prices/delivery-prices/$deliveryPrice->id")
        ->assertJsonPath('data.price', $deliveryPrice->price)
        ->assertOk();
});

test('GET /api/v1/delivery-prices/delivery-prices/{id} 404', function () {
    /** @var DeliveryPrice $deliveryPrice */
    $undefinedId = 1;
    getJson("/api/v1/delivery-prices/delivery-prices/$undefinedId")
        ->assertNotFound();
});

test('PATCH /api/v1/delivery-prices/delivery-prices/{id} 200', function () {
    /** @var FederalDistrict $federalDistrict */
    $federalDistrict = FederalDistrict::factory()->create();
    /** @var Region $region */
    $region = Region::factory()->create();
    /** @var DeliveryPrice $deliveryPrice */
    $deliveryPrice = DeliveryPrice::factory()->create();

    $request = PatchDeliveryPriceRequestFactory::new()->make([
        'federal_district_id' => $federalDistrict->id,
        'region_id' => $region->id,
        'region_guid' => $region->guid,
    ]);

    patchJson("/api/v1/delivery-prices/delivery-prices/$deliveryPrice->id", $request)
        ->assertJsonPath('data.price', $request['price'])
        ->assertOk();

    assertDatabaseHas(DeliveryPrice::class, [
        'id' => $deliveryPrice->id,
        'price' => $request['price'],
    ]);
});

test('PATCH /api/v1/delivery-prices/delivery-prices/{id} 400', function () {
    /** @var DeliveryPrice $deliveryPriceExist */
    $deliveryPriceExist = DeliveryPrice::factory()->create();

    /** @var DeliveryPrice $deliveryPrice */
    $deliveryPrice = DeliveryPrice::factory()->create([
        'delivery_service' => $deliveryPriceExist->delivery_service,
        'delivery_method' => $deliveryPriceExist->delivery_method,
    ]);

    $request = PatchDeliveryPriceRequestFactory::new()->fromModel($deliveryPrice)->make([
        'federal_district_id' => $deliveryPriceExist->federal_district_id,
        'region_id' => $deliveryPriceExist->region_id,
        'region_guid' => $deliveryPriceExist->region_guid,
    ]);

    patchJson("/api/v1/delivery-prices/delivery-prices/{$deliveryPrice->id}", $request)
        ->assertBadRequest()
        ->assertJsonFragment(['Такое значение поля federal district id уже существует.']);

    assertDatabaseHas(DeliveryPrice::class, [
        'id' => $deliveryPrice->id,
        'federal_district_id' => $deliveryPrice->federal_district_id,
        'region_id' => $deliveryPrice->region_id,
        'region_guid' => $deliveryPrice->region_guid,
        'delivery_service' => $deliveryPrice->delivery_service,
        'delivery_method' => $deliveryPrice->delivery_method,
    ]);
});

test('PATCH /api/v1/delivery-prices/delivery-prices/{id} 404', function () {
    /** @var FederalDistrict $federalDistrict */
    $federalDistrict = FederalDistrict::factory()->create();
    /** @var Region $region */
    $region = Region::factory()->create();

    $request = PatchDeliveryPriceRequestFactory::new()->make([
        'federal_district_id' => $federalDistrict->id,
        'region_id' => $region->id,
        'region_guid' => $region->guid,
    ]);

    $undefinedId = 1;
    patchJson("/api/v1/delivery-prices/delivery-prices/$undefinedId", $request)
        ->assertNotFound();
});

test('DELETE /api/v1/delivery-prices/delivery-prices/{id} 200', function () {
    /** @var DeliveryPrice $deliveryPrice */
    $deliveryPrice = DeliveryPrice::factory()->create();

    deleteJson("/api/v1/delivery-prices/delivery-prices/$deliveryPrice->id")
        ->assertOk();

    assertModelMissing($deliveryPrice);
});

test('DELETE /api/v1/delivery-prices/delivery-prices/{id} 404', function () {
    $undefinedId = 1;
    deleteJson("/api/v1/delivery-prices/delivery-prices/$undefinedId")
        ->assertNotFound();
});

test('POST /api/v1/delivery-prices/delivery-prices:search 200', function () {
    $deliveryPrices = DeliveryPrice::factory()
        ->count(10)
        ->sequence(
            ['region_guid' => 'some-valid-region-guid-1'],
            ['region_guid' => 'some-valid-region-guid-2'],
        )
        ->create();

    postJson('/api/v1/delivery-prices/delivery-prices:search', [
        'filter' => ['region_guid' => 'some-valid-region-guid-2'],
        'sort' => ['-id'],
    ])
        ->assertOk()
        ->assertJsonCount(5, 'data')
        ->assertJsonPath('data.0.id', $deliveryPrices->last()->id)
        ->assertJsonPath('data.0.region_guid', 'some-valid-region-guid-2');
});

test('POST /api/v1/delivery-prices/delivery-prices:search filter success', function (string $fieldKey, $value = null, ?string $filterKey = null, $filterValue = null) {
    DeliveryPrice::factory()->create();
    /** @var DeliveryPrice $deliveryPrice */
    $deliveryPrice = DeliveryPrice::factory()->create($value ? [$fieldKey => $value] : []);

    postJson('/api/v1/delivery-prices/delivery-prices:search', [
        'filter' => [
            ($filterKey ?: $fieldKey) => ($filterValue ?: $deliveryPrice->{$fieldKey}),
        ], 'sort' => ['-id'], 'pagination' => ['type' => PaginationTypeEnum::OFFSET, 'limit' => 1],
    ])
        ->assertOk()
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $deliveryPrice->id);
})->with([
    ['federal_district_id', 1],
    ['region_id', 1],
    ['region_guid', 'some-valid-region-guid'],
    ['delivery_service', DeliveryServiceEnum::TK1],
    ['delivery_method', DeliveryMethodEnum::DELIVERY],
    ['price', 100, 'price_lte', 100],
    ['price', 100, 'price_lte', 200],
    ['price', 100, 'price_gte', 100],
    ['price', 100, 'price_gte', 10],
]);

test('POST /api/v1/delivery-prices/delivery-prices:search sort success', function (string $sort) {
    DeliveryPrice::factory()->create();
    postJson('/api/v1/delivery-prices/delivery-prices:search', ['sort' => [$sort]])->assertStatus(200);
})->with([
    'id',
    'price',
    'region.name',
    'federal_district.name',
]);

test('POST /api/v1/delivery-prices/delivery-prices:search include success', function (string $include, string $relationship = null) {
    /** @var DeliveryPrice $deliveryPrice */
    $deliveryPrice = DeliveryPrice::factory()->create();
    $relatedModel = $deliveryPrice->{$relationship ?? $include};

    postJson('/api/v1/delivery-prices/delivery-prices:search', [
        'include' => [
            $include,
        ],
    ])
        ->assertOk()
        ->assertJsonPath("data.0.$include.id", $relatedModel->id);
})->with([
    ['region'],
    ['federal_district', 'federalDistrict'],
]);

test('POST /api/v1/delivery-prices/delivery-prices:search-one 200', function () {
    $deliveryPrice = DeliveryPrice::factory()->create();

    $requestBody = [
        'filter' => [
            'id' => $deliveryPrice['id'],
        ],
    ];

    postJson('/api/v1/delivery-prices/delivery-prices:search-one', $requestBody)
        ->assertOk()
        ->assertJsonPath('data.id', $deliveryPrice['id']);
});

test('POST /api/v1/delivery-prices/delivery-prices:checkout 200', function () {
    /** @var Region $region */
    $region = Region::factory()->create();
    /** @var DeliveryPrice $deliveryPrice */
    $deliveryPrice = DeliveryPrice::factory()->create([
        'region_id' => $region->id,
        'region_guid' => $region->guid,
    ]);

    $request = CheckoutDeliveryPriceRequestFactory::new()->make([
        'region_guid' => $region->guid,
        'delivery_method' => $deliveryPrice->delivery_method,
    ]);

    postJson('/api/v1/delivery-prices/delivery-prices:checkout', $request)
        ->assertStatus(200)
        ->assertJsonPath("data.0.id", $deliveryPrice->id);
});

test('POST /api/v1/delivery-prices/delivery-prices:checkout empty region 200', function () {
    /** @var Region $region */
    $region = Region::factory()->create();
    /** @var DeliveryPrice $deliveryPrice */
    $deliveryPrice = DeliveryPrice::factory()->create([
        'federal_district_id' => $region->federal_district_id,
        'region_id' => null,
        'region_guid' => null,
    ]);

    $request = CheckoutDeliveryPriceRequestFactory::new()->make([
        'region_guid' => $region->guid,
        'delivery_method' => $deliveryPrice->delivery_method,
    ]);

    postJson('/api/v1/delivery-prices/delivery-prices:checkout', $request)
        ->assertStatus(200)
        ->assertJsonPath("data.0.id", $deliveryPrice->id);
});
