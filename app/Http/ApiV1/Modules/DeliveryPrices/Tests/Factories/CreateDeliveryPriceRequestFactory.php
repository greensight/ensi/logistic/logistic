<?php

namespace App\Http\ApiV1\Modules\DeliveryPrices\Tests\Factories;

use App\Http\ApiV1\OpenApiGenerated\Enums\DeliveryMethodEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\DeliveryServiceEnum;
use Ensi\LaravelTestFactories\BaseApiFactory;

class CreateDeliveryPriceRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'federal_district_id' => $this->faker->modelId(),
            'region_id' => $this->faker->optional()->modelId(),
            'region_guid' => $this->faker->optional()->uuid(),
            'delivery_service' => $this->faker->optional()->randomEnum(DeliveryServiceEnum::cases()),
            'delivery_method' => $this->faker->randomEnum(DeliveryMethodEnum::cases()),
            'price' => $this->faker->numberBetween(),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
