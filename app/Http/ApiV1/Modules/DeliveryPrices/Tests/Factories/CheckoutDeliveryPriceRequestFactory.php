<?php

namespace App\Http\ApiV1\Modules\DeliveryPrices\Tests\Factories;

use App\Http\ApiV1\OpenApiGenerated\Enums\DeliveryMethodEnum;
use Ensi\LaravelTestFactories\BaseApiFactory;

class CheckoutDeliveryPriceRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'region_guid' => $this->faker->uuid(),
            'delivery_method' => $this->faker->randomEnum(DeliveryMethodEnum::cases()),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
