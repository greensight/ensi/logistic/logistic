<?php

namespace App\Http\ApiV1\Modules\DeliveryPrices\Queries;

use App\Domain\DeliveryPrices\Models\DeliveryPrice;
use App\Http\ApiV1\Support\Sorts\FederalDistrictNameSort;
use App\Http\ApiV1\Support\Sorts\RegionNameSort;
use Ensi\QueryBuilderHelpers\Filters\NumericFilter;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\AllowedInclude;
use Spatie\QueryBuilder\AllowedSort;
use Spatie\QueryBuilder\QueryBuilder;

class DeliveryPricesQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(DeliveryPrice::query());

        $this->allowedSorts([
            'id',
            'price',
            AllowedSort::custom('region.name', new RegionNameSort()),
            AllowedSort::custom('federal_district.name', new FederalDistrictNameSort()),
        ]);

        $this->allowedIncludes([
            AllowedInclude::relationship('federal_district', 'federalDistrict'),
            'region',
        ]);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('federal_district_id'),
            AllowedFilter::exact('region_id'),
            AllowedFilter::exact('region_guid'),
            AllowedFilter::exact('delivery_service'),
            AllowedFilter::exact('delivery_method'),

            ...NumericFilter::make('price')->gte()->lte(),
        ]);

        $this->defaultSort('-id');
    }
}
