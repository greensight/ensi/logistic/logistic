<?php

namespace App\Http\ApiV1\Modules\DeliveryPrices\Requests;

use App\Domain\DeliveryPrices\Models\DeliveryPrice;
use App\Domain\Geos\Models\FederalDistrict;
use App\Domain\Geos\Models\Region;
use App\Http\ApiV1\OpenApiGenerated\Enums\DeliveryMethodEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\DeliveryServiceEnum;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Enum;

class CreateDeliveryPriceRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'federal_district_id' => [
                'required',
                'integer',
                Rule::unique(DeliveryPrice::class)->where(function ($query) {
                    return $query->where('region_id', $this->request->get('region_id'))
                        ->where('delivery_service', $this->request->get('delivery_service'))
                        ->where('delivery_method', $this->request->get('delivery_method'));
                }),
                'exists:' . (new FederalDistrict())->getTable() . ',id',
            ],
            'region_id' => ['nullable', 'integer', 'exists:' . (new Region())->getTable() . ',id'],
            'region_guid' => ['nullable', 'string', 'exists:' . (new Region())->getTable() . ',guid'],
            'delivery_service' => ['nullable', new Enum(DeliveryServiceEnum::class)],
            'delivery_method' => ['required', new Enum(DeliveryMethodEnum::class)],
            'price' => ['required', 'integer', 'min:0'],
        ];
    }
}
