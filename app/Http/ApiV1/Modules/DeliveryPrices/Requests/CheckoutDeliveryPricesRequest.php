<?php

namespace App\Http\ApiV1\Modules\DeliveryPrices\Requests;

use App\Domain\DeliveryPrices\Actions\DeliveryPrice\Data\CheckoutDeliveryPricesData;
use App\Http\ApiV1\OpenApiGenerated\Enums\DeliveryMethodEnum;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rules\Enum;

class CheckoutDeliveryPricesRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'delivery_method' => ['required', new Enum(DeliveryMethodEnum::class)],
            'region_guid' => ['required', 'string'],
        ];
    }

    public function convertToObject(): CheckoutDeliveryPricesData
    {
        return new CheckoutDeliveryPricesData(
            $this->string('region_guid'),
            $this->integer('delivery_method'),
        );
    }
}
