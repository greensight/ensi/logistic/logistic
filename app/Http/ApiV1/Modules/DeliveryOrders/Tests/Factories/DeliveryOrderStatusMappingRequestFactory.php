<?php

namespace App\Http\ApiV1\Modules\DeliveryOrders\Tests\Factories;

use App\Http\ApiV1\OpenApiGenerated\Enums\DeliveryOrderStatusEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\DeliveryServiceEnum;
use Ensi\LaravelTestFactories\BaseApiFactory;

class DeliveryOrderStatusMappingRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'delivery_service_id' => $this->faker->randomEnum(DeliveryServiceEnum::cases()),
            'status' => $this->faker->randomEnum(DeliveryOrderStatusEnum::cases()),
            'external_status' => $this->faker->name(),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
