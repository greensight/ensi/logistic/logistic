<?php

use App\Domain\DeliveryOrders\Enums\DeliveryOrderStatus;
use App\Domain\DeliveryOrders\Models\DeliveryOrder;
use App\Domain\DeliveryOrders\Models\DeliveryOrderPlace;
use App\Domain\DeliveryOrders\Models\DeliveryOrderPlaceItem;
use App\Domain\Kafka\Actions\Send\SendDeliveryOrderEventAction;
use App\Http\ApiV1\OpenApiGenerated\Enums\DeliveryOrderStatusEnum;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\LaravelTestFactories\FakerProvider;

use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test("POST /api/v1/delivery-orders/delivery-orders:search success", function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $this->mock(SendDeliveryOrderEventAction::class)->allows('execute');

    $deliveryOrders = DeliveryOrder::factory()
        ->count(10)
        ->sequence(
            ['status' => DeliveryOrderStatus::SHIPPED],
            ['status' => DeliveryOrderStatus::NEW],
        )
        ->create();
    $lastId = $deliveryOrders->last()->id;

    $requestBody = [
        "filter" => [
            "status" => DeliveryOrderStatusEnum::NEW->value,
        ],
        "sort" => [
            "-id",
        ],
    ];

    postJson("/api/v1/delivery-orders/delivery-orders:search", $requestBody)
        ->assertStatus(200)
        ->assertJsonCount(5, 'data')
        ->assertJsonPath('data.0.id', $lastId)
        ->assertJsonPath('data.0.status', DeliveryOrderStatusEnum::NEW->value);
})->with(FakerProvider::$optionalDataset);

test("POST /api/v1/delivery-orders/delivery-orders:search?include='places.items' success", function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $this->mock(SendDeliveryOrderEventAction::class)->allows('execute');

    /** @var DeliveryOrder $deliveryOrder */
    $deliveryOrder = DeliveryOrder::factory()->create();
    /** @var DeliveryOrderPlace $deliveryOrderPlace */
    $deliveryOrderPlace = DeliveryOrderPlace::factory()->for($deliveryOrder, 'order')->create();
    /** @var DeliveryOrderPlace $deliveryOrderPlaceItem */
    $deliveryOrderPlaceItem = DeliveryOrderPlaceItem::factory()->for($deliveryOrderPlace, 'place')->create();

    postJson("/api/v1/delivery-orders/delivery-orders:search", [
        "include" => ["places.items"],
    ])
        ->assertStatus(200)
        ->assertJsonPath('data.0.places.0.id', $deliveryOrderPlace->id)
        ->assertJsonPath('data.0.places.0.items.0.id', $deliveryOrderPlaceItem->id);
})->with(FakerProvider::$optionalDataset);

test("POST /api/v1/delivery-orders/delivery-orders:search-one success", function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $this->mock(SendDeliveryOrderEventAction::class)->allows('execute');

    /** @var DeliveryOrder $deliveryOrder */
    $deliveryOrder = DeliveryOrder::factory()->create();

    $requestBody = [
        "filter" => [
            "id" => $deliveryOrder['id'],
        ],
    ];

    postJson("/api/v1/delivery-orders/delivery-orders:search-one", $requestBody)
        ->assertStatus(200)
        ->assertJsonPath('data.id', $deliveryOrder->id)
        ->assertJsonPath('data.delivery_id', $deliveryOrder->delivery_id)
        ->assertJsonPath('data.delivery_service_id', $deliveryOrder->delivery_service_id)
        ->assertJsonPath('data.status', $deliveryOrder->status)
        ->assertJsonPath('data.sender_address.address_string', $deliveryOrder->sender_address->address_string);
})->with(FakerProvider::$optionalDataset);
