<?php

namespace App\Http\ApiV1\Modules\DeliveryOrders\Requests;

use App\Http\ApiV1\OpenApiGenerated\Enums\DeliveryOrderStatusEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\DeliveryServiceEnum;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rules\Enum;

class CreateDeliveryOrderStatusMappingRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'delivery_service_id' => ['required', new Enum(DeliveryServiceEnum::class)],
            'status' => ['required', new Enum(DeliveryOrderStatusEnum::class)],
            'external_status' => ['required'],
      ];
    }
}
