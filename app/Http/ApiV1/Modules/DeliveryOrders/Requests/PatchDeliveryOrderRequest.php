<?php

namespace App\Http\ApiV1\Modules\DeliveryOrders\Requests;

use App\Http\ApiV1\OpenApiGenerated\Enums\DeliveryOrderStatusEnum;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rules\Enum;

class PatchDeliveryOrderRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'status' => ['required', new Enum(DeliveryOrderStatusEnum::class)],
        ];
    }
}
