<?php

namespace App\Http\ApiV1\Modules\DeliveryOrders\Requests;

use App\Http\ApiV1\OpenApiGenerated\Enums\DeliveryOrderStatusEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\DeliveryServiceEnum;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rules\Enum;

class PatchDeliveryOrderStatusMappingRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'delivery_service_id' => ['sometimes', new Enum(DeliveryServiceEnum::class)],
            'status' => ['sometimes', new Enum(DeliveryOrderStatusEnum::class)],
            'external_status' => ['sometimes'],
        ];
    }
}
