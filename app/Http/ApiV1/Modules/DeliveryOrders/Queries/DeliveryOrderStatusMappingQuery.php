<?php

namespace App\Http\ApiV1\Modules\DeliveryOrders\Queries;

use App\Domain\DeliveryOrders\Models\DeliveryOrderStatusMapping;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\AllowedInclude;
use Spatie\QueryBuilder\QueryBuilder;

class DeliveryOrderStatusMappingQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(DeliveryOrderStatusMapping::query());

        $this->allowedSorts(['id', 'delivery_service_id', 'status']);

        $this->allowedIncludes([
            AllowedInclude::relationship('delivery_service', 'deliveryService'),
        ]);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('delivery_service_id'),
            AllowedFilter::exact('status'),
            AllowedFilter::exact('external_status'),
        ]);

        $this->defaultSort('-id');
    }
}
