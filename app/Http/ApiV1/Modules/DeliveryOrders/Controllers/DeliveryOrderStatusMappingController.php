<?php

namespace App\Http\ApiV1\Modules\DeliveryOrders\Controllers;

use App\Domain\DeliveryOrders\Actions\DeliveryOrderStatusMapping\CreateDeliveryOrderStatusMappingAction;
use App\Domain\DeliveryOrders\Actions\DeliveryOrderStatusMapping\DeleteDeliveryOrderStatusMappingAction;
use App\Domain\DeliveryOrders\Actions\DeliveryOrderStatusMapping\PatchDeliveryOrderStatusMappingAction;
use App\Http\ApiV1\Modules\DeliveryOrders\Queries\DeliveryOrderStatusMappingQuery;
use App\Http\ApiV1\Modules\DeliveryOrders\Requests\CreateDeliveryOrderStatusMappingRequest;
use App\Http\ApiV1\Modules\DeliveryOrders\Requests\PatchDeliveryOrderStatusMappingRequest;
use App\Http\ApiV1\Modules\DeliveryOrders\Resources\DeliveryOrderStatusMappingResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Contracts\Support\Responsable;

class DeliveryOrderStatusMappingController
{
    public function create(CreateDeliveryOrderStatusMappingRequest $request, CreateDeliveryOrderStatusMappingAction $action): Responsable
    {
        return new DeliveryOrderStatusMappingResource($action->execute($request->validated()));
    }

    public function patch(int $id, PatchDeliveryOrderStatusMappingRequest $request, PatchDeliveryOrderStatusMappingAction $action): Responsable
    {
        return new DeliveryOrderStatusMappingResource($action->execute($id, $request->validated()));
    }

    public function delete(int $id, DeleteDeliveryOrderStatusMappingAction $action): Responsable
    {
        $action->execute($id);

        return new EmptyResource();
    }

    public function get(int $id, DeliveryOrderStatusMappingQuery $query): Responsable
    {
        return new DeliveryOrderStatusMappingResource($query->findOrFail($id));
    }

    public function search(PageBuilderFactory $pageBuilderFactory, DeliveryOrderStatusMappingQuery $query): Responsable
    {
        return DeliveryOrderStatusMappingResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    public function searchOne(DeliveryOrderStatusMappingQuery $query): Responsable
    {
        return new DeliveryOrderStatusMappingResource($query->firstOrFail());
    }
}
