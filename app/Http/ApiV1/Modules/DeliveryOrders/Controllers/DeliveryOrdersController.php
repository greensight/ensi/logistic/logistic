<?php

namespace App\Http\ApiV1\Modules\DeliveryOrders\Controllers;

use App\Domain\DeliveryOrders\Actions\DeliveryOrder\PatchDeliveryOrderAction;
use App\Http\ApiV1\Modules\DeliveryOrders\Queries\DeliveryOrderQuery;
use App\Http\ApiV1\Modules\DeliveryOrders\Requests\PatchDeliveryOrderRequest;
use App\Http\ApiV1\Modules\DeliveryOrders\Resources\DeliveryOrdersResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use Illuminate\Contracts\Support\Responsable;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

/**
 * Контроллер для работы с заказами на доставки (от РЦ до места получения заказа)
 */
class DeliveryOrdersController
{
    public function search(PageBuilderFactory $pageBuilderFactory, DeliveryOrderQuery $query): AnonymousResourceCollection
    {
        return DeliveryOrdersResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    public function searchOne(DeliveryOrderQuery $query): DeliveryOrdersResource
    {
        return new DeliveryOrdersResource($query->firstOrFail());
    }

    public function patch(int $id, PatchDeliveryOrderRequest $request, PatchDeliveryOrderAction $action): Responsable
    {
        return new DeliveryOrdersResource($action->execute($id, $request->validated()));
    }
}
