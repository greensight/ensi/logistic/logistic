<?php

/**
 * NOTE: This file is auto generated by OpenAPI Generator.
 * Do NOT edit it manually. Run `php artisan openapi:generate-server`.
 */

namespace App\Http\ApiV1\OpenApiGenerated\Enums;

/**
 * Способ доставки. Расшифровка значений:
 * * `1` - Курьерская доставка (доставка до адреса клиента)
 * * `2` - Самовывоз из ПВЗ (доставка до ПВЗ)
 * * `3` - Самовывоз из магазина (самовывоз без доставки)
 */
enum DeliveryMethodEnum: int
{
    /** Курьерская доставка */
    case DELIVERY = 1;
    /** Самовывоз из ПВЗ */
    case PICKUP = 2;
    /** Самовывоз из магазина */
    case STORE = 3;
}
