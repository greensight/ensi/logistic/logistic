<?php

namespace App\Domain\Kafka\Messages\Listen;

use Illuminate\Support\Fluent;
use RdKafka\Message;

/**
 * @property array $dirty
 * @property mixed $attributes
 * @property string $event
 */
abstract class AbstractEventMessage extends Fluent
{
    public const CREATE = 'create';
    public const UPDATE = 'update';
    public const DELETE = 'delete';

    public function __construct($attributes = [])
    {
        $dtoClass = $this->getDtoClass();
        $attributes['attributes'] = new $dtoClass($attributes['attributes']);
        parent::__construct($attributes);
    }

    public static function makeFromRdKafka(Message $message): self
    {
        return new static(json_decode($message->payload, true));
    }

    abstract protected function getDtoClass(): string;
}
