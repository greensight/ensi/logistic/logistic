<?php

namespace App\Domain\Kafka\Messages\Listen;

use App\Domain\Kafka\Messages\Listen\Dtos\StorePickupTimeDto;

/**
 * @property StorePickupTimeDto $attributes
 */
class StorePickupTimeEventMessage extends AbstractEventMessage
{
    protected function getDtoClass(): string
    {
        return StorePickupTimeDto::class;
    }
}
