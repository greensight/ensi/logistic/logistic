<?php

namespace App\Domain\Kafka\Messages\Listen\Tests\Factories;

use App\Domain\Kafka\Messages\Listen\Dtos\Factories\StorePickupTimeDtoFactory;
use App\Http\ApiV1\Support\Tests\Factories\KafkaMessageFactory;

class StorePickupTimeEventMessageFactory extends KafkaMessageFactory
{
    protected function definition(): array
    {
        return array_merge(parent::definition(), [
            'attributes' => StorePickupTimeDtoFactory::new()->make()->toArray(),
        ]);
    }
}
