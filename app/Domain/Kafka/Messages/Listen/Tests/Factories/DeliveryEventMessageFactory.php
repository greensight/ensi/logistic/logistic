<?php

namespace App\Domain\Kafka\Messages\Listen\Tests\Factories;

use App\Domain\Kafka\Messages\Listen\Dtos\Factories\DeliveryDtoFactory;
use App\Http\ApiV1\Support\Tests\Factories\KafkaMessageFactory;

class DeliveryEventMessageFactory extends KafkaMessageFactory
{
    protected function definition(): array
    {
        return array_merge(parent::definition(), [
            'attributes' => DeliveryDtoFactory::new()->make()->toArray(),
        ]);
    }
}
