<?php

namespace App\Domain\Kafka\Messages\Listen\Dtos;

use App\Domain\Kafka\Messages\Listen\Dtos\Factories\TimeslotDtoFactory;
use Illuminate\Support\Fluent;

/**
 * Класс-dto для сущности "Таймслот"
 * Class TimeslotDto
 *
 * @property string $id
 * @property string $from
 * @property string $to
 */
class TimeslotDto extends Fluent
{
    public static function factory(): TimeslotDtoFactory
    {
        return TimeslotDtoFactory::new();
    }
}
