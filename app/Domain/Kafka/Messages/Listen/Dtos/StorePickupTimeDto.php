<?php

namespace App\Domain\Kafka\Messages\Listen\Dtos;

use Illuminate\Support\Carbon;

/**
 * Класс-dto для сущности "Склады - время отгрузки грузов"
 * Class StorePickupTime
 *
 * @property int $external_id - id времени отгрузки груза в bu
 * @property int $store_id - id склада
 * @property int $day - день недели (1-7)
 * @property string $pickup_time_code - код времени отгрузки у службы доставки
 * @property Carbon $pickup_time_start - время начала отгрузки
 * @property Carbon $pickup_time_end - время окончания отгрузки
 * @property Carbon $cargo_export_time - время выгрузки информации о грузе в службу доставки
 * @property int $delivery_service_id - идентификатор службы доставки
 */
class StorePickupTimeDto extends AbstractDto
{
    public function __construct($attributes = [])
    {
        $attributes['external_id'] = $attributes['id'];

        parent::__construct($attributes);

        $this->casts = array_merge($this->casts, [
            'pickup_time_start' => 'datetime',
            'pickup_time_end' => 'datetime',
            'cargo_export_time' => 'datetime',
        ]);
    }
}
