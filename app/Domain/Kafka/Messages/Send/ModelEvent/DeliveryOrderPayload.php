<?php

namespace App\Domain\Kafka\Messages\Send\ModelEvent;

use App\Domain\DeliveryOrders\Models\DeliveryOrder;
use App\Domain\Kafka\Messages\Send\Payload;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

class DeliveryOrderPayload extends Payload
{
    public function __construct(protected DeliveryOrder $model)
    {
    }

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->model->id,

            'delivery_id' => $this->model->delivery_id,
            'delivery_service_id' => $this->model->delivery_service_id,
            'customer_id' => $this->model->customer_id,

            'number' => $this->model->number,
            'external_id' => $this->model->external_id,
            'error_external_id' => $this->model->error_external_id,

            'status' => $this->model->status,
            'status_at' => $this->model->status_at->toJSON(),

            'external_status' => $this->model->external_status,
            'external_status_at' => $this->model->external_status_at?->toJSON(),

            'height' => $this->model->height,
            'length' => $this->model->length,
            'width' => $this->model->width,
            'weight' => $this->model->weight,

            'shipment_method' => $this->model->shipment_method,
            'delivery_method' => $this->model->delivery_method,

            'delivery_date' => $this->model->delivery_date?->format(BaseJsonResource::DATE_FORMAT),

            'point_in_id' => $this->model->point_in_id,
            'point_out_id' => $this->model->point_out_id,
            'shipment_time_start' => $this->model->shipment_time_start?->format('H:i'),
            'shipment_time_end' => $this->model->shipment_time_end?->format('H:i'),
            'delivery_time_start' => $this->model->delivery_time_start?->format('H:i'),
            'delivery_time_end' => $this->model->delivery_time_end?->format('H:i'),
            'delivery_time_code' => $this->model->delivery_time_code,

            'description' => $this->model->description,

            'assessed_cost' => $this->model->assessed_cost,
            'delivery_cost' => $this->model->delivery_cost,
            'delivery_cost_vat' => $this->model->delivery_cost_vat,
            'delivery_cost_pay' => $this->model->delivery_cost_pay,
            'cod_cost' => $this->model->cod_cost,
            'is_delivery_payed_by_recipient' => $this->model->is_delivery_payed_by_recipient,

            'sender_is_seller' => $this->model->sender_is_seller,
            'sender_inn' => $this->model->sender_inn,
            'sender_address' => $this->model->sender_address?->toArray(),
            'sender_company_name' => $this->model->sender_company_name,
            'sender_contact_name' => $this->model->sender_contact_name,
            'sender_email' => $this->model->sender_email,
            'sender_phone' => $this->model->sender_phone,
            'sender_comment' => $this->model->sender_comment,

            'recipient_address' => $this->model->recipient_address?->toArray(),
            'recipient_company_name' => $this->model->recipient_company_name,
            'recipient_contact_name' => $this->model->recipient_contact_name,
            'recipient_email' => $this->model->recipient_email,
            'recipient_phone' => $this->model->recipient_phone,
            'recipient_comment' => $this->model->recipient_comment,

            'created_at' => $this->model->created_at?->toJSON(),
            'updated_at' => $this->model->updated_at?->toJSON(),
        ];
    }
}
