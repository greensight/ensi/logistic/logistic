<?php

use App\Domain\DeliveryOrders\Models\DeliveryOrder;
use App\Domain\Kafka\Actions\Send\SendDeliveryOrderEventAction;
use App\Domain\Kafka\Messages\Send\ModelEvent\DeliveryOrderPayload;
use App\Domain\Kafka\Messages\Send\ModelEvent\ModelEventMessage;
use Ensi\LaravelTestFactories\FakerProvider;

use function PHPUnit\Framework\assertEquals;
use function PHPUnit\Framework\assertIsArray;
use function PHPUnit\Framework\assertNull;

use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class);
uses()->group('unit', 'kafka');

test("generate DeliveryOrderEventMessage success", function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $this->mock(SendDeliveryOrderEventAction::class)->allows('execute');

    /** @var DeliveryOrder $model */
    $model = DeliveryOrder::factory()->create();
    $payload = new DeliveryOrderPayload($model);
    assertEquals($payload->jsonSerialize(), $model->attributesToArray());

    $modelEvent = new ModelEventMessage($model, $payload, ModelEventMessage::CREATE, 'test');
    assertNull($modelEvent->dirty);

    $modelEvent = new ModelEventMessage($model, $payload, ModelEventMessage::UPDATE, 'test');
    assertIsArray($modelEvent->dirty);
})->with(FakerProvider::$optionalDataset);
