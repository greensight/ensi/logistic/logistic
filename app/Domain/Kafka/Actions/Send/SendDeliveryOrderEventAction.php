<?php

namespace App\Domain\Kafka\Actions\Send;

use App\Domain\DeliveryOrders\Models\DeliveryOrder;
use App\Domain\Kafka\Messages\Send\ModelEvent\DeliveryOrderPayload;
use App\Domain\Kafka\Messages\Send\ModelEvent\ModelEventMessage;

class SendDeliveryOrderEventAction
{
    public function __construct(protected SendMessageAction $action)
    {
    }

    public function execute(DeliveryOrder $model, string $event): void
    {
        $modelEvent = new ModelEventMessage($model, new DeliveryOrderPayload($model), $event, 'delivery-orders');
        $this->action->execute($modelEvent);
    }
}
