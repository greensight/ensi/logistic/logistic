<?php

use App\Domain\Common\Tests\Factories\Bu\StoreFactory;
use App\Domain\Common\Tests\Factories\Oms\DeliveryFactory;
use App\Domain\Common\Tests\Factories\Oms\OrderFactory;
use App\Domain\Common\Tests\Factories\Oms\ShipmentFactory;
use App\Domain\DeliveryOrders\Enums\DeliveryOrderStatus;
use App\Domain\DeliveryOrders\Models\DeliveryOrder;
use App\Domain\DeliveryServices\Enums\DeliveryMethod;
use App\Domain\DeliveryServices\Enums\DeliveryService;
use App\Domain\Kafka\Actions\Listen\DeliveryListenAction;
use App\Domain\Kafka\Actions\Send\SendDeliveryOrderEventAction;
use App\Domain\Kafka\Messages\Listen\DeliveryEventMessage;
use App\Domain\Kafka\Messages\Listen\Tests\Factories\DeliveryEventMessageFactory;
use App\Http\ApiV1\OpenApiGenerated\Enums\DeliveryMethodEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\DeliveryOrderStatusEnum;
use Ensi\LaravelTestFactories\FakerProvider;
use Ensi\OmsClient\Dto\DeliveryStatusEnum;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertDatabaseMissing;

use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class);
uses()->group('integration', 'kafka', 'kafka-listen-delivery');

test("Action DeliveryListenAction create success", function (DeliveryMethod $deliveryMethod, DeliveryMethodEnum $orderDeliveryMethod, ?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var IntegrationTestCase $this */
    $deliveryId = 1;
    $message = DeliveryEventMessageFactory::new()->make([
        'event' => DeliveryEventMessage::CREATE,
        'attributes' => [
            'id' => $deliveryId,
            'status' => DeliveryStatusEnum::ASSEMBLED,
            'delivery_method' => $deliveryMethod,
        ],
    ]);

    $shipment = ShipmentFactory::new()->withOrderItems()->make(['delivery_id' => $deliveryId]);
    $delivery = DeliveryFactory::new()->withShipments([$shipment])->make(['id' => $deliveryId]);
    $this->mockOrdersApi()->allows([
        'getOrder' => OrderFactory::new()
            ->withDeliveries([$delivery])
            ->withDeliveryMethod($orderDeliveryMethod->value)
            ->makeResponse(['delivery_service' => DeliveryService::TK1]),
    ]);
    $this->mockStoresApi()->allows([
        'getStore' => StoreFactory::new()->withContacts()->makeResponse(['id' => $shipment->getStoreId()]),
    ]);

    $this->mock(SendDeliveryOrderEventAction::class)->shouldReceive('execute');

    resolve(DeliveryListenAction::class)->execute($message);

    assertDatabaseHas((new DeliveryOrder())->getTable(), [
        'delivery_id' => $deliveryId,
        'status' => DeliveryOrderStatus::NEW,
    ]);
})->with([
    [DeliveryMethod::METHOD_DELIVERY, DeliveryMethodEnum::DELIVERY],
    [DeliveryMethod::METHOD_PICKUP, DeliveryMethodEnum::PICKUP],
], FakerProvider::$optionalDataset);

test("Action DeliveryListenAction not create success", function (int $status, DeliveryMethod $deliveryMethod) {
    /** @var IntegrationTestCase $this */
    $deliveryId = 1;
    $message = DeliveryEventMessageFactory::new()->make([
        'event' => DeliveryEventMessage::CREATE,
        'attributes' => [
            'id' => $deliveryId,
            'status' => $status,
            'delivery_method' => $deliveryMethod,
        ],
    ]);

    resolve(DeliveryListenAction::class)->execute($message);

    assertDatabaseMissing((new DeliveryOrder())->getTable(), [
        'delivery_id' => $deliveryId,
    ]);
})->with([
    'status does not allow save' => [DeliveryStatusEnum::NEW, DeliveryMethod::METHOD_DELIVERY],
    'delivery_method does not allow save' => [DeliveryStatusEnum::ASSEMBLED, DeliveryMethod::METHOD_STORE],
]);

test("Action DeliveryListenAction update success", function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $oldHeight = 1000;
    $newHeight = 900;

    /** @var DeliveryOrder $deliveryOrder */
    $deliveryOrder = DeliveryOrder::factory()->notDone()->createQuietly(['height' => $oldHeight]);

    /** @var IntegrationTestCase $this */
    $message = DeliveryEventMessageFactory::new()->make([
        'event' => DeliveryEventMessage::UPDATE,
        'attributes' => [
            'id' => $deliveryOrder->delivery_id,
            'status' => DeliveryStatusEnum::ASSEMBLED,
        ],
    ]);

    $shipment = ShipmentFactory::new()->withOrderItems()->make(['delivery_id' => $deliveryOrder->delivery_id]);
    $delivery = DeliveryFactory::new()->withShipments([$shipment])->make(['id' => $deliveryOrder->delivery_id, 'height' => $newHeight]);
    $this->mockOrdersApi()->allows([
        'getOrder' => OrderFactory::new()
            ->withDeliveries([$delivery])
            ->withDeliveryMethod($deliveryOrder->delivery_method)
            ->makeResponse(['delivery_service' => DeliveryService::TK1]),
    ]);
    $this->mockStoresApi()->allows([
        'getStore' => StoreFactory::new()->withContacts()->makeResponse(['id' => $shipment->getStoreId()]),
    ]);

    $this->mock(SendDeliveryOrderEventAction::class)->shouldReceive('execute');

    resolve(DeliveryListenAction::class)->execute($message);

    assertDatabaseHas((new DeliveryOrder())->getTable(), [
        'delivery_id' => $deliveryOrder->delivery_id,
        'status' => $deliveryOrder->status,
        'height' => $newHeight,
    ]);
})->with(FakerProvider::$optionalDataset);

test("Action DeliveryListenAction cancel success", function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var DeliveryOrder $deliveryOrder */
    $deliveryOrder = DeliveryOrder::factory()->notDone()->createQuietly();

    /** @var IntegrationTestCase $this */
    $message = DeliveryEventMessageFactory::new()->make([
        'event' => DeliveryEventMessage::UPDATE,
        'attributes' => [
            'id' => $deliveryOrder->delivery_id,
            'status' => DeliveryStatusEnum::CANCELED,
        ],
    ]);

    $this->mock(SendDeliveryOrderEventAction::class)->shouldReceive('execute');

    resolve(DeliveryListenAction::class)->execute($message);

    assertDatabaseHas((new DeliveryOrder())->getTable(), [
        'delivery_id' => $deliveryOrder->delivery_id,
        'status' => DeliveryOrderStatusEnum::CANCELED,
    ]);
})->with(FakerProvider::$optionalDataset);

test("Action DeliveryListenAction delete success", function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var DeliveryOrder $deliveryOrder */
    $deliveryOrder = DeliveryOrder::factory()->notDone()->createQuietly();

    /** @var IntegrationTestCase $this */
    $message = DeliveryEventMessageFactory::new()->make([
        'event' => DeliveryEventMessage::DELETE,
        'attributes' => [
            'id' => $deliveryOrder->delivery_id,
            'status' => DeliveryStatusEnum::CANCELED,
        ],
    ]);

    $this->mock(SendDeliveryOrderEventAction::class)->shouldReceive('execute');

    resolve(DeliveryListenAction::class)->execute($message);

    assertDatabaseHas((new DeliveryOrder())->getTable(), [
        'delivery_id' => $deliveryOrder->delivery_id,
        'status' => DeliveryOrderStatusEnum::CANCELED,
    ]);
})->with(FakerProvider::$optionalDataset);
