<?php

use App\Domain\CargoOrders\Models\StorePickupTime;
use App\Domain\Kafka\Actions\Listen\StorePickupTimeListenAction;
use App\Domain\Kafka\Messages\Listen\StorePickupTimeEventMessage;
use App\Domain\Kafka\Messages\Listen\Tests\Factories\StorePickupTimeEventMessageFactory;
use Illuminate\Support\Arr;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertModelMissing;

use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class);
uses()->group('integration', 'kafka', 'kafka-listen-store-pickup-time');

test("Action StorePickupTimeListenAction create success", function () {
    /** @var IntegrationTestCase $this */
    $storePickupTimeExternalId = 1;
    $message = StorePickupTimeEventMessageFactory::new()->make([
        'event' => StorePickupTimeEventMessage::CREATE,
        'attributes' => [
            'id' => $storePickupTimeExternalId,
        ],
    ]);

    resolve(StorePickupTimeListenAction::class)->execute($message);

    assertDatabaseHas((new StorePickupTime())->getTable(), [
        'external_id' => $storePickupTimeExternalId,
    ]);
});

test("Action StorePickupTimeListenAction patch success", function () {
    /** @var IntegrationTestCase $this */
    $storePickupTimeExternalId = 1;
    /** @var StorePickupTime $storePickupTime */
    $storePickupTime = StorePickupTime::factory()->create(['external_id' => $storePickupTimeExternalId]);

    $message = StorePickupTimeEventMessageFactory::new()->make([
        'event' => StorePickupTimeEventMessage::UPDATE,
        'attributes' => [
            'id' => $storePickupTimeExternalId,
        ],
    ]);
    $messageData = StorePickupTimeEventMessage::makeFromRdKafka($message);

    $messageUpdateData = Arr::only($messageData->attributes->toArray(), $messageData->dirty);
    resolve(StorePickupTimeListenAction::class)->execute($message);
    $data = array_merge($storePickupTime->toArray(), $messageUpdateData);
    unset($data['created_at']);
    unset($data['updated_at']);

    assertDatabaseHas(
        (new StorePickupTime())->getTable(),
        $data
    );
});

test("Action StorePickupTimeListenAction delete success", function () {
    /** @var IntegrationTestCase $this */
    $storePickupTimeExternalId = 1;
    /** @var StorePickupTime $storePickupTime */
    $storePickupTime = StorePickupTime::factory()->create(['external_id' => $storePickupTimeExternalId]);

    $message = StorePickupTimeEventMessageFactory::new()->make([
        'event' => StorePickupTimeEventMessage::DELETE,
        'attributes' => [
            'id' => $storePickupTimeExternalId,
        ],
    ]);

    resolve(StorePickupTimeListenAction::class)->execute($message);

    assertModelMissing($storePickupTime);
});
