<?php

namespace App\Domain\DeliveryOrders\Models\Tests\Factories;

use App\Domain\DeliveryOrders\Enums\DeliveryOrderStatus;
use App\Domain\DeliveryOrders\Models\DeliveryOrderStatusMapping;
use App\Domain\DeliveryServices\Enums\DeliveryService;
use Ensi\LaravelTestFactories\BaseModelFactory;

class DeliveryOrderStatusMappingFactory extends BaseModelFactory
{
    protected $model = DeliveryOrderStatusMapping::class;

    public function definition(): array
    {
        return [
            'delivery_service_id' => $this->faker->randomEnum(DeliveryService::cases()),
            'status' => $this->faker->randomElement(DeliveryOrderStatus::validValues()),
            'external_status' => $this->faker->text(50),
        ];
    }
}
