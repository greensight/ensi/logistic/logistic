<?php

namespace App\Domain\DeliveryOrders\Models\Tests\Factories;

use App\Domain\DeliveryOrders\Models\DeliveryOrderPlace;
use App\Domain\DeliveryOrders\Models\DeliveryOrderPlaceItem;
use Ensi\LaravelTestFactories\BaseModelFactory;

class DeliveryOrderPlaceItemFactory extends BaseModelFactory
{
    protected $model = DeliveryOrderPlaceItem::class;

    public function definition(): array
    {
        $qty = $this->faker->randomFloat(2, 1, 100);

        return [
            'delivery_order_place_id' => DeliveryOrderPlace::factory(),

            'vendor_code' => $this->faker->nullable()->numerify('############'),
            'barcode' => $this->faker->nullable()->numerify('############'),
            'name' => $this->faker->text(50),
            'qty' => $qty,
            'qty_delivered' => $this->faker->nullable()->randomFloat(2, 1, $qty),
            'height' => $this->faker->numberBetween(10, 100),
            'length' => $this->faker->numberBetween(10, 100),
            'width' => $this->faker->numberBetween(10, 100),
            'weight' => $this->faker->numberBetween(500, 1000),
            'assessed_cost' => $this->faker->numberBetween(500, 1000),
            'cost' => $this->faker->numberBetween(500, 1000),
            'cost_vat' => $this->faker->randomElement([0, 10, 20]),
            'price' => $this->faker->numberBetween(500, 1000),
        ];
    }
}
