<?php

namespace App\Domain\DeliveryOrders\Models\Tests\Factories;

use App\Domain\DeliveryOrders\Enums\DeliveryOrderStatus;
use App\Domain\DeliveryOrders\Models\DeliveryOrder;
use App\Domain\DeliveryServices\Enums\DeliveryMethod;
use App\Domain\DeliveryServices\Enums\DeliveryService;
use App\Domain\DeliveryServices\Enums\ShipmentMethod;
use App\Domain\DeliveryServices\Models\Point;
use App\Domain\Support\Data\Address;
use Ensi\LaravelTestFactories\BaseModelFactory;

class DeliveryOrderFactory extends BaseModelFactory
{
    protected $model = DeliveryOrder::class;

    public function definition(): array
    {
        return [
            'delivery_id' => $this->faker->unique()->numberBetween(1000, 100000),
            'delivery_service_id' => $this->faker->randomEnum(DeliveryService::cases()),
            'customer_id' => $this->faker->modelId(),

            'number' => $this->faker->unique()->numerify('######-#'),
            'external_id' => $this->faker->unique()->numerify('#########'),
            'error_external_id' => $this->faker->nullable()->text(),
            'status' => $this->faker->randomElement(DeliveryOrderStatus::validValues()),
            'status_at' => $this->faker->dateTime(),
            'external_status' => $this->faker->text(50),
            'height' => $this->faker->numberBetween(10, 100),
            'length' => $this->faker->numberBetween(10, 100),
            'width' => $this->faker->numberBetween(10, 100),
            'weight' => $this->faker->numberBetween(500, 1000),
            'shipment_method' => $this->faker->randomElement(ShipmentMethod::validValues()),
            'delivery_method' => $this->faker->randomEnum(DeliveryMethod::cases()),
            'delivery_date' => $this->faker->date(),
            'point_in_id' => Point::factory(),
            'point_out_id' => Point::factory(),
            'shipment_time_start' => $this->faker->time('H:i'),
            'shipment_time_end' => $this->faker->time('H:i'),
            'delivery_time_start' => $this->faker->time('H:i'),
            'delivery_time_end' => $this->faker->time('H:i'),
            'delivery_time_code' => $this->faker->randomNumber(),
            'description' => $this->faker->nullable()->text(),

            'assessed_cost' => $this->faker->numberBetween(1000, 10000),
            'delivery_cost' => $this->faker->numberBetween(0, 2000),
            'delivery_cost_vat' => $this->faker->randomElement([0, 10, 20]),
            'delivery_cost_pay' => $this->faker->numberBetween(0, 2000),
            'cod_cost' => $this->faker->numberBetween(0, 2000),
            'is_delivery_payed_by_recipient' => $this->faker->boolean(),

            'sender_is_seller' => $this->faker->boolean(),
            'sender_inn' => $this->faker->numerify('##########'),
            'sender_address' => Address::factory()->make(),
            'sender_company_name' => $this->faker->company(),
            'sender_contact_name' => $this->faker->name(),
            'sender_email' => $this->faker->email(),
            'sender_phone' => $this->faker->numerify('+7##########'),
            'sender_comment' => $this->faker->text(),

            'recipient_address' => $this->faker->boolean() ? Address::factory()->make() : null,
            'recipient_company_name' => $this->faker->nullable()->company(),
            'recipient_contact_name' => $this->faker->nullable()->name(),
            'recipient_email' => $this->faker->nullable()->email(),
            'recipient_phone' => $this->faker->nullable()->numerify('+7##########'),
            'recipient_comment' => $this->faker->nullable()->text(),
        ];
    }

    public function withPersonalData(): self
    {
        return $this->state([
            'recipient_address' => Address::factory()->make(),
            'recipient_company_name' => $this->faker->company(),
            'recipient_contact_name' => $this->faker->name(),
            'recipient_email' => $this->faker->email(),
            'recipient_phone' => $this->faker->numerify('+7##########'),
            'recipient_comment' => $this->faker->text(),
        ]);
    }

    public function done(): self
    {
        return $this->state([
            'status' => $this->faker->randomElement(DeliveryOrderStatus::done()),
        ]);
    }

    public function notDone(): self
    {
        $notDoneStatuses = array_diff(DeliveryOrderStatus::validValues(), DeliveryOrderStatus::done());

        return $this->state([
            'status' => $this->faker->randomElement($notDoneStatuses),
        ]);
    }
}
