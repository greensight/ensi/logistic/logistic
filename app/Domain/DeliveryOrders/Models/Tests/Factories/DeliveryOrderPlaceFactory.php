<?php

namespace App\Domain\DeliveryOrders\Models\Tests\Factories;

use App\Domain\DeliveryOrders\Models\DeliveryOrder;
use App\Domain\DeliveryOrders\Models\DeliveryOrderPlace;
use Ensi\LaravelTestFactories\BaseModelFactory;

class DeliveryOrderPlaceFactory extends BaseModelFactory
{
    protected $model = DeliveryOrderPlace::class;

    public function definition(): array
    {
        return [
            'delivery_order_id' => DeliveryOrder::factory(),

            'number' => $this->faker->unique()->numerify('######-#-#'),
            'barcode' => $this->faker->nullable()->numerify('############'),
            'height' => $this->faker->numberBetween(10, 100),
            'length' => $this->faker->numberBetween(10, 100),
            'width' => $this->faker->numberBetween(10, 100),
            'weight' => $this->faker->numberBetween(500, 1000),
        ];
    }
}
