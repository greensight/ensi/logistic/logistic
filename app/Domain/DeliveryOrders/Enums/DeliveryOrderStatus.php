<?php

namespace App\Domain\DeliveryOrders\Enums;

/**
 * Статус заказа на доставку
 */
class DeliveryOrderStatus
{
    /** @var int - новый */
    public const NEW = 10;
    /** @var int - передан для доставки */
    public const SHIPPED = 20;
    /** @var int - готов к выдаче */
    public const READY_FOR_RECIPIENT = 30;
    /** @var int - получен */
    public const DONE = 100;
    /** @var int - отменен */
    public const CANCELED = 200;

    public function __construct(protected int $id, protected string $name)
    {
    }

    /**
     * @return DeliveryOrderStatus[]
     */
    public static function all(): array
    {
        return [
            new self(self::NEW, 'Новый'),
            new self(self::SHIPPED, 'Передан для доставки'),
            new self(self::READY_FOR_RECIPIENT, 'Готов к выдаче'),
            new self(self::DONE, 'Получен'),
            new self(self::CANCELED, 'Отменен'),
        ];
    }

    public static function validValues(): array
    {
        return [
            self::NEW,
            self::SHIPPED,
            self::READY_FOR_RECIPIENT,
            self::DONE,
            self::CANCELED,
        ];
    }

    public static function done(): array
    {
        return [
            self::DONE,
            self::CANCELED,
        ];
    }
}
