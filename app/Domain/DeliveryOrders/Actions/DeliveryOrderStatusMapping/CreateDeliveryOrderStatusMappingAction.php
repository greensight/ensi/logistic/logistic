<?php

namespace App\Domain\DeliveryOrders\Actions\DeliveryOrderStatusMapping;

use App\Domain\DeliveryOrders\Models\DeliveryOrderStatusMapping;

class CreateDeliveryOrderStatusMappingAction
{
    public function execute(array $fields): DeliveryOrderStatusMapping
    {
        $deliveryOrderStatusMapping = new DeliveryOrderStatusMapping();
        $deliveryOrderStatusMapping->fill($fields);
        $deliveryOrderStatusMapping->save();

        return $deliveryOrderStatusMapping;
    }
}
