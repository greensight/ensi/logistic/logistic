<?php

namespace App\Domain\DeliveryOrders\Actions\DeliveryOrderStatusMapping;

use App\Domain\DeliveryOrders\Models\DeliveryOrderStatusMapping;

class PatchDeliveryOrderStatusMappingAction
{
    public function execute(int $id, array $fields): DeliveryOrderStatusMapping
    {
        /** @var DeliveryOrderStatusMapping $deliveryOrderStatusMapping */
        $deliveryOrderStatusMapping = DeliveryOrderStatusMapping::query()->findOrFail($id);
        $deliveryOrderStatusMapping->fill($fields);
        $deliveryOrderStatusMapping->save();

        return $deliveryOrderStatusMapping;
    }
}
