<?php

namespace App\Domain\DeliveryOrders\Actions\DeliveryOrder;

use App\Domain\DeliveryOrders\Models\DeliveryOrderPlace;
use App\Domain\DeliveryOrders\Models\DeliveryOrderPlaceItem;
use Throwable;

class CreateDeliveryOrderPlaceItemAction
{
    /**
     * @throws Throwable
     */
    public function execute(DeliveryOrderPlace $deliveryOrderPlace, array $fields): DeliveryOrderPlaceItem
    {
        $deliveryOrderPlaceItem = new DeliveryOrderPlaceItem();
        $deliveryOrderPlaceItem->fill($fields);
        $deliveryOrderPlaceItem->delivery_order_place_id = $deliveryOrderPlace->id;

        return $deliveryOrderPlaceItem;
    }
}
