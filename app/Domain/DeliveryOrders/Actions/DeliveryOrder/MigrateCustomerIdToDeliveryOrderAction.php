<?php

namespace App\Domain\DeliveryOrders\Actions\DeliveryOrder;

use App\Domain\DeliveryOrders\Models\DeliveryOrder;
use Ensi\OmsClient\Api\OrdersApi;
use Ensi\OmsClient\Dto\Delivery;
use Ensi\OmsClient\Dto\PaginationTypeEnum as OmsPaginationTypeEnum;
use Ensi\OmsClient\Dto\RequestBodyPagination as OmsRequestBodyPagination;
use Ensi\OmsClient\Dto\SearchOrdersRequest;
use Generator;

class MigrateCustomerIdToDeliveryOrderAction
{
    private const CHUNK_SIZE = 50;

    public function __construct(protected readonly OrdersApi $ordersApi)
    {
    }

    public function execute(): void
    {
        foreach ($this->loadOrders() as $order) {
            $orderDeliveries = $order->getDeliveries();

            if (is_null($orderDeliveries)) {
                continue;
            }

            $orderDeliveryIds = array_map(function (Delivery $orderDelivery) {
                return $orderDelivery->getId();
            }, $orderDeliveries);

            DeliveryOrder::query()
                ->whereIn('delivery_id', $orderDeliveryIds)
                ->update(['customer_id' => $order->getCustomerId()]);
        }
    }

    protected function loadOrders(): Generator
    {
        $searchOrdersRequest = new SearchOrdersRequest();
        $paginationRequest = new OmsRequestBodyPagination();
        $paginationRequest
            ->setType(OmsPaginationTypeEnum::CURSOR)
            ->setLimit(self::CHUNK_SIZE);

        $searchOrdersRequest
            ->setInclude(['deliveries'])
            ->setPagination($paginationRequest);

        do {
            $response = $this->ordersApi->searchOrders($searchOrdersRequest);

            foreach ($response->getData() as $order) {
                yield $order;
            }

            $nextCursor = $response->getMeta()->getPagination()->getNextCursor();
            $searchOrdersRequest->getPagination()->setCursor($nextCursor);
        } while (!is_null($nextCursor));
    }
}
