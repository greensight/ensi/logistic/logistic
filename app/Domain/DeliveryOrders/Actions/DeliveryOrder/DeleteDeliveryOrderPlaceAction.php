<?php

namespace App\Domain\DeliveryOrders\Actions\DeliveryOrder;

use App\Domain\DeliveryOrders\Models\DeliveryOrderPlace;

class DeleteDeliveryOrderPlaceAction
{
    public function execute(int $deliveryPlaceId): void
    {
        /** @var DeliveryOrderPlace $deliveryOrderPlace */
        $deliveryOrderPlace = DeliveryOrderPlace::query()->findOrFail($deliveryPlaceId);
        $deliveryOrderPlace->delete();
    }
}
