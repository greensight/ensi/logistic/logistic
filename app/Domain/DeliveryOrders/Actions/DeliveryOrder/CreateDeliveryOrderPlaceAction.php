<?php

namespace App\Domain\DeliveryOrders\Actions\DeliveryOrder;

use App\Domain\DeliveryOrders\Models\DeliveryOrder;
use App\Domain\DeliveryOrders\Models\DeliveryOrderPlace;
use Illuminate\Support\Facades\DB;

class CreateDeliveryOrderPlaceAction
{
    public function __construct(
        protected CreateDeliveryOrderPlaceItemAction $createItemAction,
        protected CalcDeliveryOrderPlaceSizesAction $calcDeliveryOrderPlaceSizesAction,
    ) {
    }

    public function execute(DeliveryOrder $deliveryOrder, array $fields): DeliveryOrderPlace
    {
        return DB::transaction(function () use ($deliveryOrder, $fields) {
            $deliveryOrderPlace = new DeliveryOrderPlace();
            $deliveryOrderPlace->fill($fields);
            $deliveryOrderPlace->delivery_order_id = $deliveryOrder->id;
            $deliveryOrderPlace->saveQuietly();

            foreach ($fields['items'] as $item) {
                $this->createItemAction->execute($deliveryOrderPlace, $item);
            }

            $this->calcDeliveryOrderPlaceSizesAction->execute($deliveryOrderPlace);

            return $deliveryOrderPlace;
        });
    }
}
