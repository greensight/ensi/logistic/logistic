<?php

namespace App\Domain\DeliveryOrders\Actions\DeliveryOrder;

use App\Domain\DeliveryOrders\Enums\DeliveryOrderStatus;
use App\Domain\DeliveryOrders\Models\DeliveryOrder;
use App\Domain\Kafka\Messages\Listen\Dtos\DeliveryDto;
use App\Domain\Support\Data\Address;
use Ensi\OmsClient\Dto\DeliveryStatusEnum;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Throwable;

class CreateDeliveryOrderAction
{
    public function __construct(
        protected CreateDeliveryOrderPlaceAction $createPlaceAction
    ) {
    }

    /**
     * @throws Throwable
     */
    public function execute(array $fields): DeliveryOrder
    {
        return DB::transaction(function () use ($fields) {
            if (is_array($fields['sender_address'])) {
                $fields['sender_address'] = new Address($fields['sender_address']);
            }
            if (is_array($fields['recipient_address'])) {
                $fields['recipient_address'] = new Address($fields['recipient_address']);
            }
            $fields['status'] = $fields['status'] ?? DeliveryOrderStatus::NEW;
            /** @var DeliveryOrder $deliveryOrder */
            $deliveryOrder = DeliveryOrder::create(Arr::only($fields, DeliveryOrder::FILLABLE));

            foreach ($fields['places'] as $place) {
                $this->createPlaceAction->execute($deliveryOrder, $place);
            }

            $deliveryOrder->refresh();
            $deliveryOrder->loadMissing('places.items');

            return $deliveryOrder;
        });
    }

    /**
     * @throws Throwable
     */
    public function createFromDeliveryDto(DeliveryDto $deliveryDto): ?DeliveryOrder
    {
        if ($deliveryDto->status != DeliveryStatusEnum::ASSEMBLED) {
            return null;
        }

        if (!$deliveryDto->delivery_method->isDelivery()) {
            return null;
        }

        return $this->execute($deliveryDto->toDeliveryOrderFields());
    }
}
