<?php

namespace App\Domain\DeliveryOrders\Actions\DeliveryOrder;

use App\Domain\DeliveryOrders\Models\DeliveryOrderPlaceItem;

class PatchDeliveryOrderPlaceItemAction
{
    public function execute(int $deliveryOrderPlaceItemId, array $fields): DeliveryOrderPlaceItem
    {
        /** @var DeliveryOrderPlaceItem $deliveryOrderPlaceItem */
        $deliveryOrderPlaceItem = DeliveryOrderPlaceItem::query()->findOrFail($deliveryOrderPlaceItemId);
        $deliveryOrderPlaceItem->update($fields);

        return $deliveryOrderPlaceItem;
    }
}
