<?php

use App\Domain\Common\Tests\Factories\Oms\DeliveryFactory;
use App\Domain\Common\Tests\Factories\Oms\OrderFactory;
use App\Domain\DeliveryOrders\Actions\DeliveryOrder\CancelDeliveryOrderAction;
use App\Domain\DeliveryOrders\Actions\DeliveryOrder\CreateDeliveryOrderAction;
use App\Domain\DeliveryOrders\Actions\DeliveryOrder\MigrateCustomerIdToDeliveryOrderAction;
use App\Domain\DeliveryOrders\Actions\DeliveryOrder\PatchDeliveryOrderAction;
use App\Domain\DeliveryOrders\Enums\DeliveryOrderStatus;
use App\Domain\DeliveryOrders\Models\DeliveryOrder;
use App\Domain\DeliveryOrders\Models\DeliveryOrderPlace;
use App\Domain\DeliveryOrders\Models\DeliveryOrderPlaceItem;
use App\Domain\Kafka\Actions\Send\SendDeliveryOrderEventAction;
use Ensi\LaravelTestFactories\FakerProvider;

use function Pest\Laravel\assertDatabaseHas;

use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class);
uses()->group('unit');

// region CreateDeliveryOrderAction
test("CreateDeliveryOrderAction success", function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $this->mock(SendDeliveryOrderEventAction::class)->allows('execute');

    /** @var IntegrationTestCase $this */
    $deliveryOrderFields = DeliveryOrder::factory()->make()->toArray();
    $deliveryOrderPlacesFields = DeliveryOrderPlace::factory()->count(3)->make()->toArray();
    $deliveryOrderFields['places'] = $deliveryOrderPlacesFields;
    foreach ($deliveryOrderFields['places'] as &$place) {
        $deliveryOrderPlaceItemsFields = DeliveryOrderPlaceItem::factory()->count(3)->make()->toArray();
        $place['items'] = $deliveryOrderPlaceItemsFields;
    }

    $deliveryOrder = resolve(CreateDeliveryOrderAction::class)->execute($deliveryOrderFields);

    assertDatabaseHas((new DeliveryOrder())->getTable(), [
        'id' => $deliveryOrder->id,
    ]);
})->with(FakerProvider::$optionalDataset);
// endregion

// region PatchDeliveryOrderAction
test("PatchDeliveryAction success", function (?bool $always) {
    /** @var IntegrationTestCase $this */
    FakerProvider::$optionalAlways = $always;

    $this->mock(SendDeliveryOrderEventAction::class)->allows('execute');

    /** @var DeliveryOrder $deliveryOrder */
    $deliveryOrder = DeliveryOrder::factory()->create(['status' => DeliveryOrderStatus::NEW]);

    $fields = [
        'status' => DeliveryOrderStatus::SHIPPED,
        'places' => DeliveryOrderPlace::factory()->count(5)->make([
            'items' => DeliveryOrderPlaceItem::factory()->count(3)->make()->toArray(),
        ])->toArray(),
    ];

    $deliveryOrder = resolve(PatchDeliveryOrderAction::class)->execute($deliveryOrder->id, $fields);

    assertDatabaseHas((new DeliveryOrder())->getTable(), [
        'id' => $deliveryOrder->id,
        'status' => $deliveryOrder->status,
    ]);
})->with(FakerProvider::$optionalDataset);
// endregion

// region CancelDeliveryOrderAction
test("CancelDeliveryAction success", function (?bool $always) {
    /** @var IntegrationTestCase $this */
    FakerProvider::$optionalAlways = $always;

    $this->mock(SendDeliveryOrderEventAction::class)->allows('execute');

    /** @var DeliveryOrder $deliveryOrder */
    $deliveryOrder = DeliveryOrder::factory()->create(['status' => DeliveryOrderStatus::NEW]);

    $deliveryOrder = resolve(CancelDeliveryOrderAction::class)->execute($deliveryOrder->id);

    assertDatabaseHas((new DeliveryOrder())->getTable(), [
        'id' => $deliveryOrder->id,
        'status' => $deliveryOrder->status,
    ]);
})->with(FakerProvider::$optionalDataset);
// endregion

test("MigrateCustomerIdToDeliveryOrderAction success", function (?bool $always) {
    /** @var IntegrationTestCase $this */
    FakerProvider::$optionalAlways = $always;

    $this->mock(SendDeliveryOrderEventAction::class)->allows('execute');

    /** @var DeliveryOrder $deliveryOrder */
    $deliveryOrder = DeliveryOrder::factory()->create();

    $customerId = 1;

    $this->mockOrdersOrdersApi()->allows([
        'searchOrders' => OrderFactory::new()
            ->withDeliveries([DeliveryFactory::new()->make(['id' => $deliveryOrder->delivery_id])])
            ->makeResponseSearch([
                ['customer_id' => $customerId],
            ]),
    ]);

    resolve(MigrateCustomerIdToDeliveryOrderAction::class)->execute();

    assertDatabaseHas((new DeliveryOrder())->getTable(), [
        'id' => $deliveryOrder->id,
        'customer_id' => $customerId,
    ]);
})->with(FakerProvider::$optionalDataset);
