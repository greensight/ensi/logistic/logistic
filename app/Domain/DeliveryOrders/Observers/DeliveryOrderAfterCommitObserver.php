<?php

namespace App\Domain\DeliveryOrders\Observers;

use App\Domain\DeliveryOrders\Models\DeliveryOrder;
use App\Domain\Kafka\Actions\Send\SendDeliveryOrderEventAction;
use App\Domain\Kafka\Messages\Send\ModelEvent\ModelEventMessage;

class DeliveryOrderAfterCommitObserver
{
    public bool $afterCommit = true;

    public function __construct(
        protected SendDeliveryOrderEventAction $sendKafkaAction
    ) {
    }

    public function created(DeliveryOrder $model): void
    {
        $this->sendKafkaAction->execute($model, ModelEventMessage::CREATE);
    }

    public function updated(DeliveryOrder $model): void
    {
        $this->sendKafkaAction->execute($model, ModelEventMessage::UPDATE);
    }

    public function deleted(DeliveryOrder $model): void
    {
        $this->sendKafkaAction->execute($model, ModelEventMessage::DELETE);
    }
}
