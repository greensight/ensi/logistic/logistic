<?php

namespace App\Domain\DeliveryServices\Enums;

/**
 * Способ доставки на последней миле (доставка до места получения заказа)
 * Class DeliveryMethod
 * @package App\Domain\DeliveryServices\Enums
 */
enum DeliveryMethod: int
{
    /** @var int курьерская доставка (доставка до адреса клиента) */
    case METHOD_DELIVERY = 1;
    /** @var int самовывоз из ПВЗ (доставка до ПВЗ) */
    case METHOD_PICKUP = 2;
    /** @var int самовывоз из магазина (самовывоз без доставки) */
    case METHOD_STORE = 3;

    public function name(): string
    {
        return __(implode('.', ['enums', DeliveryMethod::class, $this->value]));
    }

    public function isDelivery(): bool
    {
        return match ($this) {
            DeliveryMethod::METHOD_DELIVERY, DeliveryMethod::METHOD_PICKUP => true,
            default => false,
        };
    }
}
