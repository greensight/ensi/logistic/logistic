<?php

namespace App\Domain\DeliveryServices\Enums;

/**
 * Способы доставки на нулевой миле (доставка от продавца до распределительного центра)
 */
class ShipmentMethod
{
    /** @var int - курьер службы доставки */
    public const METHOD_DS_COURIER = 1;
    /** @var int - собственный курьер */
    public const METHOD_OWN_COURIER = 2;

    /**
     * @param int $externalId - идентификатор типа приема в интеграторе служб доставки ApiShip
     */
    public function __construct(public int $id, public string $name, public int $externalId)
    {
    }

    /** @return self[] */
    public static function all(): array
    {
        return [
            new self(self::METHOD_DS_COURIER, 'Курьер службы доставки', 1),
            new self(self::METHOD_OWN_COURIER, 'Собственный курьер', 2),
        ];
    }

    public static function validValues(): array
    {
        return [
            self::METHOD_DS_COURIER,
            self::METHOD_OWN_COURIER,
        ];
    }

    public static function findByExternalId(int $externalId): ?self
    {
        $items = self::all();

        foreach ($items as $item) {
            if ($item->externalId == $externalId) {
                return $item;
            }
        }

        return null;
    }
}
