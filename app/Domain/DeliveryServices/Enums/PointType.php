<?php

namespace App\Domain\DeliveryServices\Enums;

/**
 * Типы точек приема/выдачи заказов
 */
class PointType
{
    /** @var int - пункт выдачи заказа */
    public const TYPE_PICKUP_POINT = 1;
    /** @var int - постамат */
    public const TYPE_POSTOMAT = 2;
    /** @var int - отделение Почты России */
    public const TYPE_RU_POST_OFFICE = 3;
    /** @var int - терминал */
    public const SERVICE_TERMINAL = 4;

    /**
     * @param string $externalId - идентификатор службы в интеграторе служб доставки
     */
    public function __construct(public int $id, public string $name, public string $externalId)
    {
    }

    /** @return self[] */
    public static function all(): array
    {
        return [
            new self(self::TYPE_PICKUP_POINT, 'Пункт выдачи заказа', '1'),
            new self(self::TYPE_POSTOMAT, 'Постамат', '2'),
            new self(self::TYPE_RU_POST_OFFICE, 'Отделение Почты России', '3'),
            new self(self::SERVICE_TERMINAL, 'Терминал', '4'),
        ];
    }

    public static function validValues(): array
    {
        return [
            self::TYPE_PICKUP_POINT,
            self::TYPE_POSTOMAT,
            self::TYPE_RU_POST_OFFICE,
            self::SERVICE_TERMINAL,
        ];
    }

    public static function findByExternalId(string $externalId): ?self
    {
        $items = self::all();

        foreach ($items as $item) {
            if ($item->externalId == $externalId) {
                return $item;
            }
        }

        return null;
    }
}
