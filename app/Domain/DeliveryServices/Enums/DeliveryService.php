<?php

namespace App\Domain\DeliveryServices\Enums;

enum DeliveryService: int
{
    case TK1 = 1;
    case TK2 = 2;
    case TK3 = 3;
    case TK4 = 4;
    case TK5 = 5;
    case TK6 = 6;
    case TK7 = 7;
    case TK8 = 8;
    case TK9 = 9;
    case TK10 = 10;
}
