<?php

namespace App\Domain\DeliveryServices\Enums;

/**
 * Статус службы доставки (логистического оператора)
 * Class DeliveryServiceStatus
 * @package App\Domain\DeliveryServices\Enums
 */
class DeliveryServiceStatus
{
    /** @var int - активен */
    public const ACTIVE = 1;
    /** @var int - не активен */
    public const INACTIVE = 2;

    public function __construct(public int $id, public string $name)
    {
    }

    /** @return self[] */
    public static function all(): array
    {
        return [
            new self(self::ACTIVE, 'Активен'),
            new self(self::INACTIVE, 'Не активен'),
        ];
    }

    public static function validValues(): array
    {
        return [
            self::ACTIVE,
            self::INACTIVE,
        ];
    }
}
