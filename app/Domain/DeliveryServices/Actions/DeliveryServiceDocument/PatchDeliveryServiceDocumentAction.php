<?php

namespace App\Domain\DeliveryServices\Actions\DeliveryServiceDocument;

use App\Domain\DeliveryServices\Models\DeliveryServiceDocument;

class PatchDeliveryServiceDocumentAction
{
    public function execute(int $id, array $fields): DeliveryServiceDocument
    {
        /** @var DeliveryServiceDocument $deliveryServiceDocument */
        $deliveryServiceDocument = DeliveryServiceDocument::query()->findOrFail($id);
        $deliveryServiceDocument->update($fields);

        return $deliveryServiceDocument;
    }
}
