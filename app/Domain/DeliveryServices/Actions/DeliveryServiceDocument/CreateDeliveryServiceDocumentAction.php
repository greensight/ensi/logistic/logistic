<?php

namespace App\Domain\DeliveryServices\Actions\DeliveryServiceDocument;

use App\Domain\DeliveryServices\Models\DeliveryServiceDocument;

class CreateDeliveryServiceDocumentAction
{
    public function execute(array $fields): DeliveryServiceDocument
    {
        $deliveryServiceDocument = new DeliveryServiceDocument();
        $deliveryServiceDocument->fill($fields);
        $deliveryServiceDocument->save();

        return $deliveryServiceDocument;
    }
}
