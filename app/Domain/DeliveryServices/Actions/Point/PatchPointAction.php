<?php

namespace App\Domain\DeliveryServices\Actions\Point;

use App\Domain\DeliveryServices\Models\Point;

class PatchPointAction
{
    public function execute(int $id, array $fields): Point
    {
        /** @var Point $point */
        $point = Point::query()->findOrFail($id);
        $point->update($fields);

        return $point;
    }
}
