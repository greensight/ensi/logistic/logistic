<?php

namespace App\Domain\DeliveryServices\Actions\City;

use App\Domain\DeliveryServices\Actions\City\Data\CityData;
use App\Domain\DeliveryServices\Models\City;

class CreateOrUpdateCityAction
{
    public function execute(CityData $cityData): City
    {
        $city = City::query()
            ->where('guid', $cityData->guid)
            ->first() ?: new City();
        $city->region_id = $cityData->region_id;
        $city->name = $cityData->name;
        $city->guid = $cityData->guid;

        $city->save();

        return $city;
    }
}
