<?php

namespace App\Domain\DeliveryServices\Actions\City;

use App\Domain\DeliveryServices\Actions\City\Data\CityDeliveryServiceLinkData;
use App\Domain\DeliveryServices\Models\CityDeliveryServiceLink;

class CreateOrUpdateCityDeliveryServiceLinkAction
{
    public function execute(CityDeliveryServiceLinkData $cityDeliveryServiceLinkData): CityDeliveryServiceLink
    {
        $cityDeliveryServiceLink = CityDeliveryServiceLink::query()
            ->where('delivery_service', $cityDeliveryServiceLinkData->delivery_service_id)
            ->where('city_guid', $cityDeliveryServiceLinkData->city_guid)
            ->first();
        if (is_null($cityDeliveryServiceLink)) {
            $cityDeliveryServiceLink = new CityDeliveryServiceLink();
            $cityDeliveryServiceLink->delivery_service = $cityDeliveryServiceLinkData->delivery_service_id;
            $cityDeliveryServiceLink->city_guid = $cityDeliveryServiceLinkData->city_guid;
        }
        $cityDeliveryServiceLink->city_id = $cityDeliveryServiceLinkData->city_id;
        $cityDeliveryServiceLink->payload = $cityDeliveryServiceLinkData->payload ?: [];

        $cityDeliveryServiceLink->save();

        return $cityDeliveryServiceLink;
    }
}
