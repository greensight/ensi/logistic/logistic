<?php

namespace App\Domain\DeliveryServices\Actions\DeliveryService;

use App\Domain\DeliveryServices\Models\DeliveryServicePaymentMethodLink;
use Exception;

class DeletePaymentMethodFromDeliveryServiceAction
{
    /**
     * @throws Exception
     */
    public function execute(int $deliveryServiceId, int $paymentMethod): void
    {
        $link = DeliveryServicePaymentMethodLink::query()
            ->where('delivery_service_id', $deliveryServiceId)
            ->where('payment_method', $paymentMethod)
            ->firstOrFail();
        $link->delete();
    }
}
