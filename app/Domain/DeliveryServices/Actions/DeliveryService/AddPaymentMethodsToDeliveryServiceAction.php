<?php

namespace App\Domain\DeliveryServices\Actions\DeliveryService;

use App\Domain\DeliveryServices\Models\DeliveryServicePaymentMethodLink;

class AddPaymentMethodsToDeliveryServiceAction
{
    public function execute(int $deliveryServiceId, array $paymentMethods): void
    {
        $existLinks = DeliveryServicePaymentMethodLink::query()
            ->where('delivery_service_id', $deliveryServiceId)
            ->whereIn('payment_method', $paymentMethods)
            ->get()
            ->keyBy('payment_method');

        foreach ($paymentMethods as $paymentMethod) {
            if (!$existLinks->has($paymentMethod)) {
                $link = new DeliveryServicePaymentMethodLink();
                $link->delivery_service_id = $deliveryServiceId;
                $link->payment_method = $paymentMethod;

                $link->save();
            }
        }
    }
}
