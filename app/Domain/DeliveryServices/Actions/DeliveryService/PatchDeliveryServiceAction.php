<?php

namespace App\Domain\DeliveryServices\Actions\DeliveryService;

use App\Domain\DeliveryServices\Models\DeliveryService;

class PatchDeliveryServiceAction
{
    public function execute(int $id, array $fields): DeliveryService
    {
        /** @var DeliveryService $deliveryService */
        $deliveryService = DeliveryService::query()->findOrFail($id);
        $deliveryService->fill($fields);
        $deliveryService->save();

        return $deliveryService;
    }
}
