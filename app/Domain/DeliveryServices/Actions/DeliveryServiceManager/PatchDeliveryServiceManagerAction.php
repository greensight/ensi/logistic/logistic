<?php

namespace App\Domain\DeliveryServices\Actions\DeliveryServiceManager;

use App\Domain\DeliveryServices\Models\DeliveryServiceManager;

class PatchDeliveryServiceManagerAction
{
    public function execute(int $id, array $fields): DeliveryServiceManager
    {
        /** @var DeliveryServiceManager $deliveryServiceManager */
        $deliveryServiceManager = DeliveryServiceManager::query()->findOrFail($id);
        $deliveryServiceManager->update($fields);

        return $deliveryServiceManager;
    }
}
