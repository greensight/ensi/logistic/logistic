<?php

namespace App\Domain\DeliveryServices\Actions\DeliveryServiceManager;

use App\Domain\DeliveryServices\Models\DeliveryServiceManager;

class CreateDeliveryServiceManagerAction
{
    public function execute(array $fields): DeliveryServiceManager
    {
        $deliveryServiceManager = new DeliveryServiceManager();
        $deliveryServiceManager->fill($fields);
        $deliveryServiceManager->save();

        return $deliveryServiceManager;
    }
}
