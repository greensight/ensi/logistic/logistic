<?php

namespace App\Domain\DeliveryServices\Models;

use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Документы службы доставки
 * Class DeliveryServiceDocument
 * @package App\Domain\DeliveryServices\Models
 *
 * @property int $id - id
 * @property int $delivery_service_id - id службы доставки
 * @property string $name - название
 * @property string|null $file - ссылка на файл с документом
 * @property CarbonInterface|null $created_at - дата создания
 * @property CarbonInterface|null $updated_at - дата обновления
 *
 * @property DeliveryService $deliveryService - служба доставки
 */
class DeliveryServiceDocument extends Model
{
    protected $table = 'delivery_service_documents';

    protected $fillable = [
        'delivery_service_id',
        'name',
    ];

    public function deliveryService(): BelongsTo
    {
        return $this->belongsTo(DeliveryService::class);
    }
}
