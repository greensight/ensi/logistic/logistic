<?php

namespace App\Domain\DeliveryServices\Models\Tests\Factories;

use App\Domain\DeliveryServices\Models\Point;
use App\Domain\DeliveryServices\Models\PointWorking;
use Ensi\LaravelTestFactories\BaseModelFactory;

class PointWorkingFactory extends BaseModelFactory
{
    protected $model = PointWorking::class;

    public function definition(): array
    {
        return [
            'point_id' => Point::factory(),
            'is_active' => $this->faker->boolean(),
            'day' => $this->faker->numberBetween(1, 7),
            'working_start_time' => $this->faker->time('H:i'),
            'working_end_time' => $this->faker->time('H:i'),
        ];
    }
}
