<?php

namespace App\Domain\DeliveryServices\Models\Tests\Factories;

use App\Domain\DeliveryServices\Enums\DeliveryService;
use App\Domain\DeliveryServices\Models\Point;
use App\Domain\Support\Data\Address;
use Ensi\LaravelTestFactories\BaseModelFactory;

class PointFactory extends BaseModelFactory
{
    protected $model = Point::class;

    public function definition(): array
    {
        return [
            'delivery_service_id' => $this->faker->nullable()->randomEnum(DeliveryService::cases()),
            'external_id' => $this->faker->unique()->numerify('#########'),
            'is_active' => $this->faker->boolean(),
            'name' => $this->faker->text(50),
            'description' => $this->faker->nullable()->text(50),
            'phone' => $this->faker->nullable()->numerify('+7##########'),
            'timezone' => $this->faker->nullable()->timezone(),
            'address' => $this->faker->nullable()->exactly(Address::factory()->make()),
            'address_reduce' => $this->faker->nullable()->address(),
            'metro_station' => $this->faker->nullable()->text(50),
            'city_guid' => $this->faker->nullable()->uuid(),
            'geo_lat' => $this->faker->nullable()->exactly((string)$this->faker->latitude()),
            'geo_lon' => $this->faker->nullable()->exactly((string)$this->faker->longitude()),
            'only_online_payment' => $this->faker->nullable()->boolean(),
            'has_payment_card' => $this->faker->nullable()->boolean(),
            'has_courier' => $this->faker->nullable()->boolean(),
            'is_postamat' => $this->faker->nullable()->boolean(),
            'max_value' => $this->faker->nullable()->exactly((string)$this->faker->numberBetween(1, 100)),
            'max_weight' => $this->faker->numberBetween(1, 100),
        ];
    }

    public function withAddress(?Address $address = null): self
    {
        return $this->state(['address' => $address !== null ? $address : Address::factory()->make()]);
    }
}
