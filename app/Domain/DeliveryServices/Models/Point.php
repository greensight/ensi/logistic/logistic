<?php

namespace App\Domain\DeliveryServices\Models;

use App\Domain\DeliveryServices\Models\Tests\Factories\PointFactory;
use App\Domain\Support\Data\Address;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;

/**
 * Пункт приема/выдачи заказов
 *
 * @property int $id
 * @property int|null $delivery_service_id - сервис доставки
 * @property string $external_id - внешний идентификатор
 * @property bool $is_active - активность
 * @property string $name - название
 * @property string|null $description - описание проезда
 * @property string|null $phone - номер
 * @property string|null $timezone - часовой пояс
 *
 * @property Address|null $address - адрес
 * @property string|null $address_reduce - сокращенный адрес пвз
 * @property string|null $metro_station - станция метро
 * @property string|null $city_guid - идентификатор города
 * @property string|null $geo_lat - широта
 * @property string|null $geo_lon - долгота
 *
 * @property bool|null $only_online_payment - выдача только полностью оплаченных посылок
 * @property bool|null $has_payment_card - возможность оплаты картой
 * @property bool|null $has_courier - осуществляется ли курьерская доставка
 * @property bool|null $is_postamat - отделение является постаматом
 *
 * @property string|null $max_value - ограничение объема (куб.метры)
 * @property int|null $max_weight - ограничение веса (кг)
 *
 * @property CarbonInterface|null $created_at - дата создания
 * @property CarbonInterface|null $updated_at - дата обновления
 *
 * @property-read Collection|PointWorking[] $pointWorkings
 */
class Point extends Model
{
    protected $table = 'points';

    protected $fillable = ['delivery_service_id', 'is_active', 'address', 'city_guid', 'timezone', 'geo_lat', 'geo_lon'];

    protected $casts = [
        'address' => Address::class,
        'only_online_payment' => 'bool',
        'has_payment_card' => 'bool',
        'has_courier' => 'bool',
        'is_postamat' => 'bool',
        'max_weight' => 'int',
    ];

    public function pointWorkings(): HasMany
    {
        return $this->hasMany(PointWorking::class);
    }

    public static function factory(): PointFactory
    {
        return PointFactory::new();
    }
}
