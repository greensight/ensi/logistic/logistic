<?php

namespace App\Domain\DeliveryServices\Models;

use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Доступные способы оплаты для службы доставки
 * Class DeliveryServicePaymentMethodLink
 * @package App\Domain\DeliveryServices\Models
 *
 * @property int $id - id
 * @property int $delivery_service_id - id службы доставки
 * @property int $payment_method - id способа оплаты из OMS
 * @property CarbonInterface|null $created_at - дата создания
 * @property CarbonInterface|null $updated_at - дата обновления
 *
 * @property DeliveryService $deliveryService - служба доставки
 */
class DeliveryServicePaymentMethodLink extends Model
{
    protected $table = 'delivery_service_payment_method_links';

    public function deliveryService(): BelongsTo
    {
        return $this->belongsTo(DeliveryService::class);
    }
}
