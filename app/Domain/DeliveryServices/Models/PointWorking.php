<?php

namespace App\Domain\DeliveryServices\Models;

use App\Domain\DeliveryServices\Models\Tests\Factories\PointWorkingFactory;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property int $id
 * @property int $point_id
 * @property bool $is_active - активность
 *
 * @property int $day - день недели (1-7)
 * @property string $working_start_time - время начала работы склада
 * @property string $working_end_time - время конца работы склада
 *
 * @property CarbonInterface|null $created_at - дата создания
 * @property CarbonInterface|null $updated_at - дата обновления
 *
 * @property-read Point $point
 */
class PointWorking extends Model
{
    protected $table = 'point_workings';

    public function point(): BelongsTo
    {
        return $this->belongsTo(Point::class);
    }

    public static function factory(): PointWorkingFactory
    {
        return PointWorkingFactory::new();
    }
}
