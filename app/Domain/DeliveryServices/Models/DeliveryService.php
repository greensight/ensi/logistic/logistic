<?php

namespace App\Domain\DeliveryServices\Models;

use App\Domain\DeliveryServices\Models\Tests\Factories\DeliveryServiceFactory;
use App\Domain\Support\Data\Address;
use App\Http\ApiV1\OpenApiGenerated\Enums\DeliveryServiceStatusEnum;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;

/**
 * Служба доставки (логистический оператор) на последней миле
 * (доставка от распределительного центра до места получения заказа)
 * Class DeliveryService
 * @package App\Domain\DeliveryServices\Models
 *
 * @property int $id - id
 * @property string $name - название
 * @property CarbonInterface $registered_at - дата регистрации
 *
 * @property string $legal_info_company_name - юр. название компании
 * @property Address $legal_info_company_address - юр. адрес компании
 * @property string $legal_info_inn - ИНН
 * @property string $legal_info_payment_account - р/с
 * @property string $legal_info_bik - БИК
 * @property string $legal_info_bank - банк
 * @property string $legal_info_bank_correspondent_account - к/с банка
 *
 * @property string $general_manager_name - ФИО ген. директора
 *
 * @property string $contract_number - номер договора
 * @property CarbonInterface $contract_date - дата договора
 *
 * @property int $vat_rate - ставка НДС
 * @property int $taxation_type - тип налогообложения
 *
 * @property DeliveryServiceStatusEnum $status - статус
 *
 * @property string $comment - комментарий
 * @property string $apiship_key - код в системе ApiShip
 *
 * @property int $priority - приоритет (чем меньше, тем выше)
 * @property int $max_shipments_per_day - максимальное кол-во отправлений в день
 * @property CarbonInterface $max_cargo_export_time - время создания заявки для забора отправления день-в-день
 *
 * @property bool $do_consolidation - консолидация многоместных отправлений?
 * @property bool $do_deconsolidation - расконсолидация многоместных отправлений?
 * @property bool $do_zero_mile - осуществляет нулевую милю?
 * @property bool $do_express_delivery - осуществляет экспресс-доставку?
 * @property bool $do_return - принимает возвраты?
 * @property bool $do_dangerous_products_delivery - осуществляет доставку опасных грузов?
 * @property bool $do_transportation_oversized_cargo - перевозка крупногабаритных грузов?
 *
 * @property bool $add_partial_reject_service - добавлять услугу частичного отказ в заказ на доставку?
 * @property bool $add_insurance_service - добавлять услугу страхования груза в заказ на доставку?
 * @property bool $add_fitting_service - добавлять услугу примерки в заказ на доставку?
 * @property bool $add_return_service - добавлять услугу возврата в заказ на доставку?
 * @property bool $add_open_service - добавлять услугу вскрытия при получении в заказ на доставку?
 *
 * @property int $pct - planned сonsolidation time - плановое время доставки заказа от склада продавца
 * до логистического хаба ЛО и обработки заказа в сортировочном центре или хабе на стороне ЛО (мин)
 *
 * @property CarbonInterface|null $created_at - дата создания
 * @property CarbonInterface|null $updated_at - дата обновления
 *
 * @property Collection<DeliveryServiceDocument> $deliveryServiceDocuments - документы
 * @property Collection<DeliveryServiceManager> $deliveryServiceManagers - менеджеры
 * @property Collection<DeliveryServicePaymentMethodLink> $paymentMethods - доступные способы оплаты
 */
class DeliveryService extends Model
{
    protected $table = 'delivery_services';

    protected $casts = [
        'registered_at' => 'datetime',
        'legal_info_company_address' => 'array',
        'contract_date' => 'date',
        'max_cargo_export_time' => 'datetime',
        'status' => DeliveryServiceStatusEnum::class,
    ];

    protected $fillable = [
        'name',
        'registered_at',
        'legal_info_company_name',
        'legal_info_company_address',
        'legal_info_inn',
        'legal_info_payment_account',
        'legal_info_bik',
        'legal_info_bank',
        'legal_info_bank_correspondent_account',
        'general_manager_name',
        'contract_number',
        'contract_date',
        'vat_rate',
        'taxation_type',
        'status',
        'comment',
        'apiship_key',
        'priority',
        'max_shipments_per_day',
        'max_cargo_export_time',
        'do_consolidation',
        'do_deconsolidation',
        'do_zero_mile',
        'do_express_delivery',
        'do_return',
        'do_dangerous_products_delivery',
        'do_transportation_oversized_cargo',
        'add_partial_reject_service',
        'add_insurance_service',
        'add_fitting_service',
        'add_return_service',
        'add_open_service',
        'pct',
    ];

    protected function getLegalInfoCompanyAddressAttribute(): ?Address
    {
        return $this->attributes['legal_info_company_address']
            ? new Address(json_decode($this->attributes['legal_info_company_address']))
            : null;
    }

    protected function setLegalInfoCompanyAddressAttribute(?Address $address)
    {
        if ($address && !$address->address_string) {
            $address->address_string = $address->getFullString();
        }
        $this->attributes['legal_info_company_address'] = $address?->toJson();
    }

    public function deliveryServiceDocuments(): HasMany
    {
        return $this->hasMany(DeliveryServiceDocument::class);
    }

    public function deliveryServiceManagers(): HasMany
    {
        return $this->hasMany(DeliveryServiceManager::class);
    }

    public function paymentMethods(): HasMany
    {
        return $this->hasMany(DeliveryServicePaymentMethodLink::class);
    }

    public static function factory(): DeliveryServiceFactory
    {
        return DeliveryServiceFactory::new();
    }
}
