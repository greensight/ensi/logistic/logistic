<?php

namespace App\Domain\DeliveryServices\Models;

use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Менеджеры службы доставки
 * Class DeliveryServiceDocument
 * @package App\Domain\DeliveryServices\Models
 *
 * @property int $id - id
 * @property int $delivery_service_id - id службы доставки
 * @property string $name - ФИО
 * @property string $phone - телефон
 * @property string $email - e-mail
 * @property CarbonInterface|null $created_at - дата создания
 * @property CarbonInterface|null $updated_at - дата обновления
 *
 * @property DeliveryService $deliveryService - служба доставки
 */
class DeliveryServiceManager extends Model
{
    protected $table = 'delivery_service_managers';

    protected $fillable = [
        'delivery_service_id',
        'name',
        'phone',
        'email',
    ];

    public function deliveryService(): BelongsTo
    {
        return $this->belongsTo(DeliveryService::class);
    }
}
