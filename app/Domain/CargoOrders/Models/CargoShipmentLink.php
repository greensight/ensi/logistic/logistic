<?php

namespace App\Domain\CargoOrders\Models;

use App\Domain\CargoOrders\Models\Tests\Factories\CargoShipmentLinkFactory;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Ссылка на отправления в OMS
 * Class CargoShipmentLink
 * @package App\Domain\CargoOrders\Models
 *
 * @property int $id - идентификатор ссылки на отправление
 * @property int $cargo_id - идентификатор груза
 * @property int $shipment_id - идентификатор отправления в OMS
 *
 * @property CarbonInterface|null $created_at - дата создания
 * @property CarbonInterface|null $updated_at - дата обновления
 *
 * @property-read Cargo $cargo
 */
class CargoShipmentLink extends Model
{
    protected $table = 'cargo_shipment_links';

    public function cargo(): BelongsTo
    {
        return $this->belongsTo(Cargo::class);
    }

    public static function factory(): CargoShipmentLinkFactory
    {
        return CargoShipmentLinkFactory::new();
    }
}
