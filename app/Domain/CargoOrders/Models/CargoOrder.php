<?php

namespace App\Domain\CargoOrders\Models;

use App\Domain\CargoOrders\Models\Tests\Factories\CargoOrderFactory;
use App\Http\ApiV1\OpenApiGenerated\Enums\CargoOrderStatusEnum;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property int $id - идентификатор заказа на забор груза
 * @property int $cargo_id - идентификатор груза
 * @property string $timeslot_id - идентификатор таймслота
 * @property CarbonInterface $timeslot_from - время начала таймлоста
 * @property CarbonInterface $timeslot_to - время окончания таймслота
 *
 * @property string $cdek_intake_number - номер заявки СДЭК на вызов курьера
 * @property string $external_id - идентификатор во внешней системе
 * @property string $error_external_id - текст последней ошибки при создании заявки на вызов курьера для забора груза в службе доставки
 *
 * @property CarbonInterface $date - дата забора груза
 * @property CargoOrderStatusEnum $status - статус заказа на забор груза
 *
 * @property-read Cargo $cargo - груз
 */
class CargoOrder extends Model
{
    protected $table = 'cargo_orders';

    protected $casts = [
        'status' => CargoOrderStatusEnum::class,
    ];

    public function cargo(): BelongsTo
    {
        return $this->belongsTo(Cargo::class);
    }

    public static function factory(): CargoOrderFactory
    {
        return CargoOrderFactory::new();
    }
}
