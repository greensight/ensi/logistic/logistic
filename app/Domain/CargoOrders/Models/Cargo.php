<?php

namespace App\Domain\CargoOrders\Models;

use App\Domain\CargoOrders\Models\Tests\Factories\CargoFactory;
use App\Domain\DeliveryServices\Models\DeliveryService;
use App\Http\ApiV1\OpenApiGenerated\Enums\CargoStatusEnum;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;

/**
 * Груз - совокупность отправлений для доставки на нулевой миле (доставка от мерчанта до распределительного центра)
 * Class Cargo
 * @package App\Domain\CargoOrders\Models
 *
 * @property int $id - идентификатор груза
 * @property int $seller_id - id продавца
 * @property int $store_id - id склада продав
 * @property CargoStatusEnum $status - статус
 * @property CarbonInterface|null $status_at - дата установки статуса
 * @property bool $is_problem - флаг, что у груза проблемы при отгрузке
 * @property CarbonInterface|null $is_problem_at - дата установки флага проблемного груза
 * @property string $shipping_problem_comment - последнее сообщение мерчанта о проблеме с отгрузкой
 * @property bool $is_canceled - флаг, что груз отменен
 * @property CarbonInterface|null $is_canceled_at - дата установки флага отмены груза
 * @property int $delivery_service_id - идентификатор сервиса доставки
 *
 * @property float $width - ширина
 * @property float $height - высота
 * @property float $length - длина
 * @property float $weight - вес
 *
 * @property CarbonInterface|null $created_at - дата создания
 * @property CarbonInterface|null $updated_at - дата обновления
 *
 * @property-read Collection<CargoShipmentLink> $shipmentLinks - ссылки на отправления в OMS
 * @property-read DeliveryService $deliveryService - сервис доставки
 */
class Cargo extends Model
{
    protected $table = 'cargo';

    protected $fillable = [
        'seller_id', 'store_id', 'delivery_service_id', 'width', 'height', 'length', 'weight',
        'status', 'status_at', 'is_problem', 'is_problem_at', 'shipping_problem_comment', 'is_canceled', 'is_canceled_at',
    ];

    protected $casts = [
        'status' => CargoStatusEnum::class,
    ];

    public function shipmentLinks(): HasMany
    {
        return $this->hasMany(CargoShipmentLink::class);
    }

    public function deliveryService(): BelongsTo
    {
        return $this->belongsTo(DeliveryService::class);
    }

    public function cargoOrder(): BelongsTo
    {
        return $this->belongsTo(CargoOrder::class, 'id', 'cargo_id');
    }

    public static function factory(): CargoFactory
    {
        return CargoFactory::new();
    }
}
