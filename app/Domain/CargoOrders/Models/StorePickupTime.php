<?php

namespace App\Domain\CargoOrders\Models;

use App\Domain\CargoOrders\Models\Tests\Factories\StorePickupTimeFactory;
use App\Domain\DeliveryServices\Models\DeliveryService;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Класс-модель для сущности "Склады - время отгрузки грузов"
 * Class StorePickupTime
 *
 * @property int $id - id времени отгрузки груза
 * @property int $external_id - идентификатор во внешней системе (bu)
 * @property int $store_id - id склада
 * @property int $day - день недели (1-7)
 * @property string $pickup_time_code - код времени отгрузки у службы доставки
 * @property CarbonInterface $pickup_time_start - время начала отгрузки
 * @property CarbonInterface $pickup_time_end - время окончания отгрузки
 * @property CarbonInterface $cargo_export_time - время выгрузки информации о грузе в службу доставки
 * @property int $delivery_service_id - идентификатор службы доставки
 *
 * @property CarbonInterface|null $created_at - дата создания
 * @property CarbonInterface|null $updated_at - дата обновления
 */
class StorePickupTime extends Model
{
    protected $table = 'store_pickup_times';

    protected $fillable = [
        'external_id',
        'store_id',
        'day',
        'pickup_time_code',
        'pickup_time_start',
        'pickup_time_end',
        'cargo_export_time',
        'delivery_service_id',
    ];

    public function deliveryService(): BelongsTo
    {
        return $this->belongsTo(DeliveryService::class);
    }

    public static function factory(): StorePickupTimeFactory
    {
        return StorePickupTimeFactory::new();
    }
}
