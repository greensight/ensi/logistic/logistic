<?php

namespace App\Domain\CargoOrders\Models\Tests\Factories;

use App\Domain\CargoOrders\Models\StorePickupTime;
use App\Domain\DeliveryServices\Enums\DeliveryService;
use Ensi\LaravelTestFactories\BaseModelFactory;

class StorePickupTimeFactory extends BaseModelFactory
{
    protected $model = StorePickupTime::class;

    public function definition(): array
    {
        return [
            'store_id' => $this->faker->modelId(),
            'external_id' => $this->faker->unique()->modelId(),
            'day' => $this->faker->numberBetween(1, 7),
            'pickup_time_code' => "{$this->faker->numberBetween(0, 23)}-{$this->faker->numberBetween(0, 23)}",
            'pickup_time_start' => $this->faker->time('H:i'),
            'pickup_time_end' => $this->faker->time('H:i'),
            'cargo_export_time' => $this->faker->time('H:i'),
            'delivery_service_id' => $this->faker->randomEnum(DeliveryService::cases()),
        ];
    }
}
