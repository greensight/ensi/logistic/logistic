<?php

namespace App\Domain\CargoOrders\Models\Tests\Factories;

use App\Domain\CargoOrders\Models\Cargo;
use App\Domain\CargoOrders\Models\CargoShipmentLink;
use Ensi\LaravelTestFactories\BaseModelFactory;

class CargoShipmentLinkFactory extends BaseModelFactory
{
    protected $model = CargoShipmentLink::class;

    public function definition(): array
    {
        return [
            'cargo_id' => Cargo::factory(),
            'shipment_id' => $this->faker->modelId(),
        ];
    }
}
