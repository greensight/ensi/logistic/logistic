<?php

namespace App\Domain\CargoOrders\Models\Tests\Factories;

use App\Domain\CargoOrders\Models\Cargo;
use App\Domain\DeliveryServices\Enums\DeliveryService;
use App\Http\ApiV1\OpenApiGenerated\Enums\CargoStatusEnum;
use Ensi\LaravelTestFactories\BaseModelFactory;

class CargoFactory extends BaseModelFactory
{
    protected $model = Cargo::class;

    public function definition(): array
    {
        return [
            'seller_id' => $this->faker->modelId(),
            'store_id' => $this->faker->modelId(),
            'status' => $this->faker->randomEnum(CargoStatusEnum::cases()),
            'status_at' => $this->faker->date(),
            'is_problem' => $this->faker->boolean(),
            'is_problem_at' => $this->faker->date(),
            'shipping_problem_comment' => $this->faker->text(200),
            'is_canceled' => $this->faker->boolean(),
            'is_canceled_at' => $this->faker->date(),
            'delivery_service_id' => $this->faker->randomEnum(DeliveryService::cases()),

            'width' => $this->faker->randomFloat(),
            'height' => $this->faker->randomFloat(),
            'length' => $this->faker->randomFloat(),
            'weight' => $this->faker->randomFloat(),
        ];
    }
}
