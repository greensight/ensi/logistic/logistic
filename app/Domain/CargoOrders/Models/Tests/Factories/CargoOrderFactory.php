<?php

namespace App\Domain\CargoOrders\Models\Tests\Factories;

use App\Domain\CargoOrders\Models\Cargo;
use App\Domain\CargoOrders\Models\CargoOrder;
use App\Http\ApiV1\OpenApiGenerated\Enums\CargoOrderStatusEnum;
use Ensi\LaravelTestFactories\BaseModelFactory;

class CargoOrderFactory extends BaseModelFactory
{
    protected $model = CargoOrder::class;

    public function definition(): array
    {
        return [
            'cargo_id' => Cargo::factory(),

            'timeslot_id' => $this->faker->uuid(),
            'timeslot_from' => $this->faker->time(),
            'timeslot_to' => $this->faker->time(),

            'cdek_intake_number' => $this->faker->unique()->numerify('######'),
            'external_id' => $this->faker->unique()->numerify('######'),
            'error_external_id' => $this->faker->text(20),
            'date' => $this->faker->date(),
            'status' => $this->faker->randomEnum(CargoOrderStatusEnum::cases()),
        ];
    }
}
