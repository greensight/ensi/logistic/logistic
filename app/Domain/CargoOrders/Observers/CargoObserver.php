<?php

namespace App\Domain\CargoOrders\Observers;

use App\Domain\CargoOrders\Models\Cargo;

class CargoObserver
{
    public function saving(Cargo $cargo): void
    {
        if ($cargo->isDirty('status')) {
            $cargo->status_at = now();
        }

        if ($cargo->isDirty('is_problem')) {
            $cargo->is_problem_at = now();
        }

        if ($cargo->isDirty('is_canceled')) {
            $cargo->is_canceled_at = now();
        }
    }
}
