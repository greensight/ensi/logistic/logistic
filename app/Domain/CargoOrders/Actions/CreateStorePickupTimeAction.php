<?php

namespace App\Domain\CargoOrders\Actions;

use App\Domain\CargoOrders\Models\StorePickupTime;

class CreateStorePickupTimeAction
{
    public function execute(array $fields): StorePickupTime
    {
        $storePickupTime = new StorePickupTime();
        $storePickupTime->fill($fields);
        $storePickupTime->save();

        return $storePickupTime;
    }
}
