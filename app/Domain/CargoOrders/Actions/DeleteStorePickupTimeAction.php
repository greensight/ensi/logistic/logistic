<?php

namespace App\Domain\CargoOrders\Actions;

use App\Domain\CargoOrders\Models\StorePickupTime;

class DeleteStorePickupTimeAction
{
    public function execute(int $storePickupTimeExternalId): void
    {
        $storePickupTime = StorePickupTime::query()
            ->where('external_id', $storePickupTimeExternalId)
            ->firstOrFail();

        $storePickupTime->delete();
    }
}
