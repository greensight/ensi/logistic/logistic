<?php

namespace App\Domain\CargoOrders\Actions;

use App\Domain\CargoOrders\Models\Cargo;

class CancelCargoAction
{
    public function execute(int $cargoId): Cargo
    {
        /** @var Cargo $cargo */
        $cargo = Cargo::query()->findOrFail($cargoId);

        $cargo->is_canceled = true;
        $cargo->save();

        return $cargo;
    }
}
