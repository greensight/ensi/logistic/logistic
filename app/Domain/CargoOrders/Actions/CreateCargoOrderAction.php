<?php

namespace App\Domain\CargoOrders\Actions;

use App\Domain\CargoOrders\Models\CargoOrder;

class CreateCargoOrderAction
{
    public function execute(array $fields): CargoOrder
    {
        $cargoOrder = new CargoOrder();
        $cargoOrder->save();

        return $cargoOrder;
    }
}
