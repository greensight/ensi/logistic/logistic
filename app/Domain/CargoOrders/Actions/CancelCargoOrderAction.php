<?php

namespace App\Domain\CargoOrders\Actions;

use App\Domain\CargoOrders\Models\CargoOrder;
use App\Http\ApiV1\OpenApiGenerated\Enums\CargoOrderStatusEnum;

class CancelCargoOrderAction
{
    public function execute(int $cargoOrderId): CargoOrder
    {
        /** @var CargoOrder $cargoOrder */
        $cargoOrder = CargoOrder::query()->findOrFail($cargoOrderId);

        $cargoOrder->status = CargoOrderStatusEnum::CANCELED;
        $cargoOrder->save();

        return $cargoOrder;
    }
}
