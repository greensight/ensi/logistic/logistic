<?php

namespace App\Domain\CargoOrders\Actions;

use App\Domain\CargoOrders\Models\StorePickupTime;

class PatchStorePickupTimeAction
{
    public function execute(int $storePickupTimeExternalId, array $fields): StorePickupTime
    {
        /** @var StorePickupTime $storePickupTime */
        $storePickupTime = StorePickupTime::query()
            ->where('external_id', $storePickupTimeExternalId)
            ->firstOrFail();

        $storePickupTime->update($fields);

        return $storePickupTime;
    }
}
