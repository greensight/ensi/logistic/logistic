<?php

namespace App\Domain\CargoOrders\Actions;

use App\Domain\CargoOrders\Models\Cargo;

class PatchCargoAction
{
    public function execute(int $cargoId, array $fields): Cargo
    {
        /** @var Cargo $cargo */
        $cargo = Cargo::query()->findOrFail($cargoId);
        $cargo->fill($fields);
        $cargo->save();

        return $cargo;
    }
}
