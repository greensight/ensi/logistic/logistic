<?php

namespace App\Domain\Geos\Actions\Region;

use App\Domain\Geos\Models\Region;

class PatchRegionAction
{
    public function execute(int $id, array $fields): Region
    {
        /** @var Region $region */
        $region = Region::query()->findOrFail($id);
        $region->fill($fields);
        $region->save();

        return $region;
    }
}
