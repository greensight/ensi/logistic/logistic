<?php

namespace App\Domain\Geos\Actions\Region;

use App\Domain\Geos\Models\Region;

class DeleteRegionAction
{
    public function execute(int $id): void
    {
        $region = Region::query()->findOrFail($id);
        $region->delete();
    }
}
