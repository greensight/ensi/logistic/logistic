<?php

namespace App\Domain\Geos\Actions\Region;

use App\Domain\Geos\Models\Region;

class CreateRegionAction
{
    public function execute(array $fields): Region
    {
        $region = new Region();
        $region->fill($fields);
        $region->save();

        return $region;
    }
}
