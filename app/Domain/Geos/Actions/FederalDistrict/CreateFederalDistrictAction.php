<?php

namespace App\Domain\Geos\Actions\FederalDistrict;

use App\Domain\Geos\Models\FederalDistrict;

class CreateFederalDistrictAction
{
    public function execute(array $fields): FederalDistrict
    {
        $federalDistrict = new FederalDistrict();
        $federalDistrict->fill($fields);

        $federalDistrict->save();

        return $federalDistrict;
    }
}
