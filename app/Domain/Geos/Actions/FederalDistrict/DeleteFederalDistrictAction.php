<?php

namespace App\Domain\Geos\Actions\FederalDistrict;

use App\Domain\Geos\Models\FederalDistrict;

class DeleteFederalDistrictAction
{
    public function execute(int $id): void
    {
        $federalDistrict = FederalDistrict::query()->findOrFail($id);
        $federalDistrict->delete();
    }
}
