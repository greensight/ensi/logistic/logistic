<?php

namespace App\Domain\Geos\Actions\FederalDistrict;

use App\Domain\Geos\Models\FederalDistrict;

class PatchFederalDistrictAction
{
    public function execute(int $id, array $fields): FederalDistrict
    {
        /** @var FederalDistrict $federalDistrict */
        $federalDistrict = FederalDistrict::query()->findOrFail($id);
        $federalDistrict->fill($fields);
        $federalDistrict->save();

        return $federalDistrict;
    }
}
