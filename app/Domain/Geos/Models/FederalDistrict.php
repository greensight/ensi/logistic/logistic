<?php

namespace App\Domain\Geos\Models;

use App\Domain\Geos\Models\Tests\Factories\FederalDistrictFactory;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;

/**
 * Федеральный округ
 * Class FederalDistrict
 * @package App\Domain\Geos\Models
 *
 * @property int $id - id
 * @property string $name - название
 * @property CarbonInterface|null $created_at - дата создания
 * @property CarbonInterface|null $updated_at - дата обновления
 *
 * @property Collection<Region> $regions
 */
class FederalDistrict extends Model
{
    protected $table = 'federal_districts';

    protected $fillable = ['name'];

    public function regions(): HasMany
    {
        return $this->hasMany(Region::class);
    }

    public static function factory(): FederalDistrictFactory
    {
        return FederalDistrictFactory::new();
    }
}
