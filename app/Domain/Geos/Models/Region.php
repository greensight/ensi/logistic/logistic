<?php

namespace App\Domain\Geos\Models;

use App\Domain\DeliveryPrices\Models\DeliveryPrice;
use App\Domain\DeliveryServices\Models\City;
use App\Domain\Geos\Models\Tests\Factories\RegionFactory;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;

/**
 * Регион
 * Class Region
 * @package App\Domain\Geos\Models
 *
 * @property int $id - id
 * @property int $federal_district_id - id федерального округа
 * @property string $name - название
 * @property string $guid - id ФИАС региона
 * @property CarbonInterface|null $created_at - дата создания
 * @property CarbonInterface|null $updated_at - дата обновления
 *
 * @property FederalDistrict $federalDistrict
 * @property Collection<DeliveryPrice> $deliveryPrices
 * @property Collection<City> $cities
 */
class Region extends Model
{
    protected $table = 'regions';

    protected $fillable = ['federal_district_id', 'name', 'guid'];

    public static function findByGuid(string $guid): ?self
    {
        return Region::where('guid', $guid)->first();
    }

    public function federalDistrict(): BelongsTo
    {
        return $this->belongsTo(FederalDistrict::class);
    }

    public function deliveryPrices(): HasMany
    {
        return $this->hasMany(DeliveryPrice::class);
    }

    public function cities(): HasMany
    {
        return $this->hasMany(City::class);
    }

    public static function factory(): RegionFactory
    {
        return RegionFactory::new();
    }
}
