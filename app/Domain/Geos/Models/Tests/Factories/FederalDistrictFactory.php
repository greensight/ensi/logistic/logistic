<?php

namespace App\Domain\Geos\Models\Tests\Factories;

use App\Domain\Geos\Models\FederalDistrict;
use Ensi\LaravelTestFactories\BaseModelFactory;

class FederalDistrictFactory extends BaseModelFactory
{
    protected $model = FederalDistrict::class;

    public function definition(): array
    {
        return [
            'name' => $this->faker->text(10),
        ];
    }
}
