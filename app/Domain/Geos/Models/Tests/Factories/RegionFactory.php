<?php

namespace App\Domain\Geos\Models\Tests\Factories;

use App\Domain\Geos\Models\Region;
use Ensi\LaravelTestFactories\BaseModelFactory;

class RegionFactory extends BaseModelFactory
{
    protected $model = Region::class;

    public function definition(): array
    {
        return [
            'federal_district_id' => FederalDistrictFactory::new(),

            'name' => $this->faker->text(10),
            'guid' => $this->faker->uuid(),
        ];
    }
}
