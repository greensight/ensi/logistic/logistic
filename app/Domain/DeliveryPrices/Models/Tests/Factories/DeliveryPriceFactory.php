<?php

namespace App\Domain\DeliveryPrices\Models\Tests\Factories;

use App\Domain\DeliveryPrices\Models\DeliveryPrice;
use App\Domain\DeliveryServices\Enums\DeliveryMethod;
use App\Domain\DeliveryServices\Enums\DeliveryService;
use App\Domain\Geos\Models\FederalDistrict;
use App\Domain\Geos\Models\Region;
use Ensi\LaravelTestFactories\BaseModelFactory;

class DeliveryPriceFactory extends BaseModelFactory
{
    protected $model = DeliveryPrice::class;

    public function definition(): array
    {
        $fn = function ($field) {
            static $region = null;
            if (is_null($region)) {
                $region = Region::factory()->create();
            }

            return $region->getAttribute($field);
        };

        return [
            'federal_district_id' => FederalDistrict::factory(),
            'region_id' => fn () => $fn('id'),

            'region_guid' => fn () => $fn('guid'),
            'price' => $this->faker->numberBetween(500, 1000),
            'delivery_service' => $this->faker->optional()->randomEnum(DeliveryService::cases()),
            'delivery_method' => $this->faker->randomEnum(DeliveryMethod::cases()),
        ];
    }
}
