<?php

namespace App\Domain\DeliveryPrices\Models;

use App\Domain\DeliveryPrices\Models\Tests\Factories\DeliveryPriceFactory;
use App\Domain\DeliveryServices\Models\DeliveryService;
use App\Domain\Geos\Models\FederalDistrict;
use App\Domain\Geos\Models\Region;
use App\Http\ApiV1\OpenApiGenerated\Enums\DeliveryMethodEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\DeliveryServiceEnum;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Стоимость доставки по регионам
 * Class DeliveryPrice
 * @package App\Domain\DeliveryPrices\Models
 *
 * @property int $id - id
 * @property int $federal_district_id - id федерального округа
 * @property int $region_id - id региона
 * @property string $region_guid - id ФИАС региона
 * @property DeliveryServiceEnum|null $delivery_service - id службы доставки
 * @property DeliveryMethodEnum $delivery_method - способ доставки на последней миле (доставка до места получения заказа)
 * @property int $price - цена доставки
 * @property CarbonInterface|null $created_at - дата создания
 * @property CarbonInterface|null $updated_at - дата обновления
 *
 * @property DeliveryService $deliveryService - служба доставки
 * @property FederalDistrict $federalDistrict - федеральный округ
 * @property Region $region - федеральный округ
 */
class DeliveryPrice extends Model
{
    protected $fillable = [
        'federal_district_id', 'region_id', 'region_guid', 'delivery_service', 'delivery_method', 'price',
    ];

    protected $casts = [
        'delivery_service' => DeliveryServiceEnum::class,
        'delivery_method' => DeliveryMethodEnum::class,
    ];

    public function deliveryService(): BelongsTo
    {
        return $this->belongsTo(DeliveryService::class);
    }

    public function federalDistrict(): BelongsTo
    {
        return $this->belongsTo(FederalDistrict::class);
    }

    public function region(): BelongsTo
    {
        return $this->belongsTo(Region::class);
    }

    public static function factory(): DeliveryPriceFactory
    {
        return DeliveryPriceFactory::new();
    }
}
