<?php

namespace App\Domain\DeliveryPrices\Actions\DeliveryPrice;

use App\Domain\DeliveryPrices\Models\DeliveryPrice;

class DeleteDeliveryPriceAction
{
    public function execute(int $id): void
    {
        $deliveryPrice = DeliveryPrice::query()->findOrFail($id);
        $deliveryPrice->delete();
    }
}
