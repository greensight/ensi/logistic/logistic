<?php

namespace App\Domain\DeliveryPrices\Actions\DeliveryPrice\Data;

class CheckoutDeliveryPricesData
{
    public function __construct(
        public string $regionGuid,
        public int $deliveryMethod,
    ) {
    }
}
