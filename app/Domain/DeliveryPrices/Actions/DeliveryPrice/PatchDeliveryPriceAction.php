<?php

namespace App\Domain\DeliveryPrices\Actions\DeliveryPrice;

use App\Domain\DeliveryPrices\Models\DeliveryPrice;

class PatchDeliveryPriceAction
{
    public function execute(int $id, array $fields): DeliveryPrice
    {
        /** @var DeliveryPrice $deliveryPrice */
        $deliveryPrice = DeliveryPrice::query()->findOrFail($id);

        $deliveryPrice->fill($fields);
        $deliveryPrice->save();

        return $deliveryPrice;
    }
}
