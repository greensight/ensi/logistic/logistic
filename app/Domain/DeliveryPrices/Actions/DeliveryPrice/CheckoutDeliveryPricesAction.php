<?php

namespace App\Domain\DeliveryPrices\Actions\DeliveryPrice;

use App\Domain\DeliveryPrices\Actions\DeliveryPrice\Data\CheckoutDeliveryPricesData;
use App\Domain\DeliveryPrices\Models\DeliveryPrice;
use App\Domain\Geos\Models\Region;
use Illuminate\Support\Collection;

class CheckoutDeliveryPricesAction
{
    public function execute(CheckoutDeliveryPricesData $data): Collection
    {
        $region = Region::findByGuid($data->regionGuid);

        if ($region === null) {
            return new Collection();
        }

        /** @var Collection<DeliveryPrice> $deliveryPrices */
        $deliveryPrices = DeliveryPrice::query()
            ->where('region_guid', $data->regionGuid)
            ->where('delivery_method', $data->deliveryMethod)
            ->get();

        if ($deliveryPrices->isNotEmpty()) {
            return $deliveryPrices;
        }

        return DeliveryPrice::query()
            ->where('federal_district_id', $region->federal_district_id)
            ->where('delivery_method', $data->deliveryMethod)
            ->whereNull('region_id')
            ->get();
    }
}
