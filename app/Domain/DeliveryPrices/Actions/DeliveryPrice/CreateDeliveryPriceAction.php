<?php

namespace App\Domain\DeliveryPrices\Actions\DeliveryPrice;

use App\Domain\DeliveryPrices\Models\DeliveryPrice;

class CreateDeliveryPriceAction
{
    public function execute(array $fields): DeliveryPrice
    {
        $deliveryPrice = new DeliveryPrice();
        $deliveryPrice->fill($fields);
        $deliveryPrice->save();

        return $deliveryPrice;
    }
}
