<?php

namespace App\Domain\DeliveryKpis\Models;

use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * KPI для расчета сроков доставки
 * Class DeliveryKpi
 * @package App\Domain\DeliveryKpis\Models
 *
 * @property int $id - id
 * @property int $rtg - ready-to-go time - время для проверки заказа АОЗ до его передачи в MAS (мин)
 * @property int $ct - confirmation time - время перехода Отправления из статуса “Ожидает подтверждения”
 * в статус “На комплектации” (мин)
 * @property int $ppt - planned processing time - плановое время для прохождения Отправлением статусов
 * от “На комплектации” до “Готов к передаче ЛО” (мин)
 * @property CarbonInterface|null $created_at - дата создания
 * @property CarbonInterface|null $updated_at - дата обновления
 */
class DeliveryKpi extends Model
{
    protected $table = 'delivery_kpi';

    protected $fillable = ['rtg', 'ct', 'ppt'];

    public static function get(): ?DeliveryKpi
    {
        /** @var DeliveryKpi|null $deliveryKpi */
        $deliveryKpi = self::query()->first();

        return $deliveryKpi;
    }
}
