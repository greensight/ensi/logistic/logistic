<?php

namespace App\Domain\DeliveryKpis\Models;

use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * Class DeliveryKpiPpt
 * @package App\Domain\DeliveryKpis\Models
 *
 * @property int $id - id
 * @property int $seller_id - id продавца
 * @property int $ppt - planned processing time - плановое время для прохождения Отправлением статусов
 * от “На комплектации” до “Готов к передаче ЛО” (мин)
 * @property CarbonInterface|null $created_at - дата создания
 * @property CarbonInterface|null $updated_at - дата обновления
 */
class DeliveryKpiPpt extends Model
{
    protected $table = 'delivery_kpi_ppt';

    protected $fillable = ['ppt'];
}
