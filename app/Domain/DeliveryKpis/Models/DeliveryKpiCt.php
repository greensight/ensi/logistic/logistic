<?php

namespace App\Domain\DeliveryKpis\Models;

use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * Class DeliveryKpiCt
 * @package App\Domain\DeliveryKpis\Models
 *
 * @property int $id - id
 * @property int $seller_id - id продавца
 * @property int $ct - confirmation time - время перехода Отправления из статуса “Ожидает подтверждения”
 * в статус “На комплектации (мин)
 * @property CarbonInterface|null $created_at - дата создания
 * @property CarbonInterface|null $updated_at - дата обновления
 */
class DeliveryKpiCt extends Model
{
    protected $table = 'delivery_kpi_ct';

    protected $fillable = ['ct'];
}
