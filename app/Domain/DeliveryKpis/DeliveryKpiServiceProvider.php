<?php

namespace App\Domain\DeliveryKpis;

use App\Domain\DeliveryKpis\Actions\DeliveryKpi\GetDeliveryKpiAction;
use App\Domain\DeliveryKpis\Actions\DeliveryKpiCt\GetCtBySellerAction;
use App\Domain\DeliveryKpis\Actions\DeliveryKpiPpt\GetPptBySellerAction;
use Illuminate\Support\ServiceProvider;

class DeliveryKpiServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        //
    }

    public function boot(): void
    {
        $this->fillingContainer();
    }

    protected function fillingContainer(): void
    {
        $this->app->scoped(
            GetCtBySellerAction::class,
            function ($app) {
                return new GetCtBySellerAction($app->make(GetDeliveryKpiAction::class));
            }
        );

        $this->app->scoped(
            GetDeliveryKpiAction::class,
            function () {
                return new GetDeliveryKpiAction();
            }
        );

        $this->app->scoped(
            GetPptBySellerAction::class,
            function ($app) {
                return new GetPptBySellerAction($app->make(GetDeliveryKpiAction::class));
            }
        );
    }
}
