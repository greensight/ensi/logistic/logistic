<?php

namespace App\Domain\DeliveryKpis\Actions\DeliveryKpiCt;

use App\Domain\DeliveryKpis\Models\DeliveryKpiCt;
use App\Exceptions\CreateException;

class CreateDeliveryKpiCtAction
{
    public function execute(int $sellerId, array $fields): DeliveryKpiCt
    {
        /** @var DeliveryKpiCt|null $deliveryKpiCt */
        $deliveryKpiCt = DeliveryKpiCt::query()
            ->where('seller_id', $sellerId)
            ->first();
        if ($deliveryKpiCt) {
            throw new CreateException('DeliveryKpiCt are exist for this seller', 400);
        }
        $deliveryKpiCt = new DeliveryKpiCt();
        $deliveryKpiCt->fill($fields);
        $deliveryKpiCt->seller_id = $sellerId;

        $deliveryKpiCt->save();

        return $deliveryKpiCt;
    }
}
