<?php

namespace App\Domain\DeliveryKpis\Actions\DeliveryKpiCt;

use App\Domain\DeliveryKpis\Models\DeliveryKpiCt;

class PatchDeliveryKpiCtAction
{
    public function execute(int $sellerId, array $fields): DeliveryKpiCt
    {
        /** @var DeliveryKpiCt $deliveryKpiCt */
        $deliveryKpiCt = DeliveryKpiCt::query()
            ->where('seller_id', $sellerId)
            ->firstOrNew();

        $deliveryKpiCt->fill($fields);
        $deliveryKpiCt->seller_id = $sellerId;

        $deliveryKpiCt->save();

        return $deliveryKpiCt;
    }
}
