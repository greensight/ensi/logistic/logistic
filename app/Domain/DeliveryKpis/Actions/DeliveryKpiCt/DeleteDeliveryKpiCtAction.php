<?php

namespace App\Domain\DeliveryKpis\Actions\DeliveryKpiCt;

use App\Domain\DeliveryKpis\Models\DeliveryKpiCt;

class DeleteDeliveryKpiCtAction
{
    public function execute(int $sellerId): void
    {
        /** @var DeliveryKpiCt $deliveryKpiCt */
        $deliveryKpiCt = DeliveryKpiCt::query()
            ->where('seller_id', $sellerId)
            ->firstOrFail();

        $deliveryKpiCt->delete();
    }
}
