<?php

namespace App\Domain\DeliveryKpis\Actions\DeliveryKpiCt;

use App\Domain\DeliveryKpis\Actions\DeliveryKpi\GetDeliveryKpiAction;
use App\Domain\DeliveryKpis\Models\DeliveryKpiCt;
use Illuminate\Support\Collection;

class GetCtBySellerAction
{
    /** @var Collection|DeliveryKpiCt[] */
    protected Collection|array|null $deliveryKpiCt = null;

    public function __construct(protected GetDeliveryKpiAction $getDeliveryKpiAction)
    {
    }

    public function execute(int $sellerId): int
    {
        if (is_null($this->deliveryKpiCt)) {
            $this->deliveryKpiCt = DeliveryKpiCt::query()
                ->where('seller_id', $sellerId)
                ->get()
                ->keyBy('seller_id');
        }
        if ($this->deliveryKpiCt->has($sellerId)) {
            return $this->deliveryKpiCt[$sellerId]->ct;
        } else {
            $deliveryKpi = $this->getDeliveryKpiAction->execute();

            return $deliveryKpi ? $deliveryKpi->ct : 0;
        }
    }

    public function loadCt(array $sellersIds): void
    {
        $this->deliveryKpiCt = DeliveryKpiCt::query()
            ->whereIn('seller_id', $sellersIds)
            ->get()
            ->keyBy('seller_id');
    }
}
