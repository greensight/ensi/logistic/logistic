<?php

namespace App\Domain\DeliveryKpis\Actions\DeliveryKpiPpt;

use App\Domain\DeliveryKpis\Models\DeliveryKpiPpt;
use App\Exceptions\CreateException;

class CreateDeliveryKpiPptAction
{
    public function execute(int $sellerId, array $fields): DeliveryKpiPpt
    {
        /** @var DeliveryKpiPpt|null $deliveryKpiPpt */
        $deliveryKpiPpt = DeliveryKpiPpt::query()
            ->where('seller_id', $sellerId)
            ->first();
        if ($deliveryKpiPpt) {
            throw new CreateException('DeliveryKpiPpt are exist for this seller', 400);
        }
        $deliveryKpiPpt = new DeliveryKpiPpt();
        $deliveryKpiPpt->fill($fields);
        $deliveryKpiPpt->seller_id = $sellerId;

        $deliveryKpiPpt->save();

        return $deliveryKpiPpt;
    }
}
