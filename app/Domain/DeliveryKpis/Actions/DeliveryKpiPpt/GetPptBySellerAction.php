<?php

namespace App\Domain\DeliveryKpis\Actions\DeliveryKpiPpt;

use App\Domain\DeliveryKpis\Actions\DeliveryKpi\GetDeliveryKpiAction;
use App\Domain\DeliveryKpis\Models\DeliveryKpiPpt;
use Illuminate\Support\Collection;

class GetPptBySellerAction
{
    /** @var Collection|DeliveryKpiPpt[] */
    protected Collection|array|null $deliveryKpiPpt = null;

    public function __construct(protected GetDeliveryKpiAction $getDeliveryKpiAction)
    {
    }

    public function execute(int $sellerId): int
    {
        if (is_null($this->deliveryKpiPpt)) {
            $this->deliveryKpiPpt = DeliveryKpiPpt::query()
                ->where('seller_id', $sellerId)
                ->get()
                ->keyBy('seller_id');
        }
        if ($this->deliveryKpiPpt->has($sellerId)) {
            return $this->deliveryKpiPpt[$sellerId]->ppt;
        } else {
            $deliveryKpi = $this->getDeliveryKpiAction->execute();

            return $deliveryKpi ? $deliveryKpi->ppt : 0;
        }
    }

    public function loadPpt(array $sellersIds): void
    {
        $this->deliveryKpiPpt = DeliveryKpiPpt::query()
            ->whereIn('seller_id', $sellersIds)
            ->get()
            ->keyBy('seller_id');
    }
}
