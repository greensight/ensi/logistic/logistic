<?php

namespace App\Domain\DeliveryKpis\Actions\DeliveryKpiPpt;

use App\Domain\DeliveryKpis\Models\DeliveryKpiPpt;

class PatchDeliveryKpiPptAction
{
    public function execute(int $sellerId, array $fields): DeliveryKpiPpt
    {
        /** @var DeliveryKpiPpt $deliveryKpiPpt */
        $deliveryKpiPpt = DeliveryKpiPpt::query()
            ->where('seller_id', $sellerId)
            ->firstOrNew();

        $deliveryKpiPpt->fill($fields);
        $deliveryKpiPpt->seller_id = $sellerId;

        $deliveryKpiPpt->save();

        return $deliveryKpiPpt;
    }
}
