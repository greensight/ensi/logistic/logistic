<?php

namespace App\Domain\DeliveryKpis\Actions\DeliveryKpiPpt;

use App\Domain\DeliveryKpis\Models\DeliveryKpiPpt;

class DeleteDeliveryKpiPptAction
{
    public function execute(int $sellerId): void
    {
        /** @var DeliveryKpiPpt $deliveryKpiPpt */
        $deliveryKpiPpt = DeliveryKpiPpt::query()
            ->where('seller_id', $sellerId)
            ->firstOrFail();

        $deliveryKpiPpt->delete();
    }
}
