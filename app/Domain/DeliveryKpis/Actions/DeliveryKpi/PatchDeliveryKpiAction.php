<?php

namespace App\Domain\DeliveryKpis\Actions\DeliveryKpi;

use App\Domain\DeliveryKpis\Models\DeliveryKpi;

class PatchDeliveryKpiAction
{
    public function execute(array $fields): DeliveryKpi
    {
        /** @var DeliveryKpi|null $deliveryKpi */
        $deliveryKpi = DeliveryKpi::query()->first();
        if (is_null($deliveryKpi)) {
            $deliveryKpi = new DeliveryKpi();
        }

        $deliveryKpi->fill($fields);
        $deliveryKpi->save();

        return $deliveryKpi;
    }
}
