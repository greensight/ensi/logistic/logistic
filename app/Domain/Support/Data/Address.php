<?php

namespace App\Domain\Support\Data;

use App\Domain\Support\Data\Tests\Factories\AddressFactory;
use Ensi\BuClient\Dto\Address as BuAddress;
use Ensi\OmsClient\Dto\Address as OmsAddress;
use Illuminate\Contracts\Database\Eloquent\Castable;
use Illuminate\Contracts\Database\Eloquent\CastsAttributes;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Fluent;

/**
 * @property string|null $address_string - Адрес полностью
 * @property string|null $country_code - Код страны в соответствии с ISO 3166-1 alpha-2
 * @property string|null $post_index - Почтовые индекс
 * @property string|null $region - Регион
 * @property string|null $region_guid - GUID региона
 * @property string|null $area - Область
 * @property string|null $area_guid - GUID области
 * @property string|null $city - Город
 * @property string|null $city_guid - Код города
 * @property string|null $street - Улица
 * @property string|null $house - Дом
 * @property string|null $block - Корпус
 * @property string|null $flat - Квартира
 * @property string|null $floor - Этаж
 * @property string|null $porch - Подъезд
 * @property string|null $intercom - Домофон
 * @property string|null $geo_lat - Широта
 * @property string|null $geo_lon - Долгота
 */
class Address extends Fluent implements Castable
{
    public static function rules(): array
    {
        return [
            "address_string" => ['nullable', 'string'],
            "country_code" => ['nullable', 'string'],
            "post_index" => ['nullable', 'string'],
            "region" => ['nullable', 'string'],
            "region_guid" => ['nullable', 'string'],
            "area" => ['nullable', 'string'],
            "area_guid" => ['nullable', 'string'],
            "city" => ['nullable', 'string'],
            "city_guid" => ['nullable', 'string'],
            "street" => ['nullable', 'string'],
            "house" => ['nullable', 'string'],
            "block" => ['nullable', 'string'],
            "flat" => ['nullable', 'string'],
            "floor" => ['nullable', 'string'],
            "porch" => ['nullable', 'string'],
            "intercom" => ['nullable', 'string'],
            "geo_lat" => ['nullable', 'string'],
            "geo_lon" => ['nullable', 'string'],
        ];
    }

    public static function validateOrFail(array $address): self
    {
        $validator = Validator::make($address, self::rules());

        return new self($validator->validate());
    }

    /**
     * Формирует строку с полным адресом.
     *
     * @param bool $addNewLine Добавить перенос строки перед домофоном (если он задан)
     */
    public function getFullString(bool $addNewLine = false): string
    {
        $addressParts = [
            $this->address_string ?? '',
            $this->porch ? 'под. ' . $this->porch : null,
            $this->floor ? 'эт. ' . $this->floor : null,
            $this->flat ? 'кв. ' . $this->flat : null,
        ];

        $intercomLine = $this->intercom ? 'домофон ' . $this->intercom : null;
        if ($intercomLine) {
            $intercomLine = ($addNewLine ? "\n" : '') . $intercomLine;
        }
        $addressParts[] = $intercomLine;

        return join(', ', array_filter($addressParts));
    }

    public static function factory(): AddressFactory
    {
        return AddressFactory::new();
    }

    public static function makeFromBuStoreAddress(BuAddress $storeAddress): self
    {
        $self = new self();
        $self->address_string = $storeAddress->getAddressString();
        $self->country_code = $storeAddress->getCountryCode();
        $self->post_index = $storeAddress->getPostIndex();
        $self->region = $storeAddress->getAddressString();
        $self->region_guid = $storeAddress->getRegionGuid();
        $self->area = $storeAddress->getArea();
        $self->area_guid = $storeAddress->getAreaGuid();
        $self->city = $storeAddress->getCity();
        $self->city_guid = $storeAddress->getCityGuid();
        $self->street = $storeAddress->getStreet();
        $self->house = $storeAddress->getHouse();
        $self->block = $storeAddress->getBlock();
        $self->flat = $storeAddress->getFlat();
        $self->porch = $storeAddress->getPorch();
        $self->intercom = $storeAddress->getIntercom();
        $self->geo_lat = $storeAddress->getGeoLat();
        $self->geo_lon = $storeAddress->getGeoLon();

        return $self;
    }

    public static function makeFromOmsDeliveryAddress(OmsAddress $address): self
    {
        $self = new self();
        $self->address_string = $address->getAddressString();
        $self->country_code = $address->getCountryCode();
        $self->post_index = $address->getPostIndex();
        $self->region = $address->getAddressString();
        $self->region_guid = $address->getRegionGuid();
        $self->area = $address->getArea();
        $self->area_guid = $address->getAreaGuid();
        $self->city = $address->getCity();
        $self->city_guid = $address->getCityGuid();
        $self->street = $address->getStreet();
        $self->house = $address->getHouse();
        $self->block = $address->getBlock();
        $self->flat = $address->getFlat();
        $self->porch = $address->getPorch();
        $self->intercom = $address->getIntercom();
        $self->geo_lat = $address->getGeoLat();
        $self->geo_lon = $address->getGeoLon();

        return $self;
    }

    public static function castUsing(array $arguments): CastsAttributes
    {
        return new class () implements CastsAttributes {
            public function get($model, string $key, mixed $value, array $attributes): ?Address
            {
                return isset($attributes[$key]) ? new Address(json_decode($attributes[$key])) : null;
            }

            public function set($model, string $key, mixed $value, array $attributes): ?string
            {
                return match (true) {
                    is_array($value) => (new Address($value))->toJson(),
                    $value instanceof Address => $value->toJson(),
                    default => null,
                };
            }
        };
    }
}
