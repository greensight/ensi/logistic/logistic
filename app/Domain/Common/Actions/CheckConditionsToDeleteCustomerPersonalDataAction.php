<?php

namespace App\Domain\Common\Actions;

use App\Domain\Common\Actions\Data\CustomerPersonalData;
use App\Domain\DeliveryOrders\Enums\DeliveryOrderStatus;
use App\Domain\DeliveryOrders\Models\DeliveryOrder;

class CheckConditionsToDeleteCustomerPersonalDataAction
{
    protected string $message = '';

    public function execute(int $customerId): CustomerPersonalData
    {
        $customerPersonalData = new CustomerPersonalData();
        $customerPersonalData->canDelete = $this->checkConditionsToDelete($customerId);
        $customerPersonalData->message = $this->message;

        return $customerPersonalData;
    }

    public function checkConditionsToDelete(int $customerId): bool
    {
        if ($this->getNotAvailableForDeleteOrdersCount($customerId) > 0) {
            $this->message = trans('customer_delete_messages.incomplete_deliveries');

            return false;
        }

        return true;
    }

    protected function getNotAvailableForDeleteOrdersCount(int $customerId): int
    {
        return DeliveryOrder::whereCustomerId($customerId)
            ->whereNotIn('status', DeliveryOrderStatus::done())
            ->count();
    }
}
