<?php

namespace App\Domain\Common\Actions;

use App\Domain\DeliveryOrders\Models\DeliveryOrder;
use App\Exceptions\IllegalOperationException;

class DeleteCustomerPersonalDataAction
{
    public function __construct(
        protected readonly CheckConditionsToDeleteCustomerPersonalDataAction $checkConditionsToDeleteCustomerPersonalDataAction
    ) {
    }

    /**
     * @throws IllegalOperationException
     */
    public function execute(int $customerId): void
    {
        $customerPersonalData = $this->checkConditionsToDeleteCustomerPersonalDataAction->execute($customerId);

        if (!$customerPersonalData->canDelete) {
            throw new IllegalOperationException($customerPersonalData->message);
        }

        DeliveryOrder::whereCustomerId($customerId)
            ->eachById(function (DeliveryOrder $deliveryOrder) {
                $deliveryOrder->recipient_address = null;
                $deliveryOrder->recipient_email = null;
                $deliveryOrder->recipient_phone = null;
                $deliveryOrder->recipient_comment = null;
                $deliveryOrder->recipient_company_name = null;
                $deliveryOrder->recipient_contact_name = null;

                $deliveryOrder->save();
            }, count: 50);
    }
}
