<?php

namespace App\Domain\Common\Actions\Data;

class CustomerPersonalData
{
    public bool $canDelete;
    public string $message;
}
