<?php

namespace App\Domain\Common\Tests\Factories\Oms;

use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\OmsClient\Dto\Delivery;
use Ensi\OmsClient\Dto\DeliveryStatusEnum;
use Ensi\OmsClient\Dto\Shipment;

class DeliveryFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'order_id' => $this->faker->modelId(),
            'status' => $this->faker->randomElement(DeliveryStatusEnum::getAllowableEnumValues()),
            'number' => $this->faker->unique()->numerify('######-#'),
            'cost' => $this->faker->numberBetween(0, 1000),
            'date' => $this->faker->date(),
            'timeslot' => $this->faker->boolean ? TimeslotDtoFactory::new()->make() : null,
            'width' => $this->faker->numberBetween(1, 1000),
            'height' => $this->faker->numberBetween(1, 1000),
            'length' => $this->faker->numberBetween(1, 1000),
            'weight' => $this->faker->numberBetween(1, 1000),
        ];
    }

    /** @param Shipment[] $shipments */
    public function withShipments(array $shipments = [], int $count = 1): self
    {
        return $this->state(['shipments' => $shipments ?: ShipmentFactory::new()->makeSeveral($count)]);
    }

    public function make(array $extra = []): Delivery
    {
        return new Delivery($this->makeArray($extra));
    }
}
