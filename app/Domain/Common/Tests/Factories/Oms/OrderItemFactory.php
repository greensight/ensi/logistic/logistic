<?php

namespace App\Domain\Common\Tests\Factories\Oms;

use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\OmsClient\Dto\OrderItem;

class OrderItemFactory extends BaseApiFactory
{
    public function definition(): array
    {
        $pricePerOne = $this->faker->numberBetween(1, 10000);
        $costPerOne = $this->faker->numberBetween($pricePerOne, $pricePerOne + 1000);
        $qty = $this->faker->randomFloat(2, 1, 100);

        return [
            'offer_id' => $this->faker->modelId(),
            'name' => $this->faker->text(50),
            'qty' => $qty,
            'old_qty' => $this->faker->nullable()->randomFloat(2, 1, 100),
            'price_per_one' => $pricePerOne,
            'cost_per_one' => $costPerOne,
            'product_weight' => $this->faker->numberBetween(1, 100),
            'product_weight_gross' => $this->faker->numberBetween(1, 100),
            'product_width' => $this->faker->numberBetween(1, 100),
            'product_height' => $this->faker->numberBetween(1, 100),
            'product_length' => $this->faker->numberBetween(1, 100),
            'product_barcode' => $this->faker->nullable()->numerify('############'),
            'order_id' => $this->faker->modelId(),
            'shipment_id' => $this->faker->modelId(),
            'is_added' => $this->faker->boolean(),
            'is_deleted' => $this->faker->boolean(),
        ];
    }

    public function make(array $extra = []): OrderItem
    {
        return new OrderItem($this->makeArray($extra));
    }
}
