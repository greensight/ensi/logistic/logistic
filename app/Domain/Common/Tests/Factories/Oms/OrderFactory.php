<?php

namespace App\Domain\Common\Tests\Factories\Oms;

use App\Domain\DeliveryServices\Models\Point;
use App\Domain\DeliveryServices\Models\Tests\Factories\PointFactory;
use App\Http\ApiV1\OpenApiGenerated\Enums\DeliveryMethodEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\DeliveryServiceEnum;
use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\OmsClient\Dto\Delivery;
use Ensi\OmsClient\Dto\Order;
use Ensi\OmsClient\Dto\OrderResponse;
use Ensi\OmsClient\Dto\OrderStatusEnum;
use Ensi\OmsClient\Dto\SearchOrdersResponse;

class OrderFactory extends BaseApiFactory
{
    protected int $deliveryMethod;

    protected function definition(): array
    {
        $deliveryMethod = $this->deliveryMethod ?? $this->faker->randomEnum(DeliveryMethodEnum::cases());
        $isDelivery = $deliveryMethod == DeliveryMethodEnum::DELIVERY->value;

        $deliveryPrice = $this->faker->numberBetween(0, 200);
        $price = $this->faker->numberBetween($deliveryPrice + 10, 10000);

        /** @var Point $point */
        $point = PointFactory::new()->withAddress()->create();

        return [
            'id' => $this->faker->modelId(),

            'number' => $this->faker->unique()->numerify('######'),
            'responsible_id' => $this->faker->nullable()->modelId(),
            'customer_id' => $this->faker->modelId(),
            'customer_email' => $this->faker->email(),
            'cost' => $this->faker->numberBetween($price, $price + 10000),
            'price' => $price,
            'client_comment' => $this->faker->nullable()->text(50),

            'delivery_service' => $this->faker->randomEnum(DeliveryServiceEnum::cases()),
            'delivery_method' => $deliveryMethod,
            'delivery_cost' => $this->faker->numberBetween($deliveryPrice, $deliveryPrice + 500),
            'delivery_price' => $deliveryPrice,
            'delivery_point_id' => $isDelivery ? null : $point->id,
            'delivery_address' => $isDelivery ? AddressDataFactory::new()->make() : null,
            'delivery_comment' => $this->faker->nullable()->text(50),

            'receiver_name' => $this->faker->nullable()->name(),
            'receiver_phone' => $this->faker->nullable()->numerify('+7##########'),
            'receiver_email' => $this->faker->nullable()->email(),

            'status' => $this->faker->randomElement(OrderStatusEnum::getAllowableEnumValues()),
            'status_at' => $this->faker->dateTime(),

            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
        ];
    }

    /** @param Delivery[] $deliveries */
    public function withDeliveries(array $deliveries = null, int $count = 1): self
    {
        return $this->state(['deliveries' => $deliveries ?: DeliveryFactory::new()->makeSeveral($count)]);
    }

    public function withDeliveryMethod(int $deliveryMethod): self
    {
        $this->deliveryMethod = $deliveryMethod;

        return $this;
    }

    public function make(array $extra = []): Order
    {
        return new Order($this->makeArray($extra));
    }

    public function makeResponse(array $extra = []): OrderResponse
    {
        return new OrderResponse([
            'data' => $this->make($extra),
        ]);
    }

    public function makeResponseSearch(array $extras = [], int $count = 1, mixed $pagination = null): SearchOrdersResponse
    {
        return $this->generateResponseSearch(SearchOrdersResponse::class, $extras, $count, $pagination);
    }
}
