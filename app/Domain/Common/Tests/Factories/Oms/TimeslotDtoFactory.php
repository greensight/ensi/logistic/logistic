<?php

namespace App\Domain\Common\Tests\Factories\Oms;

use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\OmsClient\Dto\Timeslot;

class TimeslotDtoFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->uuid(),
            'from' => $this->faker->time('H:i'),
            'to' => $this->faker->time('H:i'),
        ];
    }

    public function make(array $extra = []): Timeslot
    {
        return new Timeslot($this->makeArray($extra));
    }
}
