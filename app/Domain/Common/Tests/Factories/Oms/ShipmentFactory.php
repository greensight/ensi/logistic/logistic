<?php

namespace App\Domain\Common\Tests\Factories\Oms;

use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\OmsClient\Dto\OrderItem;
use Ensi\OmsClient\Dto\Shipment;
use Ensi\OmsClient\Dto\ShipmentStatusEnum;

class ShipmentFactory extends BaseApiFactory
{
    public function definition(): array
    {
        return [
            'delivery_id' => $this->faker->modelId(),
            'store_id' => $this->faker->modelId(),
            'status' => $this->faker->randomElement(ShipmentStatusEnum::getAllowableEnumValues()),
            'number' => $this->faker->unique()->numerify('######-#-#'),
        ];
    }

    /** @param OrderItem[] $orderItems */
    public function withOrderItems(array $orderItems = [], int $count = 1): self
    {
        return $this->state(['order_items' => $orderItems ?: OrderItemFactory::new()->makeSeveral($count)]);
    }

    public function make(array $extra = []): Shipment
    {
        return new Shipment($this->makeArray($extra));
    }
}
