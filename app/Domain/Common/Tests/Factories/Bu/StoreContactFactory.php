<?php

namespace App\Domain\Common\Tests\Factories\Bu;

use Ensi\BuClient\Dto\StoreContact;
use Ensi\LaravelTestFactories\BaseApiFactory;

class StoreContactFactory extends BaseApiFactory
{
    public function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'store_id' => $this->faker->modelId(),
            'name' => $this->faker->nullable()->name(),
            'phone' => $this->faker->nullable()->numerify('+7##########'),
            'email' => $this->faker->nullable()->email(),
        ];
    }

    public function make(array $extra = []): StoreContact
    {
        return new StoreContact($this->makeArray($extra));
    }
}
