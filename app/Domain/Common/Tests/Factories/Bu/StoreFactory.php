<?php

namespace App\Domain\Common\Tests\Factories\Bu;

use Ensi\BuClient\Dto\SearchStoresResponse;
use Ensi\BuClient\Dto\Store;
use Ensi\BuClient\Dto\StoreContact;
use Ensi\BuClient\Dto\StoreResponse;
use Ensi\LaravelTestFactories\BaseApiFactory;

class StoreFactory extends BaseApiFactory
{
    public function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'seller_id' => $this->faker->nullable()->modelId(),
            'xml_id' => $this->faker->nullable()->exactly($this->faker->unique()->word()),
            'active' => $this->faker->boolean(),
            'name' => $this->faker->name(),
            'address' => $this->faker->exactly(AddressDataFactory::new()->make()),
            'timezone' => $this->faker->timezone(),
        ];
    }

    /** @param StoreContact[] $contacts */
    public function withContacts(array $contacts = [], int $count = 1): self
    {
        return $this->state(['contacts' => $contacts ?: StoreContactFactory::new()->makeSeveral($count)]);
    }

    public function make(array $extra = []): Store
    {
        return new Store($this->makeArray($extra));
    }

    public function makeResponse(array $extra = []): StoreResponse
    {
        return new StoreResponse([
            'data' => $this->make($extra),
        ]);
    }

    public function makeResponseSearch(array $extras = [], int $count = 1, mixed $pagination = null): SearchStoresResponse
    {
        return $this->generateResponseSearch(SearchStoresResponse::class, $extras, $count, $pagination);
    }
}
