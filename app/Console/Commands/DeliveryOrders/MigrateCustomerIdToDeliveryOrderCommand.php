<?php

namespace App\Console\Commands\DeliveryOrders;

use App\Domain\DeliveryOrders\Actions\DeliveryOrder\MigrateCustomerIdToDeliveryOrderAction;
use Illuminate\Console\Command;

class MigrateCustomerIdToDeliveryOrderCommand extends Command
{
    protected $signature = 'delivery-orders:migrate-customer-id';
    protected $description = 'Мигрировать customer_id для всех доставок из OMS';

    public function handle(MigrateCustomerIdToDeliveryOrderAction $action): void
    {
        $action->execute();
    }
}
