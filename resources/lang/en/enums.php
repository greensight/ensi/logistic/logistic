<?php

use App\Domain\DeliveryServices\Enums\DeliveryMethod;

return [
    DeliveryMethod::class => [
        DeliveryMethod::METHOD_DELIVERY->value => 'Express delivery',
        DeliveryMethod::METHOD_PICKUP->value => 'Pickup from the pickup point',
        DeliveryMethod::METHOD_STORE->value => 'Pickup from the store',
    ],
];
