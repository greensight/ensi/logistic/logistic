<?php

use App\Domain\DeliveryServices\Enums\DeliveryMethod;

return [
    DeliveryMethod::class => [
        DeliveryMethod::METHOD_DELIVERY->value => 'Курьерская доставка',
        DeliveryMethod::METHOD_PICKUP->value => 'Самовывоз из ПВЗ',
        DeliveryMethod::METHOD_STORE->value => 'Самовывоз из магазина',
    ],
];
