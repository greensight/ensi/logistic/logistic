<?php

namespace Tests;

use Ensi\BuClient\Api\SellersApi;
use Ensi\BuClient\Api\StoresApi;
use Ensi\OmsClient\Api\OrdersApi;
use Mockery\MockInterface;

trait MockServicesApi
{
    // =============== OMS ===============
    public function mockOrdersApi(): MockInterface|OrdersApi
    {
        return $this->mock(OrdersApi::class);
    }

    // =============== BU ===============
    public function mockStoresApi(): MockInterface|StoresApi
    {
        return $this->mock(StoresApi::class);
    }

    public function mockSellersApi(): MockInterface|SellersApi
    {
        return $this->mock(SellersApi::class);
    }
}
