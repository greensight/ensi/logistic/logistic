<?php

use Illuminate\Database\Migrations\Migration;

return new class () extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('delivery_services')
            ->insert([
                'id' => 1,
                'name' => 'TK1',
                'apiship_key' => 'tk1',
                'status' => 1,
                'registered_at' => now(),
                'priority' => 1,
                'do_consolidation' => true,
                'do_deconsolidation' => true,
                'do_express_delivery' => false,
                'created_at' => now(),
            ]);
        DB::table('delivery_services')
            ->insert([
                'id' => 2,
                'name' => 'TK2',
                'apiship_key' => 'tk2',
                'status' => 3,
                'registered_at' => now(),
                'priority' => 3,
                'do_consolidation' => false,
                'do_deconsolidation' => false,
                'do_express_delivery' => false,
                'created_at' => now(),
            ]);
        DB::table('delivery_services')
            ->insert([
                'id' => 3,
                'name' => 'TK3',
                'apiship_key' => 'tk3',
                'status' => 1,
                'registered_at' => now(),
                'priority' => 2,
                'do_consolidation' => false,
                'do_deconsolidation' => false,
                'do_express_delivery' => false,
                'created_at' => now(),
            ]);
        DB::table('delivery_services')
            ->insert([
                'id' => 4,
                'name' => 'TK4',
                'apiship_key' => 'tk4',
                'status' => 3,
                'registered_at' => now(),
                'priority' => 3,
                'do_consolidation' => false,
                'do_deconsolidation' => false,
                'do_express_delivery' => false,
                'created_at' => now(),
            ]);
        DB::table('delivery_services')
            ->insert([
                'id' => 5,
                'name' => 'TK5',
                'apiship_key' => 'tk5',
                'status' => 3,
                'registered_at' => now(),
                'priority' => 3,
                'do_consolidation' => false,
                'do_deconsolidation' => false,
                'do_express_delivery' => false,
                'created_at' => now(),
            ]);
        DB::table('delivery_services')
            ->insert([
                'id' => 6,
                'name' => 'TK6',
                'apiship_key' => 'tk6',
                'status' => 3,
                'registered_at' => now(),
                'priority' => 3,
                'do_consolidation' => false,
                'do_deconsolidation' => false,
                'do_express_delivery' => false,
                'created_at' => now(),
            ]);
        DB::table('delivery_services')
            ->insert([
                'id' => 7,
                'name' => 'TK7',
                'apiship_key' => 'tk7',
                'status' => 3,
                'registered_at' => now(),
                'priority' => 3,
                'do_consolidation' => false,
                'do_deconsolidation' => false,
                'do_express_delivery' => false,
                'created_at' => now(),
            ]);
        DB::table('delivery_services')
            ->insert([
                'id' => 8,
                'name' => 'TK8',
                'apiship_key' => 'tk8',
                'status' => 3,
                'registered_at' => now(),
                'priority' => 3,
                'do_consolidation' => false,
                'do_deconsolidation' => false,
                'do_express_delivery' => false,
                'created_at' => now(),
            ]);
        DB::table('delivery_services')
            ->insert([
                'id' => 9,
                'name' => 'TK9',
                'apiship_key' => 'tk9',
                'status' => 3,
                'registered_at' => now(),
                'priority' => 3,
                'do_consolidation' => false,
                'do_deconsolidation' => false,
                'do_express_delivery' => false,
                'created_at' => now(),
            ]);
        DB::table('delivery_services')
            ->insert([
                'id' => 10,
                'name' => 'TK10',
                'apiship_key' => 'tk10',
                'status' => 3,
                'registered_at' => now(),
                'priority' => 3,
                'do_consolidation' => false,
                'do_deconsolidation' => false,
                'do_express_delivery' => false,
                'created_at' => now(),
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('delivery_services')
            ->whereIn(
                'apiship_key',
                ['b2cpl', 'boxberry', 'cdek', 'dostavista', 'dpd', 'iml', 'maxi', 'pickpoint', 'pony', 'rupost']
            )
            ->delete();
    }
};
