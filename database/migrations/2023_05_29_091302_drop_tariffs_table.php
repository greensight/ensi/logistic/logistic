<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('delivery_orders', function (Blueprint $table) {
            $table->dropColumn('tariff_id');
        });

        Schema::dropIfExists('tariffs');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('tariffs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('delivery_service')->unsigned();
            $table->tinyInteger('delivery_method')->unsigned()->nullable();
            $table->tinyInteger('shipment_method')->unsigned()->nullable();
            $table->string('name');
            $table->string('description')->nullable();
            $table->string('external_id');
            $table->string('apiship_external_id');
            $table->integer('weight_min')->unsigned()->nullable();
            $table->integer('weight_max')->unsigned()->nullable();

            $table->timestamps();

            $table->foreign('delivery_service')
                ->references('id')
                ->on('delivery_services')
                ->onDelete('cascade');
        });

        Schema::table('delivery_orders', function (Blueprint $table) {
            $table->unsignedBigInteger('tariff_id')->nullable();
        });
    }
};
