<?php

use Illuminate\Database\Migrations\Migration;

return new class () extends Migration {
    public function up()
    {
        DB::statement('UPDATE federal_districts SET updated_at = NOW()');
        DB::statement('UPDATE regions SET updated_at = NOW()');
    }

    public function down()
    {
        DB::statement('UPDATE federal_districts SET updated_at = null');
        DB::statement('UPDATE regions SET updated_at = null');
    }
};
