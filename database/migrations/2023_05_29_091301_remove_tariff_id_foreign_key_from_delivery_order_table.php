<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public const OLD_MIGRATION = 'remove_tariff_id_foreign_key_from_delivery_order_table';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $data = DB::select('SELECT id FROM migrations WHERE "migration" = ?', [static::OLD_MIGRATION]);
        $item = $data[0] ?? null;

        if ($item) {
            DB::delete('DELETE FROM migrations WHERE "id" = ?', [$item->id]);

            return;
        }

        Schema::table('delivery_orders', function (Blueprint $table) {
            $table->dropForeign('delivery_orders_tariff_id_foreign');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('delivery_orders', function (Blueprint $table) {
            $table->foreign('tariff_id')->references('id')->on('tariffs');
        });
    }
};
