<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (DB::table('delivery_orders')->whereNull('customer_id')->exists()) {
            throw new Exception('Существуют записи с пустым customer_id. Заполните их командой php artisan delivery-orders:migrate-customer-id');
        }

        Schema::table('delivery_orders', function (Blueprint $table) {
            $table->bigInteger('customer_id')->unsigned()->nullable(false)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('delivery_orders', function (Blueprint $table) {
            $table->bigInteger('customer_id')->unsigned()->nullable()->change();
        });
    }
};
