<?php

use Illuminate\Database\Migrations\Migration;

return new class () extends Migration {
    public function up()
    {
        DB::statement('UPDATE delivery_services SET updated_at = NOW()');
    }

    public function down()
    {
        DB::statement('UPDATE delivery_services SET updated_at = null');
    }
};
