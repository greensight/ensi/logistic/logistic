<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up(): void
    {
        Schema::dropIfExists('point_metro_station_links');
        Schema::dropIfExists('metro_stations');
        Schema::dropIfExists('metro_lines');

        Schema::table('points', function (Blueprint $table) {
            $table->renameColumn('delivery_service', 'delivery_service_id');
            $table->integer('delivery_service_id')->nullable(true)->change();
            $table->renameColumn('active', 'is_active');

            $table->dropColumn('type');
            $table->dropColumn('apiship_external_id');
            $table->dropColumn('email');
            $table->dropColumn('timetable');

            $table->boolean('has_payment_card')->nullable(true)->change();
            $table->json('address')->nullable(true)->change();

            $table->string('timezone')->nullable();
            $table->string('address_reduce')->nullable();
            $table->string('metro_station')->nullable();
            $table->string('geo_lat')->nullable();
            $table->string('geo_lon')->nullable();

            $table->boolean('only_online_payment')->nullable();
            $table->boolean('has_courier')->nullable();
            $table->boolean('is_postamat')->nullable();

            $table->string('max_value')->nullable();
            $table->unsignedInteger('max_weight')->nullable();
        });

        Schema::create('point_workings', function (Blueprint $table) {
            $table->id();
            $table->boolean('is_active')->default(true);
            $table->foreignId('point_id')->constrained()->cascadeOnDelete();
            $table->tinyInteger('day');
            $table->string('working_start_time');
            $table->string('working_end_time');

            $table->timestamps(6);
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('point_workings');

        Schema::create('metro_lines', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');

            $table->timestamps();
        });

        Schema::create('metro_stations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('metro_line_id')->unsigned();
            $table->string('name');
            $table->string('city_guid');

            $table->timestamps();

            $table->foreign('metro_line_id')->references('id')->on('metro_lines')->onDelete('cascade');
        });

        Schema::create('point_metro_station_links', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('point_id')->unsigned();
            $table->bigInteger('metro_station_id')->unsigned();
            $table->float('distance');

            $table->timestamps();
        });

        Schema::table('point_metro_station_links', function (Blueprint $table) {
            $table->foreign('metro_station_id')
                ->references('id')
                ->on('metro_stations')->onDelete('cascade');

            $table->foreign('point_id')
                ->references('id')
                ->on('points');
        });

        Schema::table('points', function (Blueprint $table) {
            $table->renameColumn('delivery_service_id', 'delivery_service');
            $table->integer('delivery_service')->nullable(false)->change();
            $table->renameColumn('is_active', 'active');

            $table->integer('type')->unsigned()->nullable();
            $table->string('apiship_external_id')->nullable();
            $table->string('email')->nullable();
            $table->string('timetable')->nullable();


            $table->boolean('has_payment_card')->nullable(false)->change();
            $table->json('address')->nullable(false)->change();

            $table->dropColumn([
                'timezone',
                'address_reduce',
                'metro_station',
                'geo_lat',
                'geo_lon',
                'only_online_payment',
                'has_courier',
                'is_postamat',
                'max_value',
                'max_weight',
            ]);
        });

        DB::table('points')->update([
            'type' => 1,
            'apiship_external_id' => 'default_external_id',
        ]);

        Schema::table('points', function (Blueprint $table) {
            $table->integer('type')->nullable(false)->change();
            $table->string('apiship_external_id')->nullable(false)->change();
        });
    }
};
