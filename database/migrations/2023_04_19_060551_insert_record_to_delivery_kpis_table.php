<?php

use Illuminate\Database\Migrations\Migration;

return new class () extends Migration {
    public function up()
    {
        DB::table('delivery_kpi')->insert([
            'rtg' => 50,
            'ct' => 40,
            'ppt' => 110,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }

    public function down()
    {
        DB::table('delivery_kpi')->truncate();
    }
};
