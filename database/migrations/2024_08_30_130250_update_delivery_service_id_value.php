<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Str;

return new class () extends Migration {
    private const MAPPING = [
        1 => 'TK1',
        2 => 'TK2',
        3 => 'TK3',
        4 => 'TK4',
        5 => 'TK5',
        6 => 'TK6',
        7 => 'TK7',
        8 => 'TK8',
        9 => 'TK9',
        10 => 'TK10',
    ];

    public function up()
    {
        foreach (self::MAPPING as $deliveryId => $deliveryName) {
            DB::table('delivery_services')
                ->where('id', $deliveryId)
                ->update([
                    'name' => $deliveryName,
                    'apiship_key' => Str::slug($deliveryName),
                ]);
        }
    }

    public function down()
    {
    }
};
