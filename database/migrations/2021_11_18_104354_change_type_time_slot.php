<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cargo_orders', function (Blueprint $table) {
            $table->time('timeslot_from')->change();
            $table->time('timeslot_to')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Create a temporary TIMESTAMP column
        DB::statement("ALTER TABLE cargo_orders ADD COLUMN from_holder TIMESTAMP(6) NULL");
        DB::statement("ALTER TABLE cargo_orders ADD COLUMN to_holder TIMESTAMP(6) NULL");

        // Copy casted value over to the temporary column
        DB::statement("UPDATE cargo_orders SET from_holder = current_date + timeslot_from");
        DB::statement("UPDATE cargo_orders SET to_holder = current_date + timeslot_to");

        // Modify original column using the temporary column
        DB::statement("ALTER TABLE cargo_orders ALTER COLUMN timeslot_from TYPE TIMESTAMP(6) USING from_holder");
        DB::statement("ALTER TABLE cargo_orders ALTER COLUMN timeslot_to TYPE TIMESTAMP(6) USING to_holder");

        // Drop the temporary column (after examining altered column values)
        DB::statement("ALTER TABLE cargo_orders DROP COLUMN from_holder");
        DB::statement("ALTER TABLE cargo_orders DROP COLUMN to_holder");
    }
};
