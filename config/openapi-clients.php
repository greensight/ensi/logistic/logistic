<?php

return [
    'units' => [
        'bu' => [
            'base_uri' => env('UNITS_BU_SERVICE_HOST') . "/api/v1",
        ],
    ],
    'orders' => [
        'oms' => [
            'base_uri' => env('ORDERS_OMS_SERVICE_HOST') . "/api/v1",
        ],
    ],
];
