<?php

use App\Domain\Kafka\Actions\Listen\DeliveryListenAction;
use App\Domain\Kafka\Actions\Listen\StorePickupTimeListenAction;
use Ensi\LaravelInitialEventPropagation\RdKafkaConsumerMiddleware;
use Ensi\LaravelMetrics\Kafka\KafkaMetricsMiddleware;

return [
    /*
    | Optional, defaults to empty array.
    | Array of global middleware fully qualified class names.
    */
    'global_middleware' => [ RdKafkaConsumerMiddleware::class, KafkaMetricsMiddleware::class ],
    'stop_signals' => [SIGTERM, SIGINT],

    'processors' => [
        [
            'topic' => 'store-pickup-times',
            'consumer' => 'default',
            'type' => 'action',
            'class' => StorePickupTimeListenAction::class,
            'queue' => false,
            'consume_timeout' => 5000,
        ],
        [
            'topic' => 'deliveries',
            'consumer' => 'default',
            'type' => 'action',
            'class' => DeliveryListenAction::class,
            'queue' => false,
            'consume_timeout' => 5000,
        ],
    ],
];
